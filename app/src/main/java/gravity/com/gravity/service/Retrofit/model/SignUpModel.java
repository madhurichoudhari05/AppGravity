package gravity.com.gravity.service.Retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import gravity.com.gravity.retail_customer.model.PlumberUser;

/**
 * Created by Madhuri on 5/10/2018.
 */

public class SignUpModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("OTP")
    @Expose
    private String oTP;
    @SerializedName("response")
    @Expose
    private List<PlumberUser> response = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOTP() {
        return oTP;
    }

    public void setOTP(String oTP) {
        this.oTP = oTP;
    }

    public List<PlumberUser> getResponse() {
        return response;
    }

    public void setResponse(List<PlumberUser> response) {
        this.response = response;
    }

}
