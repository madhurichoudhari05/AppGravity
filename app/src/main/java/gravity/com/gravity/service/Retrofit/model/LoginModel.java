package gravity.com.gravity.service.Retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import gravity.com.gravity.plumber.model.LoginPOJO;

/**
 * Created by Madhuri on 5/10/2018.
 */

public class LoginModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("response")
    @Expose
    private List<LoginPOJO> response = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<LoginPOJO> getResponse() {
        return response;
    }

    public void setResponse(List<LoginPOJO> response) {
        this.response = response;
    }

}
