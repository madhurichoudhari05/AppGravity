package gravity.com.gravity.service.Retrofit.retrofit;


import java.util.Map;

import gravity.com.gravity.plumber.model.CustomerModel;
import gravity.com.gravity.plumber.model.progressCompleteModel;
import gravity.com.gravity.retail_customer.model.PlumUserModel;
import gravity.com.gravity.retail_customer.model.PlumberListResponse;
import gravity.com.gravity.plumber.model.PlumberRequesrListModel;
import gravity.com.gravity.retail_customer.model.UserModelSignUpModel;
import gravity.com.gravity.service.Retrofit.model.LoginModel;
import gravity.com.gravity.service.Retrofit.model.SignUpModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


public interface APIInterface {

    @POST("user_login.php")
    @FormUrlEncoded
    Call<LoginModel> getLogin(@Field("phone") String phone,
                              @Field("password") String password,
                              @Field("device token") String device_token,
                              @Field("user_type") String user_type);
    // Call<List<LoginModel>> getLogin(@Field("phone") String phone, @Field("password") String password);


    @POST("user_registration.php")
    @FormUrlEncoded
    Call<SignUpModel> getSignUp
            (@Field("name") String name,
             @Field("phone") String mobile_no,
             @Field("password") String password,
             @Field("country") String country,
             @Field("state") String state,
             @Field("city") String city,
             @Field("latitude") String latitude,
             @Field("longitude") String longitude,
             @Field("service_location") String service_location,
             @Field("user_type") String user_type,
             @Field("plumber_type") String plumber_type,
             @Field("device_token") String device_token,
             @Field("device_type") String device_type);

    @POST("user_registration.php")
    @FormUrlEncoded
    Call<UserModelSignUpModel>
    signUpUser(
            @Field("name") String name, @Field("email") String email, @Field("phone") String phone,
            @Field("password") String password, @Field("user_type") String user_type,
            @Field("device_type") String device_type, @Field("device_token") String device_token,
            @Field("latitude") String latitude, @Field("longitude") String longitude);


    @POST("user_login.php")
    @FormUrlEncoded
    Call<ResponseBody> signInUser(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("device_token") String token,
                                  @Field("user_type") String user_type);



    @POST("nearby_location.php")
    @FormUrlEncoded
    Call<PlumberListResponse> getPlumberList(@Field("latitude") String latitude,
                                                         @Field("longitude") String longitude);


    @GET("list.php")
    Call<ResponseBody> getCatagoryList();

    @POST("list.php")
    @FormUrlEncoded
    Call<ResponseBody> getSubCatagoryList(@Field("cat_id") String catagory);

    @POST("list.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductCatagoryList(@Field("subcat_id") String catagory);

    @POST("forget_password.php")
    @FormUrlEncoded
    Call<ResponseBody> forgotPassword(@Field("email") String email, @Field("user_type") String user_type);


    @POST("forget_password.php")
    @FormUrlEncoded
    Call<ResponseBody> forgotPassPlumber(@Field("phone") String phone, @Field("user_type") String user_type);


    @GET("location.php")
    Call<ResponseBody> getCountries();

    @POST("location.php")
    @FormUrlEncoded
    Call<ResponseBody> getStates(@Field("country_id") String country_id);

    @POST("location.php")
    @FormUrlEncoded
    Call<ResponseBody> getCities(@Field("state_id") String state_id);


    @Multipart
    @POST("complaint.php")
    Call<ResponseBody> uploadFileProfileile(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part[] file);

    @POST("pumber_update_location.php")
    @FormUrlEncoded
    Call<ResponseBody> updateLocation(@Field("plumber_id") String plumber_id,
                                      @Field("latitude") String latitude,
                                      @Field("longitude") String longitude,
                                      @Field("plumber_online") String user_type);


    @POST("plumber_go_offline.php")
    @FormUrlEncoded
    Call<ResponseBody> getOffLine(@Field("plumber_id") String plumber_id);

    @POST("plumber_go_online.php")
    @FormUrlEncoded
    Call<ResponseBody> getOnLine(@Field("plumber_id") String plumber_id);






    /*Push for customer to plumber  vice versa*/
    @POST("assign_plumber.php")
    @FormUrlEncoded
    Call<ResponseBody> getAssignPlumber(@Field("plumber_id") String user_id,
                                    @Field("customer_id") String customer_id
                                    ,@Field("complaint_no") String complaint_no);




    @POST("user_customer_data.php")
    @FormUrlEncoded
    Call<CustomerModel> getUserDetail(@Field("user_id") String user_id,
                                      @Field("complaint_assign") String complaint_assign,
                                      @Field("complaint_no") String complaint_no);

    @POST("user_customer_data.php")
    @FormUrlEncoded
    Call<CustomerModel> getINprogresUserDetail(@Field("user_id") String user_id,
                                      @Field("job_status") String job_status,
                                      @Field("complaint_no") String complaint_no);


    @POST("user_customer_data.php")
    @FormUrlEncoded
    Call<CustomerModel> getCompleteDetail(@Field("job_status") String job_status,
                                               @Field("complaint_no") String complaint_no);



    @POST("user_customer_data.php")
    @FormUrlEncoded
    Call<CustomerModel> getPushToCustDEtails(@Field("user_id") String complaint_id,
                                      @Field("complaint_no") String complaint_no,
                                      @Field("plumber_id") String plumber_id);






    @POST("user_customer_data.php")
    @FormUrlEncoded
    Call<PlumUserModel> getPlumberUserDetails(@Field("plumber_id") String user_id,
                                              @Field("user_id") String customer_id
                                       , @Field("complaint_no") String complaint_no
                                        , @Field("job_status") String job_status);








    @POST("user_specific_compaint.php")
    @FormUrlEncoded
    Call<PlumberRequesrListModel> specificComplaint(@Field("id") String plumber_id);

    @POST("job_status.php")
    @FormUrlEncoded
    Call<progressCompleteModel> getComplete(@Field("job_status") String jobstaus,
                                            @Field("plumber_id") String plumber_id);

    @POST("job_status.php")
    @FormUrlEncoded
    Call<progressCompleteModel> getProgress(@Field("job_status") String jobstaus,
                                              @Field("plumber_id") String plumber_id);

   /* @POST("nearby_location.php")
    @FormUrlEncoded
    Call<ResponseBody> findNearestPlumbe(@Field("latitude") String latitude,
                                         @Field("longitude") String longitude);*/


}
