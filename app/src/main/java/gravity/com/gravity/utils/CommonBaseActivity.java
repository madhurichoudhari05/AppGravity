package gravity.com.gravity.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;




/**
 * Created by arungeorge on 12/04/16.
 */

/*user_id
token
http://www.404coders.com/NetWrko/WebServices/chat/device_registration.php*/

public abstract class CommonBaseActivity extends AppCompatActivity {
    protected abstract void onPermissionResult(int requestCode, boolean isPermissionGranted);
    public static final String TAG = CommonBaseActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    protected FrameLayout mCoordinatorLayout;
    protected FragmentManager fm;
    private Toolbar mActionbarToolbar;
    String profile_pic="";
    private Snackbar mSnackbar;
    private ProgressDialog mProgressDialog;
    private  Context context;
    private ImageView toolbarImageView;
    private TextView tvNotiCount;
    int totalCount=0;
    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(this, permission, requestCode);

        return isAlreadyGranted;

    }
    protected boolean checkPermission(String[] permission){

        boolean isPermission = true;

        for(String s: permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(this,s) == PackageManager.PERMISSION_GRANTED;

        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            onPermissionResult(requestCode, true);

        } else {

            onPermissionResult(requestCode, true);
//            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();

        }

    }




    public FragmentManager getFm() {
        return fm;
    }





    @Override
    protected void onStart() {
        super.onStart();
//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this, 1);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
       // EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    public void setScreenTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }


    public void dismissProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean handleReturn = false;
        View view = null;
        int x = 0;
        int y = 0;
        try {
            handleReturn = super.dispatchTouchEvent(ev);
            view = getCurrentFocus();
            x = (int) ev.getX();
            y = (int) ev.getY();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (view instanceof EditText) {
                if (ev.getAction() == MotionEvent.ACTION_UP &&
                        !getLocationOnScreen((EditText) view).contains(x, y)) {
                    InputMethodManager input = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                            .getWindowToken(), 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return handleReturn;
    }
    protected Rect getLocationOnScreen(EditText mEditText) {

        Rect mRect = new Rect();
        int[] location = new int[2];
        mEditText.getLocationOnScreen(location);
        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();
        return mRect;

    }







}