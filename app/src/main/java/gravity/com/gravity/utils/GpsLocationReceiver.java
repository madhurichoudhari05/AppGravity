package gravity.com.gravity.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by User on 6/8/2018.
 */

public class GpsLocationReceiver  extends BroadcastReceiver {

    private FunNoParam mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(mListener!=null){
            mListener.done();


        }
    }

    public void setmListener(FunNoParam listener){
        mListener=listener;
    }
}