package gravity.com.gravity.utils.gps;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by User on 6/8/2018.
 */

public class GPSTracker2 extends Service implements LocationListener {

    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    String address="";

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker2(Context context) {
        this.mContext = context;
        getLocation();
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    if ( Build.VERSION.SDK_INT >= 23 &&
                            ContextCompat.checkSelfPermission( mContext, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission( mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return  null;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (locationManager != null) {
                if (mContext.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || mContext.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.removeUpdates(GPSTracker2.this);
                }
            }
        }
        else{
            if(locationManager != null) {

                locationManager.removeUpdates(GPSTracker2.this);



            }
        }


    }

    /**
     * Function to get latitude
     * */
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */
    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;
    public void showSettingsAlert(){

        if(alertDialogBuilder==null){
            alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialog = alertDialogBuilder.create();

            // Setting Dialog Title
            alertDialog.setTitle("GPS is settings");

            // Setting Dialog MessageData
            alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");


            // On pressing Settings button
            alertDialog.setButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(intent);
//                    dialog.cancel();
                }
            });

        }



        if(!alertDialog.isShowing()){
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
            //if its visibility is not showing then show here
//            alertDialog.show();
        }else{
            //do something here... if already showing
        }

    }

    public void dismissAlert(){
        if(alertDialog!=null && alertDialog.isShowing()){
            alertDialog.dismiss();
        }
    }


    public boolean isGPSEnabled(){

        LocationManager locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return true;
        }else{
            return false;
        }

    }


    public HashMap<String,String> saveLocation(Location location) {
        //  Toast.makeText(context, "ohhk", Toast.LENGTH_SHORT).show();
        HashMap<String,String> map=new HashMap<>();
        if (canGetLocation()) {
            double latitude = location.getLatitude();
            double longitude =location.getLongitude();
            try {
                Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses.size() > 0) {
                    String cityName = addresses.get(0).getLocality();
                    String stateName = addresses.get(0).getAdminArea();
                    String s = "Address Line: "
                            + addresses.get(0).getAddressLine(0) + "\n"
                            + addresses.get(0).getFeatureName() + "\n"
                            + "Locality: "
                            + addresses.get(0).getLocality() + "\n"
                            + addresses.get(0).getPremises() + "\n"
                            + "Admin Area: "
                            + addresses.get(0).getAdminArea() + "\n"
                            + "Country code: "
                            + addresses.get(0).getCountryCode() + "\n"
                            + "Country name: "
                            + addresses.get(0).getCountryName() + "\n"
                            + "Phone: " + addresses.get(0).getPhone()
                            + "\n" + "Postbox: "
                            + addresses.get(0).getPostalCode() + "\n"
                            + "SubLocality: "
                            + addresses.get(0).getSubLocality() + "\n"
                            + "SubAdminArea: "
                            + addresses.get(0).getSubAdminArea() + "\n"
                            + "SubThoroughfare: "
                            + addresses.get(0).getSubThoroughfare()
                            + "\n" + "Thoroughfare: "
                            + addresses.get(0).getThoroughfare() + "\n"
                            + "URL: " + addresses.get(0).getUrl();

                    Log.e("locationstate","locationstate"+s);
                    // Toast.makeText(context, "city:::::statename" + cityName+""+stateName, Toast.LENGTH_SHORT).show();

                    CommonUtils.savePreferencesString(mContext, AppConstants.CurrentCity, cityName);
                    CommonUtils.savePreferencesString(mContext, AppConstants.StateName, stateName);


                    CommonUtils.savePreferencesString(mContext, AppConstants.CURRENT_LAT, String.valueOf(latitude));
                    CommonUtils.savePreferencesString(mContext, AppConstants.CURRENT_LONGI, String.valueOf(longitude));

                    map.put("1",cityName);
                    map.put("2",stateName);

                    Log.e("CurrentCity","CurrentCity"+cityName);
                    Log.e("CurrentCity","Currentstate"+stateName);

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            Log.e("NotLocation","NotLocation");

        }
        return map;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location=location;


//        mContext.getSharedPreferences("username",0).edit().putString("location",location.getLatitude()+":"+location.getLongitude()).commit();
//        Log.e("locationB", mContext.getSharedPreferences("username",0).getString("location",null));

    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*@Override
    public IBinder onBind(Intent arg0) {
        return null;
    }*/


}