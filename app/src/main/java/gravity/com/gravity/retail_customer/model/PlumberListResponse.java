package gravity.com.gravity.retail_customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlumberListResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("response")
    @Expose
    private List<PlumberListDetailsModel> PlumberListDetailsModel = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<PlumberListDetailsModel> getPlumberListDetailsModel() {
        return PlumberListDetailsModel;
    }

    public void setPlumberListDetailsModel(List<PlumberListDetailsModel> PlumberListDetailsModel) {
        this.PlumberListDetailsModel = PlumberListDetailsModel;
    }

}
