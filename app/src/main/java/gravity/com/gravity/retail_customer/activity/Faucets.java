package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.FaucetAdapterLinear;
import gravity.com.gravity.retail_customer.adapter.FaucetsAdapter;
import gravity.com.gravity.retail_customer.fragment.BottomSheetFragment;
import gravity.com.gravity.retail_customer.model.NewProductModel;
import gravity.com.gravity.utils.SpacesItemDecoration;

/**
 * Created by Admin on 2/27/2018.
 */

public class Faucets extends Fragment {
    RecyclerView recyclerView;
    FaucetsAdapter faucetsAdapter;
    FaucetAdapterLinear faucetAdapterLinear;
    NewProductModel newProductModel;
    ArrayList<NewProductModel> newProductArrayList = new ArrayList<>();
    Context context;
    ImageView back;
    NestedScrollView nested_scrollview;
    AppBarLayout app_bar;
    TextView txt1;
    boolean status = true;
    public static int position;

    ImageView imglist_faucets;
    BottomSheetBehavior bottomSheetBehavior;
    CoordinatorLayout coordinatorLayout;
    RelativeLayout relativeLayout;
    int count = 0;
    ImageView img_sort;
    RadioGroup radiogrp;
    RelativeLayout list_grid;
    RelativeLayout sort_faucet;

    private  BottomSheetFragment bottomSheetFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_faucets,container,false);


  /*  back = view.findViewById(R.id.back);*/
        img_sort = view.findViewById(R.id.img_sort);
        recyclerView = view.findViewById(R.id.a_faucets_recylerview);
        relativeLayout = view.findViewById(R.id.relative_sort);

        imglist_faucets = view.findViewById(R.id.imglist_faucets);
        txt1 = view.findViewById(R.id.txt1);
        list_grid = view.findViewById(R.id.list_grid);
        sort_faucet = view.findViewById(R.id.sort_faucet);

        final RadioButton radioButton = view.findViewById(R.id.rb_1);
        RadioButton radioButton2 = view.findViewById(R.id.rb_2);
        RadioButton radioButton3 = view.findViewById(R.id.rb_3);
        RadioButton radioButton4 = view.findViewById(R.id.rb_4);
        //   coordinatorLayout = findViewById(R.id.coordinator);

        context = getActivity();
        setUiData();
        txt1.setText("grid");
        imglist_faucets.setImageResource(R.mipmap.grid_new);
        faucetsAdapter = new FaucetsAdapter(newProductArrayList, context, count);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerView.addItemDecoration(new SpacesItemDecoration(2));
        recyclerView.setAdapter(faucetsAdapter);


       /* back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        app_bar = findViewById(R.id.app_bar);*/


        list_grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (status) {
                    setUiData();
                    txt1.setText("list");
                    imglist_faucets.setImageResource(R.mipmap.list);

                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    faucetAdapterLinear = new FaucetAdapterLinear(newProductArrayList, context, count);

                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(layoutManager);

                    recyclerView.setAdapter(faucetAdapterLinear);
                   /* faucetsAdapter = new FaucetsAdapter(newProductArrayList, context, count);
                    recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                    recyclerView.addItemDecoration(new SpacesItemDecoration(2));
                    recyclerView.setAdapter(faucetsAdapter);
                   */
                    status = false;
                } else {
                    setUiData();
                    txt1.setText("grid");
                    imglist_faucets.setImageResource(R.mipmap.grid_new);

                    faucetsAdapter = new FaucetsAdapter(newProductArrayList, context, count);
                    recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                    recyclerView.addItemDecoration(new SpacesItemDecoration(2));
                    recyclerView.setAdapter(faucetsAdapter);
                    status = true;

                }
            }
        });

        sort_faucet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

             /*   int selectedid = radiogrp.getCheckedRadioButtonId();

                if (selectedid == R.id.rb_1) {
                    position = 0;
                } else if (selectedid == R.id.rb_2) {
                    position = 1;
                }

                else if(selectedid==R.id.rb_3){
                    position=2;
                }
                else if(selectedid==R.id.rb_4){
                    position=3;
                }
*/
                BottomSheetFragment fragment = new BottomSheetFragment();
                
                fragment.show(getFragmentManager(), "rj");



            }
        });


        nested_scrollview = view.findViewById(R.id.nested_scrollview);


        nested_scrollview.getParent().requestChildFocus(nested_scrollview, nested_scrollview);


        nested_scrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            }
        });

return view;
    }

    private void setUiData() {

        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_t);
        newProductModel.setProductName("Tariam");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("50% off");
        newProductArrayList.add(newProductModel);

        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("Bathyrom Accesories");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.homec);
        newProductModel.setProductName("Telephonic shower");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_t);
        newProductModel.setProductName("taps");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);

        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("Tariam");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("Bathyrom Accesories");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.homec);
        newProductModel.setProductName("Telephonic shower");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("taps");
        newProductModel.setProduct_price("₹ 1296");
        newProductModel.setProduct_rating_star("4.1");
        newProductModel.setProduct_discount("56% off");
        newProductArrayList.add(newProductModel);

    }
}
