package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.ProductCategoryFaucetsAdapter;
import gravity.com.gravity.retail_customer.model.NewProductModel;
import gravity.com.gravity.utils.SpacesItemDecoration;

/**
 * Created by Admin on 3/8/2018.
 */

public class ProductCategoryActivity extends Fragment {
    RecyclerView faucetsRecyView, showersRecyView, accessoriesRecycView, specialPurRecyView;
    ArrayList<NewProductModel> faucetsArrayList;
    NewProductModel faucetsModel;
    ProductCategoryFaucetsAdapter faucetsAdapter;
    Context context;
    ImageView back;
    ImageView a_pro_categry_search;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_product_category,container,false);

        faucetsRecyView = view.findViewById(R.id.a_pro_cat_recy_faucets);
        showersRecyView = view.findViewById(R.id.a_pro_cat_recy_showers);
        accessoriesRecycView = view.findViewById(R.id.a_pro_cat_recy_accessories);
        specialPurRecyView = view.findViewById(R.id.a_pro_cat_recy_s_purpose_taps);
        //  a_pro_categry_search = findViewById(R.id.a_pro_categry_search);
      /*  a_pro_categry_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context,SearchBar.class));
            }
        });
*/

        context =getActivity();
     /*  back=findViewById(R.id.back);
       back.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               onBackPressed();
           }
       });*/

        setFaucets();
        setShowers();
        setAccesssories();
        setSpecialPurposeTap();
return view;
    }


    private void setSpecialPurposeTap() {
        faucetsArrayList=new ArrayList<>();

        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Tariam");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Bathyrom ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.homec);
        faucetsModel.setProductName("Telephonic ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("taps");
        faucetsArrayList.add(faucetsModel);

        faucetsAdapter=new ProductCategoryFaucetsAdapter(faucetsArrayList,context);
        specialPurRecyView.setLayoutManager(new LinearLayoutManager(context));

        specialPurRecyView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        specialPurRecyView.addItemDecoration(new SpacesItemDecoration(7, 0));

        specialPurRecyView.setAdapter(faucetsAdapter);

    }

    private void setAccesssories() {
        faucetsArrayList=new ArrayList<>();

        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Tariam");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Bathyrom ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.homec);
        faucetsModel.setProductName("Telephonic ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("taps");
        faucetsArrayList.add(faucetsModel);

        faucetsAdapter=new ProductCategoryFaucetsAdapter(faucetsArrayList,context);
        accessoriesRecycView.setLayoutManager(new LinearLayoutManager(context));

        accessoriesRecycView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        accessoriesRecycView.addItemDecoration(new SpacesItemDecoration(7, 0));

        accessoriesRecycView.setAdapter(faucetsAdapter);

    }

    private void setShowers() {
        faucetsArrayList=new ArrayList<>();

        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Tariam");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Bathyrom ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.homec);
        faucetsModel.setProductName("Telephonic ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("taps");
        faucetsArrayList.add(faucetsModel);

        faucetsAdapter=new ProductCategoryFaucetsAdapter(faucetsArrayList,context);
        showersRecyView.setLayoutManager(new LinearLayoutManager(context));

        showersRecyView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        showersRecyView.addItemDecoration(new SpacesItemDecoration(7, 0));

        showersRecyView.setAdapter(faucetsAdapter);

    }

    private void setFaucets() {
        faucetsArrayList=new ArrayList<>();

        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Tariam");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("Bathyrom ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.homec);
        faucetsModel.setProductName("Telephonic ");
        faucetsArrayList.add(faucetsModel);


        faucetsModel = new NewProductModel();
        faucetsModel.setImgID(R.drawable.home_a);
        faucetsModel.setProductName("taps");
        faucetsArrayList.add(faucetsModel);

        faucetsAdapter=new ProductCategoryFaucetsAdapter(faucetsArrayList,context);
        faucetsRecyView.setLayoutManager(new LinearLayoutManager(context));

        faucetsRecyView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        faucetsRecyView.addItemDecoration(new SpacesItemDecoration(7, 0));

        faucetsRecyView.setAdapter(faucetsAdapter);

    }
}
