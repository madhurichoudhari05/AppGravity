package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.Latest_adapter;
import gravity.com.gravity.retail_customer.model.LatestNewsModel;

/**
 * Created by Admin on 3/6/2018.
 */

public class LatestNewsActivity extends Fragment {

    RecyclerView recyclerView;
    Latest_adapter latest_adapter;
    ArrayList<LatestNewsModel> arrayList = new ArrayList();
    LatestNewsModel latestNewsModel;
    Context context;
    ImageView back;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_latest_news,container,false);

  // context=getActivity();
        back=view.findViewById(R.id.back);


        recyclerView = view.findViewById(R.id.recycler_latest_news);
        latest_adapter = new Latest_adapter(arrayList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(latest_adapter);


        for (int i = 0; i < 10; i++) {


            int[] pic_latest = new int[]{R.drawable.home_a, R.drawable.home_b};

            latestNewsModel = new LatestNewsModel(pic_latest[0], "11/Jan/2018", "5 hours ago", "Sed ut perciciatis unde omnis iste", "Sed ut perciciatis unde omnis iste cmon cmon turn the radio on haha me this side how r u","");

            arrayList.add(latestNewsModel);
            latest_adapter.notifyDataSetChanged();


    }
    return view;
    }

}
