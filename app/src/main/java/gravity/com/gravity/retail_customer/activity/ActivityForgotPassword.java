package gravity.com.gravity.retail_customer.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import com.alimuzaffar.lib.pin.PinEntryEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.fragment.TabFragPlumberSignIn;
import gravity.com.gravity.retail_customer.fragment.TabFragUserSignIn;
import gravity.com.gravity.retail_customer.model.UserModelSignUpModel;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 3/13/2018.
 */

public class ActivityForgotPassword extends AppCompatActivity {
    AppCompatButton authenticate;
    Button varify_acco;
    private APIInterface apiInterface;
    TextInputEditText email;
    FrameLayout container_f_p;
    String user_type = "1";
    TabFragUserSignIn tabFragUserSignIn;
    Context context;
    String passwrd="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        authenticate = findViewById(R.id.authenticate);
        varify_acco = findViewById(R.id.varify_acco);
        container_f_p = findViewById(R.id.container_f_p);
        email = findViewById(R.id.email);
        context=ActivityForgotPassword.this;

        authenticate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email.getText().toString().equals("")){
                    CommonUtils.snackBar("Please Enter Your Email Id",email);
                }
                else {
                    callpasswordApi();
                }

            }
        });
    }

    private void callpasswordApi() {

        if(CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("0")) {

            Call<ResponseBody> call = apiInterface.forgotPassword(
                    email.getText().toString(), "1");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String res = response.body().string();
                        if(res!=null){
                        JSONObject jobj = new JSONObject(res);
                        if(jobj!=null){
                        if (jobj.getString("status").equals("1")) {
                            String passwrd = jobj.getString("msg");

                            dialog(passwrd);
                        }
                        }else {
                            CommonUtils.snackBar("Null response",email);
                        }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
CommonUtils.snackBar("Check Your Internet Connection",email);
                }
            });

        }else if(CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")){

            Call<ResponseBody> call = apiInterface.forgotPassPlumber(
                    email.getText().toString(), "4");
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String res = response.body().string();
                        if (res!=null) {
                            JSONObject jobj = new JSONObject(res);
                            if (jobj.getString("status").equals("1")) {
                                if (jobj.getString("msg") != null) {
                                    passwrd = jobj.getString("msg");
                                }
                                dialog(passwrd);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    CommonUtils.snackBar("Check Your Internet Connection",email);
                }
            });


        }
    }

    private void dialog(String p) {

        final Dialog dialog = new Dialog(ActivityForgotPassword.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = ActivityForgotPassword.this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setLayout(width, height);

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.newpassword_dialog);
        dialog.show();

        Button button = dialog.findViewById(R.id.varify_acco);
        PinEntryEditText txt_pin_entry = dialog.findViewById(R.id.txt_pin_entry);

           txt_pin_entry.setText(p);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("0")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("passwrd", p);
                   // tabFragUserSignIn = new TabFragUserSignIn();
                    //tabFragUserSignIn.setArguments(bundle);
                    Intent i=new Intent(context,SignInSignUpUserActivity.class);
                    i.putExtra("password",p);
                    /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_f_p, tabFragUserSignIn);
                    transaction.commit();*/
                    startActivity(i);
                    finish();
                    dialog.dismiss();
                    //startActivity(new Intent(ActivityForgotPassword.this, UserHomeActivity.class));
                } else if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("passwrd", p);
                   /* TabFragPlumberSignIn    fragment = new TabFragPlumberSignIn();
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_f_p, fragment);
                    transaction.commit();*/
                    Intent i=new Intent(context,PlumberSignInSignUp.class);
                    i.putExtra("password",p);
                    startActivity(i);
                    finish();
                    dialog.dismiss();


                }
            }


        });

    }
}
