package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 3/15/2018.
 */

public class CustomerReviewModel {
    int profile_image,ratingBar_review;
    String txt1_review;
    String txt2_review;
    String txt3_review;
    String txt4_review;
public CustomerReviewModel(){

}

public  CustomerReviewModel(int profile_image,String txt1_review,String  txt2_review,String txt3_review,String txt4_review,int ratingBar_review){
    this.profile_image=profile_image;
    this.txt1_review=txt1_review;
    this.txt2_review=txt2_review;
    this.txt3_review=txt3_review;
    this.txt4_review=txt4_review;
    this.ratingBar_review=ratingBar_review;
}

    public int getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(int profile_image) {
        this.profile_image = profile_image;
    }

    public int getRatingBar_review() {
        return ratingBar_review;
    }

    public void setRatingBar_review(int ratingBar_review) {
        this.ratingBar_review = ratingBar_review;
    }

    public String getTxt1_review() {
        return txt1_review;
    }

    public void setTxt1_review(String txt1_review) {
        this.txt1_review = txt1_review;
    }

    public String getTxt2_review() {
        return txt2_review;
    }

    public void setTxt2_review(String txt2_review) {
        this.txt2_review = txt2_review;
    }

    public String getTxt3_review() {
        return txt3_review;
    }

    public void setTxt3_review(String txt3_review) {
        this.txt3_review = txt3_review;
    }

    public String getTxt4_review() {
        return txt4_review;
    }

    public void setTxt4_review(String txt4_review) {
        this.txt4_review = txt4_review;
    }
}
