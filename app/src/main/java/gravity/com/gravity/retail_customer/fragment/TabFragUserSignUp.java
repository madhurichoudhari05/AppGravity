package gravity.com.gravity.retail_customer.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.retail_customer.model.UserModelSignUpModel;
import gravity.com.gravity.service.Retrofit.model.SignUpModel;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import gravity.com.gravity.utils.FunNoParam;
import gravity.com.gravity.utils.GpsLocationReceiver;
import gravity.com.gravity.utils.gps.GPSTracker2;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 2/15/2018.
 */

public class TabFragUserSignUp extends Fragment {

    private APIInterface apiInterface;
EditText et_password;
    private String otp="";
    private String user_id="";
    Button register;
    EditText et_fname,email_id,et_mob_no;
    ImageView iv_show_pass;
    private boolean isChecked = true;
    String user_type="1",device_type ="Android",device_token="",latitude="123",longitude="1212";
    GpsLocationReceiver receiver;
    GPSTracker2 tracker;
    Location loc;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private Context context;
    String email="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_frag_user_signup, container, false);

        context = getActivity();
        apiInterface= APIClient.getClient().create(APIInterface.class);
        et_mob_no = view.findViewById(R.id.et_mob_no);
        email_id = view.findViewById(R.id.email_id);
        et_fname = view.findViewById(R.id.et_fname);
        iv_show_pass = view.findViewById(R.id.iv_show_pass);
        et_password = view.findViewById(R.id.et_password);
        register=view.findViewById(R.id.btn_tab_f_siup);
        tracker = new GPSTracker2(getActivity());
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkValidation();
            }
        });
        iv_show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isChecked) {
                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye1);
                    isChecked = true;
                } else {
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye);
                    isChecked = false;
                }

            }
        });
/*
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // /otpDialog(String otpl);
                Toast.makeText(getActivity(), "dialog", Toast.LENGTH_SHORT).show();

            }
        });
*/

        return view;
    }

    private void checkValidation() {

        String fullname = et_fname.getText().toString();
        email=email_id.getText().toString();
       // email = email.replaceAll("\\s+","");

        if (TextUtils.isEmpty(et_fname.getText().toString())) {
            CommonUtils.snackBar("  Please Enter First Name.", et_fname);
        } else if (!fullname.matches("[a-zA-Z ]+")) {
            CommonUtils.snackBar("Name Accept Alphabets Only.", et_fname);
        }
        else if (TextUtils.isEmpty(email_id.getText())) {

            CommonUtils.snackBar("Please Enter Your email.", et_fname);
        }else if(!email.matches(emailPattern)&&email.length()>0){
            CommonUtils.snackBar("Please Enter Right Email Address.", et_fname);

        }

        else if (TextUtils.isEmpty(et_mob_no.getText().toString())) {
            CommonUtils.snackBar("Please Enter Phone Number.", et_fname);
        }
        else if (et_mob_no.getText().toString().length() < 10) {
            CommonUtils.snackBar("Mobile Number Should Be of 10 Digits.", et_fname);
        }
        else if (et_mob_no.getText().toString().length() > 10) {
            CommonUtils.snackBar("Mobile Number Should Be of 10 Digits.", et_fname);
        }
        else if (TextUtils.isEmpty(et_password.getText())) {

            CommonUtils.snackBar("Please Enter Your Password.", et_fname);
        }


        else if ((et_password.getText().toString().length() < 4)) {
            CommonUtils.snackBar("Password Must Be AtLeast 4 Character.", et_fname);

        }
        else {



            if(CommonUtils.getPreferences(getActivity(),AppConstants.CURRENT_LAT)!=null&&CommonUtils.getPreferences(context,AppConstants.CURRENT_LONGI)!=null) {



                latitude=CommonUtils.getPreferences(context,AppConstants.CURRENT_LAT);
                longitude=CommonUtils.getPreferences(context,AppConstants.CURRENT_LONGI);
                //  Toast.makeText(getActivity(),longitude  , Toast.LENGTH_SHORT).show();
                // Toast.makeText(getActivity(), place.getName() + ":::" + address, Toast.LENGTH_SHORT).show();
                Log.e("Currentlat",latitude);
                Log.e("Currentlongitude",longitude);

                // map.addMarker(new MarkerOptions().position(str));
            }

           callSignUApi();

        }
    }



    private void callSignUApi() {

            Call<UserModelSignUpModel> call=apiInterface.signUpUser(
                    et_fname.getText().toString(),
                    email,
                    et_mob_no.getText().toString(),
                    et_password.getText().toString(),
                    user_type,
                    device_type,
                    FirebaseInstanceId.getInstance().getToken(),
                    latitude,
                    longitude);
        call.enqueue(new Callback<UserModelSignUpModel>() {
            @Override
            public void onResponse(Call<UserModelSignUpModel> call, Response<UserModelSignUpModel> response) {
                UserModelSignUpModel signUpModel = response.body();
                if (signUpModel != null ) {
                    if (signUpModel.getStatus().equals(1)) {
                        // CommonUtils.snackBar(signUpModel.getMsg(), et_email);
                        if (signUpModel.getResponse()!=null&&signUpModel.getResponse().size()>0) {
                            otp = String.valueOf(signUpModel.getOTP());
                            user_id = String.valueOf(signUpModel.getResponse().get(0).getId());
                            CommonUtils.savePreferencesString(getActivity(), AppConstants.USER_ID,user_id);
                            CommonUtils.savePreferencesString(getContext(),AppConstants.USER_NAME,signUpModel.getResponse().get(0).getName());
                            Toast.makeText(getContext(), "otp is :" + signUpModel.getOTP(), Toast.LENGTH_SHORT).show();
                            Log.e("otpLogin", "otp is :" + otp);
                            otpDialog(otp);

                        }
                        else {

                            Toast.makeText(getContext(), "otp null" + signUpModel.getOTP(), Toast.LENGTH_SHORT).show();
                            Log.e("signUp","signdata"+et_mob_no);

                        }

                    }
                    else   if (signUpModel.getStatus().equals(2) ) {
                        Toast.makeText(getContext(), "status code 2"+signUpModel.getMsg(), Toast.LENGTH_SHORT).show();
                        CommonUtils.snackBar(signUpModel.getMsg(), email_id);
                    }
                    else {
                        Toast.makeText(getContext(), "else part"+signUpModel.getMsg(), Toast.LENGTH_SHORT).show();
                        CommonUtils.snackBar(signUpModel.getMsg(), email_id);
                    }
                }
                else {

                    Toast.makeText(getContext(), "No Record found ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserModelSignUpModel> call, Throwable throwable) {
                Log.d("ASD", "onFailure: ");
            }
        });
    }




    private void otpDialog(String otp1) {

        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = getActivity().getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setLayout(width, height);

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialoge_otp);
        dialog.show();

        Button button=dialog.findViewById(R.id.varify_acco);
        PinEntryEditText txt_pin_entry = dialog.findViewById(R.id.txt_pin_entry);

        txt_pin_entry.setText(otp1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesBoolean(context,AppConstants.GUESR_USER,true);
                CommonUtils.savePreferencesString(context,AppConstants.ALL_USER,"0");
                startActivity(new Intent(getActivity(), UserHomeActivity.class));
            }
        });



    }


    @Override
    public void onResume() {
        super.onResume();
        if (CommonUtils.getPreferencesString(getActivity(), AppConstants.ALL_USER).equalsIgnoreCase("2")) {
            checkPermission();
            if (receiver != null) {
                checkGpsEnable();
            }


        }
    }

    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(getActivity(), per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    protected void checkPermission() {

        if (isPerGiven(Manifest.permission.ACCESS_FINE_LOCATION)
                && isPerGiven(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            return;
        }

        String[] arr = new String[2];
        arr[0] = Manifest.permission.ACCESS_FINE_LOCATION;
        arr[1] = Manifest.permission.ACCESS_COARSE_LOCATION;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arr, 1);

        }
    }


    private boolean checkGpsEnable() {
        if (!tracker.isGPSEnabled()) {
            setReciever();
            tracker.showSettingsAlert();
            return false;
        } else {
            tracker.dismissAlert();
            loc=tracker.getLocation();

            Timer t = new Timer();
            t.scheduleAtFixedRate(
                    new TimerTask() {
                        @Override
                        public void run() {

                            if(loc==null){
                                loc=tracker.getLocation();
                            }else {
                                HashMap<String, String> map = tracker.saveLocation(loc);
                                // city = map.get("1");
                                //  state = map.get("2");


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //updateAdapters();



                                    }
                                });
                            }

                            t.cancel();

                        }

                    },
                    0,
                    1000);


            return true;
        }
    }

    private void setReciever() {
        if (receiver != null) {
            return;
        }
        receiver = new GpsLocationReceiver();
        receiver.setmListener(new FunNoParam() {
            @Override
            public void done() {



            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        getActivity().registerReceiver(receiver, filter);
    }


}
