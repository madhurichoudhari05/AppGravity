package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.SignupSignInUserAdapter;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by user on 2/15/2018.
 */

public class SignInSignUpUserActivity extends AppCompatActivity {
    ImageView back;
    LinearLayout linear_signin_signup;
    Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_signup_user);
        back = findViewById(R.id.imageView);
        context = SignInSignUpUserActivity.this;
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("SIGN IN"));
        tabLayout.addTab(tabLayout.newTab().setText("SIGN UP"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        back = findViewById(R.id.imageView);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        linear_signin_signup = findViewById(R.id.linear_signin_signup);
        linear_signin_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesBoolean(context, AppConstants.GUESR_USER, false);

                startActivity(new Intent(SignInSignUpUserActivity.this, UserHomeActivity.class));


            }
        });

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        final SignupSignInUserAdapter adapter = new SignupSignInUserAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText() == "SIGN UP") {
                    back.setVisibility(View.VISIBLE);
                } else {
                    back.setVisibility(View.INVISIBLE);
                }

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
