package gravity.com.gravity.retail_customer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.FragDealerListAdapter;
import gravity.com.gravity.retail_customer.model.DealerListModel;

/**
 * Created by Admin on 3/5/2018.
 */

public class TabFragmentListView extends Fragment {
    RecyclerView recyclerView;
    FragDealerListAdapter adapter;
    DealerListModel listModel;
    ArrayList<DealerListModel> arrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_listview, container, false);

        recyclerView=view.findViewById(R.id.f_tab_list_recyclerview);
        arrayList=new ArrayList<>();

        for(int i=0;i<10;i++){
            listModel=new DealerListModel();
            listModel.setProfile_pic(R.drawable.man_t);
            listModel.setDealer_name("Rajesh");
            listModel.setDealer_contact("9810238765");
            listModel.setDealer_distaqnce("8 KM");
            arrayList.add(listModel);
        }

        adapter=new FragDealerListAdapter(arrayList,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);


        return view;
    }

}
