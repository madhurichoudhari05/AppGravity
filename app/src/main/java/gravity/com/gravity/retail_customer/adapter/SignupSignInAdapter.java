package gravity.com.gravity.retail_customer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import gravity.com.gravity.retail_customer.fragment.TabFragmentSignIn;
import gravity.com.gravity.retail_customer.fragment.TabFragmentSignUp;

/**
 * Created by user on 2/15/2018.
 */

public class SignupSignInAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;


    public SignupSignInAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentSignIn tab1 = new TabFragmentSignIn();
                //TabFragUserSignIn tab1=new TabFragUserSignIn();
                return tab1;
            case 1:
                TabFragmentSignUp tab2 = new TabFragmentSignUp();

               // TabFragUserSignUp tab2=new TabFragUserSignUp();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
