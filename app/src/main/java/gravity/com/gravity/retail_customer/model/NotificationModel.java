package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 2/28/2018.
 */

public class NotificationModel {
    int notification_icon,pic;
    String title;

    public int getNotification_icon() {
        return notification_icon;
    }

    public void setNotification_icon(int notification_icon) {
        this.notification_icon = notification_icon;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String message;
    String time;


}
