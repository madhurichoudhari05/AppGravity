package gravity.com.gravity.retail_customer.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import gravity.com.gravity.R;

/**
 * Created by admin on 3/19/2018.
 */

public class NewsDetailActivity extends Fragment {

    ImageView back_newsdetails;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.newsdetail,container,false);
        return view;
    }
}
