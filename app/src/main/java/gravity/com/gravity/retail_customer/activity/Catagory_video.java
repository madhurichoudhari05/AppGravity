package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.CatagoryAdapter;
import gravity.com.gravity.retail_customer.model.CatagoryModel;
import gravity.com.gravity.retail_customer.model.ItemoffsetDecoratorVedio;

public class Catagory_video extends AppCompatActivity {
    ImageView back_vedio_catagory;
    RecyclerView recyclerView;
    CatagoryAdapter catagory_adapter;
    ArrayList<CatagoryModel> arrayList = new ArrayList<>();
    CatagoryModel catagory_pojo;
    Context context;
    int spanCount = 3; // 3 columns
    int spacing = 5; // 50px

    ImageView imageView;
    boolean includeEdge = true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.catagory_video);
        back_vedio_catagory = findViewById(R.id.back_vedio_catagory);
        back_vedio_catagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerView = findViewById(R.id.recycler_catagory);

/*

        imageView =findViewById(R.id.back_catagory1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
*/

        catagory_adapter = new CatagoryAdapter(arrayList, context);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        recyclerView.addItemDecoration(new ItemoffsetDecoratorVedio(spanCount, spacing, includeEdge));
        recyclerView.setAdapter(catagory_adapter);
        for (int i = 0; i < 8; i++) {

            int[] pic = new int[]{R.drawable.catogory_new,R.drawable.ic_play_arrow_black_24dp};
            int[] pic1= new int[]{R.drawable.home_b};
            CatagoryModel pojo = new CatagoryModel(pic[0],pic[1], "Catagory 1.1", "konuid is here to see","");
            arrayList.add(pojo);
            catagory_adapter.notifyDataSetChanged();
        }

    }
}
