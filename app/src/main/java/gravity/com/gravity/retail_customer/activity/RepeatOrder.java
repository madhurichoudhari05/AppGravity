package gravity.com.gravity.retail_customer.activity;

import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.RepeatOrderAdapter;

public class RepeatOrder extends AppCompatActivity {

    ViewPager viewPager;
    private static int currentPage = 0;
    TextView textView;
    ImageView back;
    private static int NUM_PAGES = 0;
    private  static Integer[] imgs = new Integer[]{R.drawable.home_a, R.drawable.home_b, R.drawable.homec};
    private ArrayList<Integer> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repeat_order);

        textView = findViewById(R.id.rupee_repeat_order);
        textView.setText("₹ 1396");


        back = findViewById(R.id.back_product_details);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imageSlider();

    }

    private void imageSlider(){

        for (int i = 0; i < imgs.length; i++) list.add(imgs[i]);


        viewPager = (ViewPager) findViewById(R.id.pager_repeatorder);

        viewPager.setAdapter(new RepeatOrderAdapter(RepeatOrder.this, list));
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator_repeat_order);
        indicator.setViewPager(viewPager);
        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES = imgs.length;

        // Auto start of viewpager
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }


    }
