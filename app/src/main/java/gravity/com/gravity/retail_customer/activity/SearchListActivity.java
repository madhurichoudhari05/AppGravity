package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.SearchListAdapter;
import gravity.com.gravity.retail_customer.model.SearchListModel;
import gravity.com.gravity.utils.SpacesItemDecoration;

/**
 * Created by Admin on 2/28/2018.
 */

public class SearchListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<SearchListModel> arrayList;
    SearchListAdapter listAdapter;
    Context context;

    SearchListModel listModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list);
        context=SearchListActivity.this;

        recyclerView=findViewById(R.id.a_search_list_recycler);
        arrayList=new ArrayList<>();
        for(int i=0;i<6;i++) {

            listModel = new SearchListModel();
            listModel.setImg_product(R.drawable.home_t);
            listModel.setImg_like(R.mipmap.like);
            listModel.setPro_name("contemparary bathroom faucets");
            listModel.setPro_price("1399");
            listModel.setPro_rating("4.5");

            arrayList.add(listModel);

        }
        listAdapter=new SearchListAdapter(arrayList,context);
        recyclerView.setLayoutManager(new GridLayoutManager(context,2));
        recyclerView.addItemDecoration(new SpacesItemDecoration(5));
        recyclerView.setAdapter(listAdapter);


    }
}
