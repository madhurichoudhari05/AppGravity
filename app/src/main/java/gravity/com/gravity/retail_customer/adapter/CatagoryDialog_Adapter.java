package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;

public class CatagoryDialog_Adapter extends RecyclerView.Adapter<CatagoryDialog_Adapter.InnerClass> {
    ArrayList<String> list;
    Context context;
    int count,count_p;
    setCatagory setCatagory;

    public CatagoryDialog_Adapter(ArrayList<String> list, Context context, setCatagory catagory, int count) {

        this.list = list;
        this.context = context;
        this.setCatagory = catagory;
        this.count = count;
    }

    @Override
    public InnerClass onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catagory_dialog_single, parent, false);
        return new InnerClass(view);
    }

    @Override
    public void onBindViewHolder(InnerClass holder, int position) {
        holder.txt_catagory.setText(list.get(position));

        // holder.txt_catagory.setText(list.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = count;
                setCatagory.catagoryText(position,list, "");
                // setCatagory.subcatagoryText(list.get(position),position);

            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class InnerClass extends RecyclerView.ViewHolder {
        TextView txt_catagory;

        public InnerClass(View itemView) {
            super(itemView);

            txt_catagory = itemView.findViewById(R.id.txt_catagory);
        }
    }

    public interface setCatagory {

        public void catagoryText(int catagory,ArrayList<String> list, String count);
        // public void subcatagoryText(String catagory,int position);
    }
}
