package gravity.com.gravity.retail_customer.model;

public class VideoModel {

    public int pic_video;
    public String txt_video;

    VideoModel() {
    }

    public VideoModel(int pic_video, String txt_video) {

        this.pic_video = pic_video;
        this.txt_video = txt_video;
    }

    public int getPic_video() {
        return pic_video;
    }

    public void setPic_video(int pic_video) {
        this.pic_video = pic_video;
    }

    public String getTxt_video() {
        return txt_video;
    }

    public void setTxt_video(String txt_video) {
        this.txt_video = txt_video;
    }
}

