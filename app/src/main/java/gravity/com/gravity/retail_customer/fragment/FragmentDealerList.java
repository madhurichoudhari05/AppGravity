package gravity.com.gravity.retail_customer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.DealerListTabAdapter;

/**
 * Created by Admin on 3/6/2018.
 */

public class FragmentDealerList extends Fragment {

    Context context;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dealerlist, container, false);

        TabLayout tabLayout=view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("List View"));
        tabLayout.addTab(tabLayout.newTab().setText("Map View"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager)view.findViewById(R.id.pager);

        final DealerListTabAdapter adapter = new DealerListTabAdapter
                (getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        return view;
    }
}