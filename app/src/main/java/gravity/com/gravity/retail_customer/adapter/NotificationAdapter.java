package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.NotificationModel;

/**
 * Created by user on 2/16/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    NotificationModel notiModel;

    ArrayList<NotificationModel> arrayList = new ArrayList<>();

    Context context;

    public NotificationAdapter(ArrayList<NotificationModel> arrayList, Context context) {


        this.arrayList = arrayList;

        this.context = context;

    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notifications_row_item, parent, false);

        NotificationAdapter.ViewHolder recyclerViewHolder = new NotificationAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {

        holder.notifica_icon.setImageResource(arrayList.get(position).getNotification_icon());
        holder.img.setImageResource(arrayList.get(position).getPic());
     holder.heading.setText(arrayList.get(position).getTitle());
     holder.message.setText(arrayList.get(position).getMessage());
     holder.time.setText(arrayList.get(position).getTime());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  notifica_icon,img;
        TextView heading,message,time;

        public ViewHolder(View itemView) {
            super(itemView);

            notifica_icon = (ImageView) itemView.findViewById(R.id.noti_icon);
            img = (ImageView) itemView.findViewById(R.id.img);
            heading=itemView.findViewById(R.id.title);
            message=itemView.findViewById(R.id.content);
            time=itemView.findViewById(R.id.time);

        }
    }
}
