package gravity.com.gravity.retail_customer.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.firebase.FcmMessengerViewModel;
import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;
import gravity.com.gravity.firebase.retofit.ApiInterface;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;

import gravity.com.gravity.firebase.retofit.model.fcmRes.handler.ChstRetrofitHandler;
import gravity.com.gravity.plumber.model.TokenModel;
import gravity.com.gravity.retail_customer.fragment.FragPlumberList;
import gravity.com.gravity.retail_customer.fragment.ShippingLocation;
import gravity.com.gravity.retail_customer.model.PlumberListDetailsModel;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by user on 2/16/2018.
 */

public class FrgPlumberListAdapter extends RecyclerView.Adapter<FrgPlumberListAdapter.ViewHolder> {

    private PlumberListDetailsModel PlumberListDetailsModel;
     private   ArrayList<PlumberListDetailsModel> arrayList;
    public   FcmMessengerViewModel fcmVM;
    private List<UserStatusDto> userList = new ArrayList<>();
    private FragPlumberList fragPlumberList;
    Context context;
    private ApiInterface mInterface;
    private String USER_ID="",complain_id="";
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;


    public FrgPlumberListAdapter(ArrayList<PlumberListDetailsModel> arrayList, Context context, FragPlumberList fragPlumberList, String complain_id) {
        this.arrayList = arrayList;
        this.context = context;
        this.complain_id = complain_id;
        mInterface = ChstRetrofitHandler.getInstance().getApi();
        this.fragPlumberList=fragPlumberList;
        setObserver();


    }




    private void setObserver() {
        if (fcmVM == null) {
            fcmVM = ViewModelProviders.of(fragPlumberList).get(FcmMessengerViewModel.class);


            fcmVM.getRequestStatus().observe(fragPlumberList, integer -> {
                //dismissProg();
                if (integer == 1) {
                    //     CommonUtils.snackBar("Request has been sent",customViewHolder1.tvFinalResult);
                    Toast.makeText(context, "Request has been Sent sucessfully.", Toast.LENGTH_SHORT).show();

                   // CommonUtils.snackBar("Request Sent",);
                    CommonUtils.savePreferencesString(context, AppConstants.PLUMBER_LIST_KEY,"PlumberList");


                    fragmentManager = (((FragmentActivity)context).getSupportFragmentManager());//getSupportFragmentManager();
                   // fragmentManager = (FragmentActivity)context).getChildFragmentManager();;//getSupportFragmentManager();

                    ShippingLocation fragment = new ShippingLocation();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.commit();

                   // context.startActivity(new Intent(context, ShippingLocation.class));

                } else {
                    Toast.makeText(context, "Fail Please try again 2", Toast.LENGTH_SHORT).show();
                }
            });

            fcmVM.getUserAppList(true).observe(fragPlumberList, res -> {
                userList.clear();
                userList.addAll(res);

            });


        }
    }


    @Override
    public FrgPlumberListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;


        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_plumber_list_row_item, parent, false);
        FrgPlumberListAdapter.ViewHolder recyclerViewHolder = new FrgPlumberListAdapter.ViewHolder(view);
        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(FrgPlumberListAdapter.ViewHolder holder, int position) {


        USER_ID = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);


        if (arrayList != null && arrayList.size() > 0) {

            PlumberListDetailsModel = arrayList.get(position);


            if(PlumberListDetailsModel.getFirstName()!=null) {
                holder.dealerName.setText(PlumberListDetailsModel.getFirstName());

            }


            if(!TextUtils.isEmpty(PlumberListDetailsModel.getProfilePic())) {
                Picasso.with(context).load(PlumberListDetailsModel.getProfilePic()).into(holder.profile_pic);

            }


            if(!TextUtils.isEmpty(PlumberListDetailsModel.getPhone())) {
                holder.contactNo.setText(PlumberListDetailsModel.getPhone());

            }


            holder.distance.setText(PlumberListDetailsModel.getDistanceInKm().substring(0,3)+"  Km");



        if (position % 6 == 0) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#81b20c"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#81b20c"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#81b20c"));
        } else if (position % 6 == 1) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FFA500"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#FFA500"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#FFA500"));
        } else if (position % 6 == 2) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#0000FF"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#0000FF"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#0000FF"));
        } else if (position % 6 == 3) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#BDB76B"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#BDB76B"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#BDB76B"));
        } else if (position % 6 == 4) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#00FFFF"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#00FFFF"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#00FFFF"));
        } else if (position % 6 == 5) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#8FBC8F"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#8FBC8F"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#8FBC8F"));
        } else if (position % 6 == 6) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FF0000"));
            setTextViewDrawableColor(holder.contactNo, Color.parseColor("#FF0000"));
            setTextViewDrawableColor(holder.distance, Color.parseColor("#FF0000"));
        }

        holder.btn_send_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                callRequestPush(PlumberListDetailsModel, position);


            }
        });

    }



       // holder.pro_name2.setText(PlumberListDetailsModel.getProductName());


    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  profile_pic;
        RelativeLayout relativeLayout;
        TextView dealerName,contactNo,distance;
        Button btn_send_req;

        public ViewHolder(View itemView) {
            super(itemView);

            profile_pic = (ImageView) itemView.findViewById(R.id.profile_pic);
            dealerName=itemView.findViewById(R.id.name);
            contactNo=itemView.findViewById(R.id.mobileNo);
            distance=itemView.findViewById(R.id.distance);
            relativeLayout=itemView.findViewById(R.id.rl_lauout);
            btn_send_req=itemView.findViewById(R.id.btn_send_req);


        }
    }

    private void  callRequestPush(PlumberListDetailsModel plumberResponse, int position){
/*
        boolean isAvailable = false;
      for (UserStatusDto dto : userList) {
          if (dto.getToId().equals(PlumberListDetailsModel.getId())) {
              isAvailable = true;
          }
      }
        if (isAvailable) {
            Toast.makeText(context, "User already accepted your request", Toast.LENGTH_SHORT).show();
        } else {*/

            if (arrayList.get(position).getId()!=null) {
                Observable<TokenModel> call = mInterface.getFcmToken(arrayList.get(position).getId());
                //  progressDialog = ProgressDialog.show(context, "In Progress", "Please wait...");
                showProg();
                call.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .doFinally(() -> {
                            dismissProg();
                        })
                        .subscribe(responseBody -> {
                                    if (responseBody.getStatus() == 1) {

                                        Log.e("deviectokenPlum",responseBody.getResponse().get(0).getDeviceToken());

                                        if(responseBody.getResponse()!=null&&responseBody.getResponse().size()>0) {

                                            if(responseBody.getResponse().get(0).getDeviceToken()!=null) {

                                                requestForChat(plumberResponse, responseBody.getResponse().get(0).getDeviceToken());

                                            }

                                            else {
                                                Toast.makeText(context, "No Device Token found", Toast.LENGTH_SHORT).show();
                                            }


                                        }
                                    } else {
                                        Toast.makeText(context, "not send", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                e -> {
                                    Toast.makeText(context, "Fail Please try again exception 4", Toast.LENGTH_SHORT).show();
                                });
            }


    }

    private void dismissProg() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    private void showProg() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    /* private void requestForChat(TimeLineBean feedItem1, String fcmToken) {
        FcmMessage msg = new FcmMessage(
                USER_ID,
                CommonUtils.getPreferences(mContext, AppConstants.USER_NAME),
                feedItem1.getUser_id(),
                System.currentTimeMillis() + "",
                FirebaseInstanceId.getInstance().getToken(),
                IConstants.IFcm.FCM_NEW_REQUEST,
                feedItem1.getImage(), feedItem1.getId(),
                feedItem1.getFull_name()

        );


        //     Toast.makeText(homeActivity, "getId"+feedItem.getId(), Toast.LENGTH_SHORT).show();

        showProg();
        fcmVM.sendFcmMessage(msg, fcmToken, true);
    }*/



    private void requestForChat( PlumberListDetailsModel listResponse,String fcmToken) {
        FcmMessage msg = new FcmMessage(
                USER_ID+"",
                CommonUtils.getPreferencesString(context,AppConstants.USER_NAME)+"",
               listResponse.getId()+"",
                System.currentTimeMillis() + "",
                CommonUtils.getPreferences(context,AppConstants.FIREBASE_KEY)+"",
                IConstants.IFcm.FCM_NEW_REQUEST,
                complain_id+"", complain_id+"",
                listResponse.getFirstName()+""

        );
        //Toast.makeText(context, "firebase ", Toast.LENGTH_SHORT).show();


        Log.e("requestDeviectOken1",FirebaseInstanceId.getInstance().getToken());
        Log.e("requestDeviectOken2",fcmToken);
        fcmVM.sendFcmMessage(msg, fcmToken, true);

    }

    ProgressDialog progressDialog;
    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter((color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
