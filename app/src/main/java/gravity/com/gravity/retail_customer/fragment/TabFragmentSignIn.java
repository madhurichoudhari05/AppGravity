package gravity.com.gravity.retail_customer.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.ActivityForgotPassword;

/**
 * Created by user on 2/15/2018.
 */

public class TabFragmentSignIn extends Fragment {

    TextView forgotPass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_signin, container, false);
        forgotPass=view.findViewById(R.id.f_pass);
        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ActivityForgotPassword.class));
            }
        });


        return view;
    }

}
