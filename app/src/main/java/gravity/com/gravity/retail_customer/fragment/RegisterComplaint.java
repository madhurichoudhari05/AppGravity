package gravity.com.gravity.retail_customer.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;


import gravity.com.gravity.DataInterface.FileUploadInterface;
import gravity.com.gravity.DataInterface.StateInterface;
import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.AadharDetail;
import gravity.com.gravity.plumber.adapter.StateAdapter;
import gravity.com.gravity.retail_customer.adapter.CatagoryDialog_Adapter;
import gravity.com.gravity.retail_customer.adapter.CategoryAdapter;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.service.Retrofit.retrofit.MadhuFileUploader;
import gravity.com.gravity.service.Retrofit.retrofit.RetrofitHandler;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

public class RegisterComplaint extends Fragment implements CatagoryDialog_Adapter.setCatagory {

    private static final int REQUEST_PERMISION = 200;
    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    private static final int RESULT_LOAD_IMAGE = 100;
    CardView catagory, subcatagory, product_name;
    AlertDialog.Builder builder, builder_subcatagory;
    AlertDialog dialog, alertDialog;
    TextView catogory_txt;
    TextView faucets_dialog, shower_dialog, accessories_dialog, spacialpurposetaps_dialog;
    private APIInterface apiInterface;
    ArrayList<String> catagorylist;
    int catagory_id = 0, sub_cat_id = 0;
    TextView txt_subcatagory;
    RadioButton yes_radio, no_radio;
    LinearLayout uploadbill, upload_warrantyCard;
    ProgressDialog progressDialog;
    int count = 0;
    int REQUEST_CAMARA = 1, SELECT_FILE;
    Button upload_bill, warranty_card;
    Button btn_submit;
    String img_path;
    String USER_ID = "";
    EditText et_description;
    TextView tv_product_name;
    RadioGroup rg_payment_option, rg_warranty;
    RadioButton radioButton;
    String payment_option = "", warranty = "";

    Context context;
    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;
    private String[] PERMISSION_CAMERA = {Manifest.permission.CAMERA};
    private ImageView img_bill;
    private String path;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};
    private  Uri selectedImage=null;
    private String picturePath="";
    EditText et_state;
    ArrayList<String> category_list,subCategory_list,product_list;
    CategoryAdapter categoryAdapter;

    String categoryID="",subCategoryID="",productID="";
    ArrayList<String> category_list_id,sub_category_list_id,product_list_id;

    @Nullable
    @Override

    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_complaint, container, false);
        catagory = view.findViewById(R.id.catagory);
        txt_subcatagory = view.findViewById(R.id.txt_subcatagory);
        catogory_txt = view.findViewById(R.id.catogory_txt);
        subcatagory = view.findViewById(R.id.subcatagory);
        product_name = view.findViewById(R.id.product_name);
        yes_radio = view.findViewById(R.id.yes_radio);
        no_radio = view.findViewById(R.id.no_radio);
        upload_warrantyCard = view.findViewById(R.id.upload_warrantyCard);
        uploadbill = view.findViewById(R.id.uploadbill);
        upload_bill = view.findViewById(R.id.upload_bill);
        warranty_card = view.findViewById(R.id.warranty_card);
        btn_submit = view.findViewById(R.id.btn_submit);
        tv_product_name = view.findViewById(R.id.et_product_name);
        et_description = view.findViewById(R.id.et_description);
        img_bill = view.findViewById(R.id.img_bill_new);
        //  rg_payment_option = view.findViewById(R.state_id.rg_payment_option);

        rg_warranty = view.findViewById(R.id.rg_warranty);
        context = getActivity();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        USER_ID = CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_ID);
        Log.d("USER_ID", USER_ID);

/*
        subcatagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSubcatagoryDialog();
            }
        });
*/
        yes_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadbill.setVisibility(View.VISIBLE);
                // upload_warrantyCard.setVisibility(View.VISIBLE);
            }
        });
        no_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadbill.setVisibility(View.GONE);

                //  uploadbill.setVisibility(View.VISIBLE);
                //  upload_warrantyCard.setVisibility(View.GONE);
            }
        });
        catagory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = count + 1;
                CommonUtils.savePreferencesString(getActivity(), AppConstants.CATEGORY, "category");
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("loading");
                progressDialog.show();
                callCatagoryApi();



            }
        });





        rg_warranty.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                warranty =
                        ((RadioButton) getView().findViewById(rg_warranty.getCheckedRadioButtonId()))
                                .getText().toString();
                // Toast.makeText(getActivity(), warranty, Toast.LENGTH_SHORT).show();
                Log.d("RadioButton", warranty);

            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  fillAddress();
                checkValidation();

            }
        });

        subcatagory.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesString(getActivity(), AppConstants.CATEGORY, "sub_category");
                count = count + 1;
                if (categoryID != "") {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("loading");
                    progressDialog.show();

                    callSubcatagoryApi();
                    //dialog.show();
                } else {
                    CommonUtils.snackBar("Please select catagory first", catogory_txt);
                }
            }
        });

        product_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (subCategoryID != "") {
                    CommonUtils.savePreferencesString(getActivity(), AppConstants.CATEGORY, "product");
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("loading");
                    progressDialog.show();

                    ProductApi();
                } else {
                    CommonUtils.snackBar("Please select subcategory first", catogory_txt);
                }
            }
        });

        upload_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!hasPermissions(context, PERMISSIONS)) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(PERMISSIONS,REQUEST_PERMISION);
                    }

                    // this.requestPermissions(getActivity(), PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else {

                   SelectImage();

                }
            }
        });



        warranty_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadWarrantyCard();
            }
        });
        return view;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void checkValidation() {
        if (TextUtils.isEmpty(catogory_txt.getText().toString()) && catogory_txt.getHint().toString().equalsIgnoreCase("Select Category")) {
            CommonUtils.snackBar("  Please Select Category.", catogory_txt);
        } else if (TextUtils.isEmpty(txt_subcatagory.getText().toString()) && txt_subcatagory.getHint().toString().equalsIgnoreCase("Select Sub-Category")) {
            CommonUtils.snackBar("Please Select Sub-Category", txt_subcatagory);
        }  else if (TextUtils.isEmpty(tv_product_name.getText().toString())&& tv_product_name.getHint().toString().equalsIgnoreCase("Select Product ")) {
            CommonUtils.snackBar("Please Select the Product Name", txt_subcatagory);
        } else if (TextUtils.isEmpty(et_description.getText().toString())) {
            CommonUtils.snackBar("Please Enter Description of the Product", txt_subcatagory);
        } else {
            openLocationform();
        }

    }

    private void openLocationform() {

        ShippingLocation shippingLocation = new ShippingLocation();
        Bundle bundle = new Bundle();
        bundle.putString("CATEGORY", catogory_txt.getText().toString());
        bundle.putString("SUB_CATEGORY", txt_subcatagory.getText().toString());
        bundle.putString("PRODUCT_NAME", tv_product_name.getText().toString());
        bundle.putString("PRODUCT_DESCRIPTION", et_description.getText().toString());
        bundle.putString("BillImage", picturePath);
        bundle.putString("WARRENT", warranty);
        //  Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.my_drawable);//bundle.putString("WARRANTY_CARD",hashMap);
        shippingLocation.setArguments(bundle);
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        //fragmentTransaction =getActivity().getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, shippingLocation);
        fragmentTransaction.addToBackStack(null);

        //  fragmentTransaction.add(R.state_id.container,shippingLocation,"ASD");

        fragmentTransaction.commit();

    }


    private void uploadWarrantyCard() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        startActivityForResult(intent, RESULT_LOAD_IMAGE);

    }




    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private String getRealPathFromURIPath(Uri contentURI, FragmentActivity activity) {

        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void SelectImage() {
        final CharSequence[] items = {"File Manager", "Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Upload Documents");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (items[i].equals("File Manager")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("file/*");
                    startActivityForResult(intent, RESULT_LOAD_IMAGE);
                } else if (items[i].equals("Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "selecte file"), RESULT_LOAD_IMAGE);
                } else if (items[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == getActivity().RESULT_OK && null != data) {

            galleryOperation(data);

           selectedImage = data.getData();

          //  Toast.makeText(context, "path"+selectedImage, Toast.LENGTH_SHORT).show();
            Log.e("path","FileManage::::"+selectedImage);


            Bitmap bitmap2=CommonUtils.getBitMapFromImageURl(picturePath, (Activity) context);


            //  img_path =   selectedImage.getEncodedPath();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
               // Bitmap bitmap= (Bitmap) data.getExtras().get("data");

                img_bill.setImageBitmap(bitmap2);

                Log.e("path","FileManage::::"+selectedImage);
              //  img_bill.setImageResource(img_path);




                new Thread(new Runnable() {
                    @Override
                    public void run() {


                        // CALL THIS METHOD TO GET THE ACTUAL PATH

                        // path = getRealPathFromURIPath(tempUri, getActivity());
                        // mPickedImages.add(path);


                //        System.out.println("Image URI=" + path);
                    }
                }).start();

            } catch (Exception e) {
                e.printStackTrace();
            }

        /*    String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();*/


          /*  Bitmap bmp = null;
            try {
                bmp = getBitmapFromUri(selectedImage);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/

        }


    }


    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        if (selectedImage == null) {
            Toast.makeText(context, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor =context.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }
        }


       /* galleryOperation(data);
        imagelist.add(picturePath);
        Bitmap bitmap2=CommonUtils.getBitMapFromImageURl(picturePath, (Activity) context);
        ivAadharcardImage.setImageBitmap(bitmap2);
        Log.e("galleryIsha",picturePath);*/

    }

    private void ProductApi() {
        Call<ResponseBody> bodyCall = apiInterface.getProductCatagoryList(subCategoryID);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    progressDialog.dismiss();
                    String s = response.body().string();
                    JSONObject object = new JSONObject(s);
                    JSONArray jsonArray = object.getJSONArray("response");
                    // catagorylist.clear();

                            product_list = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String s1 = jsonObject.getString("p_name");
                        product_list.add(s1);

                    }
                    selectProductDialog();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
    private void callCatagoryApi() {

        Call<ResponseBody> bodyCall = apiInterface.getCatagoryList();
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                     progressDialog.dismiss();
                    String res = response.body().string();
                    JSONObject jsonArray = new JSONObject(res);
                    JSONArray array = jsonArray.getJSONArray("response");
                    catagorylist = new ArrayList<>();
                    category_list_id=new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        String s = jsonObject.getString("cat_name");
                        category_list_id.add(jsonObject.getString("cat_id"));
                        catagorylist.add(s);
                    }
                    selectCategoryDialog();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }


    private void selectProductDialog() {
        builder = new AlertDialog.Builder(context);
        alertDialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);
        et_state.setHint("Select Product");
        Log.e("list", String.valueOf(product_list.size()));
       /* ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);*/
        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);

        categoryAdapter = new CategoryAdapter(product_list, context, RegisterComplaint.this,count);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(categoryAdapter);
        alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                categoryAdapter.filter(editable.toString());

            }
        });

    }

    private void selectCategoryDialog() {
        builder = new AlertDialog.Builder(context);
        alertDialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);
        et_state.setHint("Select Category");

        Log.e("list", String.valueOf(catagorylist.size()));

        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);

        categoryAdapter = new CategoryAdapter(catagorylist, context, RegisterComplaint.this,count);

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(categoryAdapter);
          alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                categoryAdapter.filter(editable.toString());

            }
        });

    }


    private void callSubcatagoryApi( ) {

        Call<ResponseBody> bodyCall = apiInterface.getSubCatagoryList(categoryID);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    String s = response.body().string();
                    if (s!= null) {
                        JSONObject object = new JSONObject(s);
                        JSONArray jsonArray = object.getJSONArray("response");
                        // catagorylist.clear();
                        subCategory_list = new ArrayList<>();
                        sub_category_list_id = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String s1 = jsonObject.getString("subcat_name");

                            subCategory_list.add(jsonObject.getString("subcat_name"));
                            sub_category_list_id.add(jsonObject.getString("subcat_id"));
                        }
                        subCategoryDialog();

                    }
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void subCategoryDialog() {
        builder = new AlertDialog.Builder(context);
        alertDialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);
        et_state.setHint("Select Sub-Category");

        Log.e("list", String.valueOf(subCategory_list.size()));
        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);

        categoryAdapter = new CategoryAdapter(subCategory_list, context, RegisterComplaint.this,count);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(categoryAdapter);
          alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                categoryAdapter.filter(editable.toString());

            }
        });

    }


    @Override
    public void catagoryText(int position, ArrayList<String>list, String count) {
        if(CommonUtils.getPreferencesString(context,AppConstants.CATEGORY).equalsIgnoreCase("category")){
            categoryID=category_list_id.get(position).toString();
            catogory_txt.setText(list.get(position));
            tv_product_name.setText("");
            txt_subcatagory.setText("");
            alertDialog.dismiss();
        }
        else if(CommonUtils.getPreferencesString(context,AppConstants.CATEGORY).equalsIgnoreCase("sub_category")){
            subCategoryID=sub_category_list_id.get(position).toString();
            txt_subcatagory.setText(list.get(position));
            tv_product_name.setText("");

            alertDialog.dismiss();

        }

        else if (CommonUtils.getPreferencesString(context, AppConstants.CATEGORY).equalsIgnoreCase("product")) {
            tv_product_name .setText(list.get(position));
            alertDialog.dismiss();

        }
        else {
            Toast.makeText(context, " Id Not Found", Toast.LENGTH_SHORT).show();
        }
    }



}
