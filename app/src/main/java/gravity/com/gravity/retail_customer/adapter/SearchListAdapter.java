package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.SearchListModel;

/**
 * Created by user on 2/16/2018.
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {

    SearchListModel searchListModel;
    ArrayList<SearchListModel> arrayList = new ArrayList<>();

    Context context;

    public SearchListAdapter(ArrayList<SearchListModel> arrayList, Context context) {


        this.arrayList = arrayList;

        this.context = context;

    }

    @Override
    public SearchListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_list_row_item, parent, false);

        SearchListAdapter.ViewHolder recyclerViewHolder = new SearchListAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(SearchListAdapter.ViewHolder holder, int position) {

        holder.pro_image.setImageResource(arrayList.get(position).getImg_product());
        holder.like.setImageResource(arrayList.get(position).getImg_like());

      //  holder.pro_image2.setImageResource(arrayList.get(position).getImgID());
        holder.pro_name.setText(arrayList.get(position).getPro_name());
        holder.rating.setText(arrayList.get(position).getPro_rating());
        holder.price.setText(arrayList.get(position).getPro_price());
       // holder.pro_name2.setText(arrayList.get(position).getProductName());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  pro_image, like;
        TextView pro_name,rating,price;

        public ViewHolder(View itemView) {
            super(itemView);

            pro_image = (ImageView) itemView.findViewById(R.id.img_product);
            like = (ImageView) itemView.findViewById(R.id.like);

            pro_name=itemView.findViewById(R.id.p_name);
            rating=itemView.findViewById(R.id.rating);
            price=itemView.findViewById(R.id.price);

        }
    }
}
