package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 3/5/2018.
 */

public class DealerListModel {

int profile_pic;
String dealer_name;
    String dealer_contact;

    public int getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(int profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getDealer_contact() {
        return dealer_contact;
    }

    public void setDealer_contact(String dealer_contact) {
        this.dealer_contact = dealer_contact;
    }

    public String getDealer_distaqnce() {
        return dealer_distaqnce;
    }

    public void setDealer_distaqnce(String dealer_distaqnce) {
        this.dealer_distaqnce = dealer_distaqnce;
    }

    String dealer_distaqnce;
}
