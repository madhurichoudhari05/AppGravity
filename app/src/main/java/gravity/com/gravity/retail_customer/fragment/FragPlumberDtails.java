package gravity.com.gravity.retail_customer.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.ActivityForgotPassword;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.retail_customer.model.PlumberUserDetailsResponse;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 2/15/2018.
 */

public class FragPlumberDtails extends Fragment {


    Button btn_completed;
    Button btn_submit;
    TextView tv_plmber_name,tv_mobileNo,tv_service_charge,tv_plumber_rating;

    String plumber_id="",complain_id="";
    private PlumberUserDetailsResponse plumberUser;

    private Context mcontext;
    ImageView back,ivProductImage;
    private TextView tv_plumber_name,tv_plumber_phone,tv_plumber_location,tv_plumber_service,tv_plumber_category,
            tv_plumber_date,tv_plumber_time,tv_plumber_request,tv_plumber_title;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_plumber_details, container, false);
        btn_completed=view.findViewById(R.id.btn_completed);
        mcontext=getActivity();
        tv_plumber_rating=view.findViewById(R.id.tv_plumber_rating);
        tv_plmber_name=view.findViewById(R.id.tv_plmber_name);
        tv_mobileNo=view.findViewById(R.id.tv_mobileNo);
        tv_service_charge=view.findViewById(R.id.tv_service_charge);


        tv_plumber_name=view.findViewById(R.id.tv_plumber_name);
        tv_plumber_phone=view.findViewById(R.id.tv_plumber_phone);
        tv_plumber_location=view.findViewById(R.id.tv_plumber_location);
        tv_plumber_service=view.findViewById(R.id.tv_plumber_service);
        tv_plumber_category=view.findViewById(R.id.tv_plumber_category);
        ivProductImage=view.findViewById(R.id.ivProductImage);
        tv_plumber_date=view.findViewById(R.id.tv_plumber_date);
        tv_plumber_time=view.findViewById(R.id.tv_plumber_time);
        tv_plumber_request=view.findViewById(R.id.tv_plumber_request);
        tv_plumber_title=view.findViewById(R.id.tv_plumber_title);
        
        
        


        if(getArguments()!=null) {
            plumberUser = (PlumberUserDetailsResponse) getArguments().getSerializable("AllDetails");
           // Toast.makeText(mcontext, plumberUser.getCustomer().getFirstName(), Toast.LENGTH_SHORT).show();
            
            tv_plmber_name.setText(plumberUser.getPlumber().getFirstName());
            tv_mobileNo.setText(plumberUser.getPlumber().getPhone());
            tv_service_charge.setText("₹ 280");

           //  Picasso.with(mcontext).load(plumberUser.getPlumber().getProfilePic()).into(ivProductImage);


            if(plumberUser.getComplaint().getDate()!=null) {
                String date = plumberUser.getComplaint().getDate();
                if(plumberUser.getCustomer().getLatitude()!=null&&plumberUser.getCustomer().getLongitude()!=null) {
                    getLocation(plumberUser.getCustomer().getLatitude(), plumberUser.getCustomer().getLongitude());
                    String[] parts = date.split(" ");
                    String currentDate = parts[0]; // 004
                    String time = parts[1];

                    tv_plumber_date.setText(currentDate);
                    tv_plumber_time.setText(time);


                } }
            tv_plumber_name.setText(plumberUser.getCustomer().getFirstName());
            tv_plumber_phone.setText(plumberUser.getCustomer().getPhone());
            tv_plumber_service.setText(plumberUser.getComplaint().getComplaintNo());

            tv_plumber_category.setText(plumberUser.getComplaint().getCategory());

            // tv_plumber_title.setText(plumberUser.getCustomer().getJobStatus());
            tv_plumber_request.setText(plumberUser.getComplaint().getComplaintDescription());
            }

        btn_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogRaingPlumber();
            }
        });


        return view;

    }

    private void dialogRaingPlumber() {


        final Dialog dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getActivity().getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setLayout(width, height);

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        dialog.setContentView(R.layout.dialog_rating_plumber);
         btn_submit=dialog.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



       /* builder = new AlertDialog.Builder(getActivity());
        alertDialog = builder.create();

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_rating_plumber, null, false);
        alertDialog.setView(view);
        alertDialog.show();
        */

    }

    private void getLocation(String latitudec, String longitudec) {



        if (latitudec!=null&& !latitudec.equals("")) {
            double latitude = Double.valueOf(latitudec);
            double longitude =Double.valueOf(longitudec);
            try {



                Geocoder geocoder = new Geocoder(mcontext, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses.size() > 0) {
                    String cityName = addresses.get(0).getLocality();
                    String stateName = addresses.get(0).getAdminArea();
                    String s = ""
                            + addresses.get(0).getAddressLine(0) + "\n";
                           /* + addresses.get(0).getFeatureName() + "\n"
                            + "Locality: "
                            + addresses.get(0).getLocality() + "\n"
                            + addresses.get(0).getPremises() + "\n"
                            + "Admin Area: ";*/
                           /* + addresses.get(0).getAdminArea() + "\n"
                            + "Country code: "
                            + addresses.get(0).getCountryCode() + "\n"
                            + "Country name: "
                            + addresses.get(0).getCountryName() + "\n"
                            + "Phone: " + addresses.get(0).getPhone()
                            + "\n" + "Postbox: "
                            + addresses.get(0).getPostalCode() + "\n"
                            + "SubLocality: "
                            + addresses.get(0).getSubLocality() + "\n"
                            + "SubAdminArea: "
                            + addresses.get(0).getSubAdminArea() + "\n"
                            + "SubThoroughfare: "
                            + addresses.get(0).getSubThoroughfare()
                            + "\n" + "Thoroughfare: "
                            + addresses.get(0).getThoroughfare() + "\n"
                            + "URL: " + addresses.get(0).getUrl();*/

                    Log.e("locationstate","locationstate"+s);
                    // Toast.makeText(context, "city:::::statename" + cityName+""+stateName, Toast.LENGTH_SHORT).show();

                    tv_plumber_location.setText(s);
                    // tv_plumber_location.setText(s+""+cityName);


                    Log.e("CurrentCity","CurrentCity"+cityName);
                    Log.e("CurrentCity","Currentstate"+stateName);

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            Log.e("NotLocation","NotLocation");

        }

    }



}
