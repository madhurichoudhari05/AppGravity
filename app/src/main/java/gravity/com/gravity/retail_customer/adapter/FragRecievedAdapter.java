package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.RepeatOrder;
import gravity.com.gravity.retail_customer.model.ReceivedOrderModel;

/**
 * Created by user on 2/16/2018.
 */

public class FragRecievedAdapter extends RecyclerView.Adapter<FragRecievedAdapter.ViewHolder> {

    ReceivedOrderModel dealerListModel;
    ArrayList<ReceivedOrderModel> arrayList = new ArrayList<>();

    Context context;

    public FragRecievedAdapter(ArrayList<ReceivedOrderModel> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public FragRecievedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_received_order_row_item, parent, false);

        FragRecievedAdapter.ViewHolder recyclerViewHolder = new FragRecievedAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(FragRecievedAdapter.ViewHolder holder, int position) {

        holder.repeateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, RepeatOrder.class));
            }
        });

        if (position % 6 == 0) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#4B0082"));

        } else if (position % 6 == 1) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FFA500"));

        } else if (position % 6 == 2) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#48D1CC"));

        } else if (position % 6 == 3) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#C71585"));

        } else if (position % 6 == 4) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#00FFFF"));

        } else if (position % 6 == 5) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FFD700"));

        } else if (position % 6 == 6) {
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FF0000"));

        }
        if (position == 0) {
            holder.contentRelative.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        if (position % 2 == 0) {

            holder.contentRelative.setBackgroundColor(Color.parseColor("#eeeeee"));
        } else {
            holder.contentRelative.setBackgroundColor(Color.parseColor("#fffefefe"));
        }


        holder.profile_pic.setImageResource(arrayList.get(position).getProduct_pic());


        //  holder.pro_image2.setImageResource(arrayList.get(position).getImgID());

        holder.orderID.setText(arrayList.get(position).getOrderID());
        holder.proSpecification.setText(arrayList.get(position).getProductSpecification());
        holder.diliveryDate.setText(arrayList.get(position).getDiliveryDate());
        holder.repeateOrder.setText(arrayList.get(position).getReceieptOrder());
        holder.repeateOrder.setText(arrayList.get(position).getReceieptOrder());

        // holder.pro_name2.setText(arrayList.get(position).getProductName());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profile_pic;
        RelativeLayout relativeLayout, contentRelative;
        TextView orderID, proSpecification, diliveryDate, repeateOrder;

        public ViewHolder(View itemView) {
            super(itemView);

            profile_pic = (ImageView) itemView.findViewById(R.id.product_pic);
            orderID = itemView.findViewById(R.id.tv_orderID);
            proSpecification = itemView.findViewById(R.id.tv_pro_specification);
            diliveryDate = itemView.findViewById(R.id.dileverydate);
            repeateOrder = itemView.findViewById(R.id.repeatOrder);
            relativeLayout = itemView.findViewById(R.id.relative);
            contentRelative = itemView.findViewById(R.id.contentRelative);

        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter((color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
