package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import gravity.com.gravity.R;

/**
 * Created by admin on 3/16/2018.
 */

public class RepeatOrderAdapter extends PagerAdapter {

    private ArrayList<Integer> imgs;
    private LayoutInflater inflater;
    private Context context;
    ImageView imageView;

    public RepeatOrderAdapter(Context context, ArrayList<Integer> imgs) {
        this.context = context;
        this.imgs=imgs;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return imgs.size();
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View  view =inflater.inflate(R.layout.repeat_order_single,container,false);
     imageView = view.findViewById(R.id.img_repeat_order);
     imageView.setImageResource(imgs.get(position));
     container.addView(view);
     return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);

    }

}
