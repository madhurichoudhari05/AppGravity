package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.Catagory_video;
import gravity.com.gravity.retail_customer.model.VideoModel;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoInner> {

    ArrayList<VideoModel> list;
    Context context;


    public VideoAdapter(ArrayList<VideoModel> list,Context context){

        this.list =list;
        this.context =context;

    }


    @Override
    public VideoInner onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_single,parent,false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context,Catagory_video.class));
            }
        });
        return new VideoInner(view);
    }

    @Override
    public void onBindViewHolder(VideoInner holder, int position) {

        VideoModel pojo =list.get(position);

        holder.txt_video.setText(pojo.txt_video);
        holder.pic_video.setImageResource(pojo.pic_video);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VideoInner extends RecyclerView.ViewHolder {

        ImageView pic_video;
        TextView txt_video;
        public VideoInner(View itemView) {
            super(itemView);


            pic_video =itemView.findViewById(R.id.pic_video);
            txt_video=itemView.findViewById(R.id.txt_video);

        }


    }
}
