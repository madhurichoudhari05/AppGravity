package gravity.com.gravity.retail_customer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import gravity.com.gravity.retail_customer.fragment.FragPlacedOrder;
import gravity.com.gravity.retail_customer.fragment.FragReceivedOrder;

/**
 * Created by user on 2/15/2018.
 */

public class MyOrderTabAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public MyOrderTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
               // TabFragUserSignIn tab1=new TabFragUserSignIn();
                FragReceivedOrder tab1=new FragReceivedOrder();

                return tab1;
            case 1:
               // TabFragmentSignUp tab2 = new TabFragmentSignUp();
                FragPlacedOrder tab2=new FragPlacedOrder();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
