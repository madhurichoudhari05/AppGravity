package gravity.com.gravity.retail_customer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import gravity.com.gravity.retail_customer.fragment.TabFragPlumberSignIn;
import gravity.com.gravity.retail_customer.fragment.TabFragPlumberSignUp;

/**
 * Created by user on 2/15/2018.
 */

public class SignupSignInPlumberAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public SignupSignInPlumberAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                //TabFragmentSignIn tab1 = new TabFragmentSignIn();
               // TabFragUserSignIn tab1=new TabFragUserSignIn();
                TabFragPlumberSignIn tab1=new TabFragPlumberSignIn();
                return tab1;
            case 1:
               // TabFragmentSignUp tab2 = new TabFragmentSignUp();
               // TabFragUserSignUp tab2=new TabFragUserSignUp();
                TabFragPlumberSignUp tab2=new TabFragPlumberSignUp();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
