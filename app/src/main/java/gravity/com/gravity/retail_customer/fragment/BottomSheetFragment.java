package gravity.com.gravity.retail_customer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import gravity.com.gravity.R;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by admin on 3/27/2018.
 */

public class BottomSheetFragment extends BottomSheetDialogFragment {

    int postion;
    static RadioGroup radioGroup;


    RadioButton rbtn;
    private Context mContext;

    public BottomSheetFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sort_dialog, container, false);
        mContext = getActivity();
        final RadioButton radioButton = view.findViewById(R.id.rb_1);
        final RadioButton radioButton2 = view.findViewById(R.id.rb_2);
        final RadioButton radioButton3 = view.findViewById(R.id.rb_3);
        final RadioButton radioButton4 = view.findViewById(R.id.rb_4);
        radioGroup = view.findViewById(R.id.radiogrp1);
        //   rbtn = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());




        if (CommonUtils.getIntPreferences(mContext, AppConstants.SORT_BY_KEY) == 0) {
            radioButton.setChecked(true);

        } else if (CommonUtils.getIntPreferences(mContext, AppConstants.SORT_BY_KEY) == 1) {
            radioButton2.setChecked(true);

        } else if (CommonUtils.getIntPreferences(mContext, AppConstants.SORT_BY_KEY) == 2) {

            radioButton3.setChecked(true);
        } else if (CommonUtils.getIntPreferences(mContext, AppConstants.SORT_BY_KEY) == 3) {

            radioButton4.setChecked(true);
        }
        else {
            Toast.makeText(mContext, "Not found", Toast.LENGTH_SHORT).show();
        }


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                RadioButton checked_r = (RadioButton) radioGroup.findViewById(i);

                //   boolean ischecked = checked_r.isChecked();

                // if(ischecked){
                if (checked_r == radioButton) {
                    radioButton.setChecked(true);
                    ((BottomSheetFragment) getFragmentManager().findFragmentByTag("rj")).dismiss();
                    CommonUtils.saveIntPreferences(mContext, AppConstants.SORT_BY_KEY, 0);


                } else if (checked_r == radioButton2) {
                    radioButton2.setChecked(true);
                    ((BottomSheetFragment) getFragmentManager().findFragmentByTag("rj")).dismiss();

                    CommonUtils.saveIntPreferences(mContext, AppConstants.SORT_BY_KEY, 1);

                } else if (checked_r == radioButton3) {
                    radioButton3.setChecked(true);
                    ((BottomSheetFragment) getFragmentManager().findFragmentByTag("rj")).dismiss();

                    CommonUtils.saveIntPreferences(mContext, AppConstants.SORT_BY_KEY, 2);

                } else if (checked_r == radioButton4) {
                    radioButton4.setChecked(true);
                    ((BottomSheetFragment) getFragmentManager().findFragmentByTag("rj")).dismiss();

                    CommonUtils.saveIntPreferences(mContext, AppConstants.SORT_BY_KEY, 3);

                }
                else {
                    Toast.makeText(mContext, "Not Found Tag.", Toast.LENGTH_SHORT).show();
                }

            }


        });




















        //    int selectedid = radioGroup.getCheckedRadioButtonId();


/*
        if (selectedid == R.state_id.rb_1) {
            radioButton.setChecked(true);
        } else if (selectedid == R.state_id.rb_2) {
            radioButton2.setChecked(true);
        }

        else if(selectedid==R.state_id.rb_3){
            radioButton3.setChecked(true);
        }
        else if(selectedid==R.state_id.rb_4){
            radioButton4.setChecked(true);
        }
*/

/*

        if (postion == 0) {
            radioButton.setChecked(true);
        }
        if (postion == 1) {
            radioButton2.setChecked(true);
        }
        if (postion == 2) {
            radioButton3.setChecked(true);
        }
        if (postion == 3) {
            radioButton4.setChecked(true);
        }
*/


/*

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
              //  Toast.makeText(getContext(), "dasdasda", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((BottomSheetFragment) getFragmentManager().findFragmentByTag("rj")).dismiss();
                      //  Faucets.position = postion;

                    }
                }, 600);


            }
        });
*/
        return view;

    }

}
