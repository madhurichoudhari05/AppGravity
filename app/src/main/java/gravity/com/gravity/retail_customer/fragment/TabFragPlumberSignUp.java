package gravity.com.gravity.retail_customer.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.DataInterface.StateInterface;
import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.AadharDetail;
import gravity.com.gravity.plumber.adapter.StateAdapter;
import gravity.com.gravity.plumber.model.StatePOJO;
import gravity.com.gravity.service.Retrofit.model.SignUpModel;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 2/15/2018.
 */

public class TabFragPlumberSignUp extends Fragment implements StateInterface{

    EditText et_fname, et_lname, et_email, et_mobile, et_password, et_c_password, et_service_location;
    ImageView img1_dropdown;
    EditText et_state;
    String user_type="";
    TextView selected_state,et_city;
    StateAdapter stateAdapter;
    RelativeLayout relative_country,relative_state,relative_city;
    Button register;
    ImageView iv_show_pass;
    private AlertDialog alertDialog;
    private APIInterface apiInterface;
    private Context mcontext;
    TextView tv_country,tv_state;
    ArrayList<String> countryList;
    ArrayList<String> cityList;
    private String otp="";
    private Intent intent;
    private String user_id="";
    private boolean isChecked = true;
    String latitute="",longitude="";
    ArrayList<String> country_list,state_list,city_list;
    ArrayList<String> country_list_id,state_list_id,city_list_id;
    String country_id ="", state_id ="",intercountry_id="";
    String State_id="";
    ProgressDialog progressDialog;
    AlertDialog.Builder builder;
    int count=0;
    String username="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_frag_plumber_signup, container, false);
        mcontext=getActivity();
        apiInterface= APIClient.getClient().create(APIInterface.class);
        img1_dropdown = view.findViewById(R.id.img1_dropdown);
        et_fname = view.findViewById(R.id.et_fname);
        et_email = view.findViewById(R.id.et_email);
        relative_country = view.findViewById(R.id.relative_country);
        relative_state = view.findViewById(R.id.relative_state);
        et_city = view.findViewById(R.id.et_city);
        tv_country=view.findViewById(R.id.tv_country);
        tv_state=view.findViewById(R.id.tv_state);
        et_password = view.findViewById(R.id.et_password);
        //et_c_password = view.findViewById(R.state_id.et_c_password);
        iv_show_pass=view.findViewById(R.id.iv_show_pass);
        et_mobile = view.findViewById(R.id.et_mob_no);
        selected_state = view.findViewById(R.id.tv_country);
        et_service_location = view.findViewById(R.id.et_service_location);
        relative_city=view.findViewById(R.id.relative_city);


        if(CommonUtils.getPreferences(getActivity(),AppConstants.CURRENT_LAT)!=null&&CommonUtils.getPreferences(mcontext,AppConstants.CURRENT_LONGI)!=null) {



            latitute=CommonUtils.getPreferences(mcontext,AppConstants.CURRENT_LAT);
            longitude=CommonUtils.getPreferences(mcontext,AppConstants.CURRENT_LONGI);
            //  Toast.makeText(getActivity(),longitude  , Toast.LENGTH_SHORT).show();
            // Toast.makeText(getActivity(), place.getName() + ":::" + address, Toast.LENGTH_SHORT).show();
            Log.e("Currentlat",latitute);
            Log.e("Currentlongitude",longitude);

            // map.addMarker(new MarkerOptions().position(str));
        }



         intent=new Intent(mcontext,AadharDetail.class);


        iv_show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isChecked) {
                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye1);
                    isChecked = true;
                } else {
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye);
                    isChecked = false;

                }

            }


        });

        relative_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesString(mcontext, AppConstants.COUNTRY_LIST,"country");

                //selectCountryDialog();
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("loading");
                progressDialog.show();
                callCountryAPI();

            }
        });

        relative_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.savePreferencesString(mcontext, AppConstants.COUNTRY_LIST, "state");
                //selectStateDialog();

             //   Log.e("coutryin", intercountry_id);
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("loading");
                progressDialog.show();
                callStateAPI();


            }
        });

        relative_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtils.savePreferencesString(mcontext, AppConstants.COUNTRY_LIST, "city");
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("loading");
                progressDialog.show();
                callCityAPI();
            }
        });

        et_fname.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                et_fname.setCursorVisible(true);
                return false;
            }
        });



        et_city.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                et_city.setCursorVisible(true);
                return false;
            }
        });

        et_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                et_password.setCursorVisible(true);
                return false;
            }
        });


        et_mobile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                et_mobile.setCursorVisible(true);
                return false;
            }
        });

        et_service_location.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                et_service_location.setCursorVisible(true);
                return false;
            }
        });






        register = view.findViewById(R.id.btn_tab_f_siup);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {








               // Toast.makeText(mcontext, "btn Clicked", Toast.LENGTH_SHORT).show();
                checkValidation();
               // callSignUApi();

            }
        });

        return view;
    }


    private void callCityAPI() {
        Log.e("ID",state_id);

        Call<ResponseBody> call = apiInterface.getCities(state_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
               progressDialog.cancel();
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.getJSONArray("response");

                    if(jsonObject.getString("status").equals("1")) {
                        city_list = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jbject = array.getJSONObject(i);
                            String c_obj = jbject.getString("name");
                            //state_id = jbject.getString("state_id");

                            city_list.add(c_obj);

                            // selectStateDialog();
                        }
                        selectCityDialog();
                    }
                    else {
                        CommonUtils.snackBar("Error Occured",et_email);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    private void callStateAPI() {


        Call<ResponseBody> call = apiInterface.getStates(country_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {


                    progressDialog.cancel();
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.getJSONArray("response");

                    if(jsonObject.getString("status").equals("1")) {
                        state_list = new ArrayList<>();
                        state_list_id = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jbject = array.getJSONObject(i);
                            String c_obj = jbject.getString("name");
                            //   state_id = jbject.getString("id");
                              // state_id=  CommonUtils.getPreferencesString(mcontext,AppConstants.CITY_POSITION,list.get(position));
                            state_list_id.add(jbject.getString("id"));
                            state_list.add(c_obj);
                            et_city.setText("");

                            // selectStateDialog();
                        }
                        selectStateDialog();
                    }
                    else {
                        CommonUtils.snackBar("Error Occured",et_email);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void callCountryAPI() {
        Call<ResponseBody> call = apiInterface.getCountries();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressDialog.cancel();
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray array = jsonObject.getJSONArray("response");
                    if(jsonObject.getString("status").equals("1")) {
                        country_list = new ArrayList<>();
                        country_list_id = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jbject = array.getJSONObject(i);
                            String c_obj = jbject.getString("name");
                          // country_id = jbject.getString("id");
                            country_list_id.add(jbject.getString("id"));
                            country_list.add(c_obj);
                            selectCountryDialog();

                        }
                    }
                    else {
                        CommonUtils.snackBar("Error Occured",et_email);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }


    private void callSignUApi() {
        getLocationFromAddress(mcontext,et_city.getText().toString());
        Log.d("LATLONG",latitute+", log is>"+longitude);

        Call<SignUpModel> call=apiInterface.getSignUp(
                et_fname.getText().toString(),
                et_mobile.getText().toString(),
                et_password.getText().toString(),
                tv_country.getText().toString(),
                tv_state.getText().toString(),
                et_city.getText().toString(),
                latitute,
                longitude,
                et_service_location.getText().toString(),
                "4",
                "Gravity",
                FirebaseInstanceId.getInstance().getToken()+"",
                "android");
         call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {

                if (response.isSuccessful())
                    Log.e("Success", new Gson().toJson(response.body()));
                else
                    Log.e("unSuccess", new Gson().toJson(response.errorBody()));

                SignUpModel signUpModel = response.body();
                if (signUpModel != null ) {
                    if (signUpModel.getStatus()==1) {
                       // CommonUtils.snackBar(signUpModel.getMsg(), et_email);
                        if (signUpModel.getResponse()!=null&&signUpModel.getResponse().size()>0) {
                            if(signUpModel.getOTP()!=null){
                            otp = String.valueOf(signUpModel.getOTP());}
                            if(signUpModel.getResponse().get(0).getId()!=null){
                            user_id = String.valueOf(signUpModel.getResponse().get(0).getId());}

                            CommonUtils.savePreferencesString(mcontext,AppConstants.PLUMBER_USER_ID,String.valueOf(signUpModel.getResponse().get(0).getId()));
                            CommonUtils.savePreferencesString(getActivity(),AppConstants.USER_NAME,signUpModel.getResponse().get(0).getName());

                            if(signUpModel.getResponse().get(0).getUserType()!=null){
                            user_type=signUpModel.getResponse().get(0).getUserType();}
                            if(signUpModel.getResponse().get(0).getName()!=null){
                          username=signUpModel.getResponse().get(0).getName();}


                          //  Toast.makeText(mcontext, "otp is :" + signUpModel.getOTP(), Toast.LENGTH_SHORT).show();
                            Log.e("otpLogin", "otp is :" + otp);
                            otpDialog(otp);

                        }
                        else {

                            Toast.makeText(mcontext, "otp null" + signUpModel.getOTP(), Toast.LENGTH_SHORT).show();
                            Log.e("signUp","signdata"+signUpModel.getResponse().get(0).getPhone());

                        }

                    }
                    else   if (signUpModel.getStatus()==2 ) {
                        Toast.makeText(mcontext, "status code 2"+signUpModel.getMsg(), Toast.LENGTH_SHORT).show();
                        Log.e("AllreadyExist",signUpModel.getStatus()+"");
                        CommonUtils.snackBar(signUpModel.getMsg(), et_email);
                    }
                    else {
                        Toast.makeText(mcontext, "else part"+signUpModel.getMsg(), Toast.LENGTH_SHORT).show();
                        CommonUtils.snackBar(signUpModel.getMsg(), et_email);
                    }
                }
                else {

                    Toast.makeText(mcontext, "No Record found ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable throwable) {

            }
        });
    }

    private void selectCityDialog() {

        builder = new AlertDialog.Builder(mcontext);
        alertDialog = builder.create();

     /*   LayoutInflater layoutInflater=LayoutInflater.from(mcontext);
        View view=layoutInflater.inflate(R.layout.state_list,null,false);
     */
        View view = LayoutInflater.from(mcontext).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);
        et_state.setHint("Select City");

        //  ArrayList<String> stateList = new ArrayList<>();
        // prepareStateList(stateList);

        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);
        stateAdapter = new StateAdapter(city_list, mcontext, TabFragPlumberSignUp.this,count,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mcontext));
        recyclerView.setAdapter(stateAdapter);
        alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                stateAdapter.filter(editable.toString());

            }
        });
    }

    private void selectCountryDialog() {
        builder = new AlertDialog.Builder(mcontext);
         alertDialog = builder.create();
        View view = LayoutInflater.from(mcontext).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);
       /* ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);*/

        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);
       stateAdapter = new StateAdapter(country_list, mcontext, TabFragPlumberSignUp.this,count,this);

        recyclerView.setLayoutManager(new LinearLayoutManager(mcontext));
        recyclerView.setAdapter(stateAdapter);
        alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

              stateAdapter.filter(editable.toString());

            }
        });

    }


    private void selectStateDialog() {
        builder = new AlertDialog.Builder(mcontext);
        alertDialog = builder.create();

     /*   LayoutInflater layoutInflater=LayoutInflater.from(mcontext);
        View view=layoutInflater.inflate(R.layout.state_list,null,false);
     */
        View view = LayoutInflater.from(mcontext).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);
        et_state.setHint("Select State");


       // ArrayList<String> stateList = new ArrayList<>();
       // prepareStateList(stateList);


        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);
        stateAdapter = new StateAdapter(state_list, mcontext, TabFragPlumberSignUp.this,count,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mcontext));
        recyclerView.setAdapter(stateAdapter);
        alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                stateAdapter.filter(editable.toString());

            }
        });

    }
    private void otpDialog(String otp1) {

        final Dialog dialog = new Dialog(mcontext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getActivity().getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setLayout(width, height);

        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.custom_dialoge_otp);

        Button button = dialog.findViewById(R.id.varify_acco);
        PinEntryEditText txt_pin_entry = dialog.findViewById(R.id.txt_pin_entry);

      txt_pin_entry.setText(otp1);
    //  txt_pin_entry.setText("345346457");

        dialog.show();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

             // Intent intent=new Intent(mcontext,AadharDetail.class);
                intent.putExtra("USERID",user_id);
                intent.putExtra("OTP",otp);
                intent.putExtra("USER_TYPE",user_type);
                intent.putExtra("USER_NAME",username);
                startActivity(intent);
            }
        });


    }

    public void setState(int position, ArrayList<String> list, String id) {
        if(CommonUtils.getPreferencesString(mcontext,AppConstants.COUNTRY_LIST).equalsIgnoreCase("country")){
            country_id=country_list_id.get(position).toString();
            this.count=count;
            state_id=id;
            tv_country.setText(list.get(position));
            tv_country.setTextColor(Color.parseColor("#8f8f8f"));
            alertDialog.dismiss();
        }
            else if(CommonUtils.getPreferencesString(mcontext,AppConstants.COUNTRY_LIST).equalsIgnoreCase("state")){
            state_id=state_list_id.get(position).toString();

            tv_state.setText(list.get(position));
            CommonUtils.savePreferencesString(mcontext,AppConstants.CITY_POSITION,list.get(position));


           // state_id=CommonUtils.getPreferencesString(mcontext,AppConstants.CITY_POSITION);
            tv_state.setTextColor(Color.parseColor("#8f8f8f"));
            alertDialog.dismiss();

        }  else if (CommonUtils.getPreferencesString(mcontext, AppConstants.COUNTRY_LIST).equalsIgnoreCase("city")) {
            et_city.setText(list.get(position));
           // CommonUtils.savePreferencesString(mcontext,AppConstants.CITY_POSITION,list.get(position));
            et_city.setTextColor(Color.parseColor("#8f8f8f"));
            alertDialog.dismiss();

        }

    }


    private void checkValidation() {

        String fullname = et_fname.getText().toString();

        if (TextUtils.isEmpty(et_fname.getText().toString())) {
            CommonUtils.snackBar("  Please Enter First Name.", et_fname);
        } else if (!fullname.matches("[a-zA-Z ]+")) {
            CommonUtils.snackBar("Name Accept Alphabets Only.", et_fname);
        }
        else if (TextUtils.isEmpty(et_mobile.getText().toString())) {
            CommonUtils.snackBar("Please Enter Phone Number.", et_fname);
        }
        else if (et_mobile.getText().toString().length() < 10) {
            CommonUtils.snackBar("Mobile Number Should Be of 10 Digits.", et_fname);
        }
        else if (et_mobile.getText().toString().length() > 10) {
            CommonUtils.snackBar("Mobile Number Should Be of 10 Digits.", et_fname);
        }
        else if (TextUtils.isEmpty(et_password.getText())) {

            CommonUtils.snackBar("Please Enter Your Password.", et_fname);
        } else if ((et_password.getText().toString().length() < 4)) {
            CommonUtils.snackBar("Password Must Be AtLeast 4 Character.", et_fname);

        }else if(TextUtils.isEmpty(tv_country.getText().toString())){
            CommonUtils.snackBar("Please Select Country",et_fname);
        }else if(TextUtils.isEmpty(tv_state.getText().toString())){
            CommonUtils.snackBar("Please Select State",et_fname);
        }
        else if(TextUtils.isEmpty(et_city.getText().toString())){

            CommonUtils.snackBar("Please Enter City Name",et_fname);
        }else if(TextUtils.isEmpty(et_service_location.getText().toString())){
            CommonUtils.snackBar("Please Enter Service Location",et_fname);
        }
        else {
           callSignUApi();

        }
    }



    public void getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 2);
            if (address!= null) {
                Address location = address.get(0);
                p1 = new LatLng(location.getLatitude(), location.getLongitude() );
                latitute= String.valueOf(location.getLatitude());
                longitude=String.valueOf(location.getLongitude());
                location.getLongitude();
            }

            else {
                Toast.makeText(context, "Address not found", Toast.LENGTH_SHORT).show();

        }
        }catch (IOException ex) {

            ex.printStackTrace();

        }


    }

    @Override
    public void getId(String id) {


        intercountry_id=id;
    }
}
