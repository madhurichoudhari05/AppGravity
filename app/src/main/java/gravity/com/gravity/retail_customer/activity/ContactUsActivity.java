package gravity.com.gravity.retail_customer.activity;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import gravity.com.gravity.R;

/**
 * Created by Admin on 3/13/2018.
 */

public class ContactUsActivity extends Fragment {

    ImageView back;
    TextView getIntouch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_contact_us,container,false);


        getIntouch=view.findViewById(R.id.tv_getInTouch);

        String a="<font color='#000000'>Get in touch with</font>";
        String b="<font color='#AC233B'> GRAVITY</font>";
        String c="<font color='#000000'><br>if you need help with</br><br>a project</br></font>";


        getIntouch.setText(Html.fromHtml(a+b+c));
       // getIntouch.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);



return view;
    }
}
