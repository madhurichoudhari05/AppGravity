package gravity.com.gravity.retail_customer.model;

public class CatagoryModel {


    int pic_catagory1,pic2_catagory1;
    String txt1_catagory1,txt2_catagory1,linear_catagory;

    CatagoryModel(){}


    public CatagoryModel(int pic_catagory1, int pic2_catagory1, String txt1_catagory1, String txt2_catagory1, String linear_catagory1){

        this.pic_catagory1=pic_catagory1;
        this.txt1_catagory1=txt1_catagory1;
        this.txt2_catagory1=txt2_catagory1;
        this.linear_catagory=linear_catagory1;
        this.pic2_catagory1=pic2_catagory1;

    }  public int getPic2_catagory1() {
        return pic2_catagory1;
    }

    public void setPic2_catagory1(int pic2_catagory1) {
        this.pic2_catagory1 = pic2_catagory1;
    }

    public String getLinear_catagory1() {
        return linear_catagory;
    }

    public void setLinear_catagory1(String linear_catagory1) {
        this.linear_catagory = linear_catagory1;
    }


    public int getPic_catagory1() {
        return pic_catagory1;
    }

    public void setPic_catagory1(int pic_catagory1) {
        this.pic_catagory1 = pic_catagory1;
    }

    public String getTxt1_catagory1() {
        return txt1_catagory1;
    }

    public void setTxt1_catagory1(String txt1_catagory1) {
        this.txt1_catagory1 = txt1_catagory1;
    }

    public String getTxt2_catagory1() {
        return txt2_catagory1;
    }

    public void setTxt2_catagory1(String txt2_catagory1) {
        this.txt2_catagory1 = txt2_catagory1;
    }
}

