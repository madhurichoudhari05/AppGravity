package gravity.com.gravity.retail_customer.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gravity.com.gravity.R;

/**
 * Created by Admin on 3/5/2018.
 */

public class TabFragMapView  extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_map_view, container, false);



        return view;
    }
}
