package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.MycartAdapter;
import gravity.com.gravity.retail_customer.model.MycartModel;

/**
 * Created by user on 2/19/2018.
 */

public class MyCartActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ImageView back;
    MycartModel mycartModel;
ArrayList<MycartModel>  mycartListt;
Context context;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycart);
          recyclerView=findViewById(R.id.a_mycart_recyclerview);
          back=findViewById(R.id.back);
          context=MyCartActivity.this;
        mycartListt=new ArrayList<>();

        mycartModel=new MycartModel();
        mycartModel.setrupee_mycart("₹ 1396");
        mycartModel.setProductPic(R.drawable.home_t);
        mycartModel.setProducrName("Lorem imspim");
        mycartModel.setAppliedOffer("1 offer applied");
        mycartModel.setAvailableOffer("1 offer available");
        mycartModel.setDeliveryDate("Delivery by Sat,Jan 13 1018");
        mycartListt.add(mycartModel);


        mycartModel=new MycartModel();
        mycartModel.setrupee_mycart("₹ 1396");
        mycartModel.setProducrName("Lorem imspim");
        mycartModel.setProductPic(R.drawable.home_t);
        mycartModel.setAppliedOffer("1 offer applied");
        mycartModel.setAvailableOffer("1 offer available");
        mycartModel.setDeliveryDate("Delivery by Sat,Jan 13 1018");
        mycartListt.add(mycartModel);


        MycartAdapter mycartAdapter=new MycartAdapter(mycartListt,context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(mycartAdapter);



back.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        onBackPressed();
    }
});


    }
}
