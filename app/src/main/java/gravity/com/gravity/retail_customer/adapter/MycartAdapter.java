package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.MycartModel;

/**
 * Created by user on 2/16/2018.
 */

public class MycartAdapter extends RecyclerView.Adapter<MycartAdapter.ViewHolder> {

    ArrayList<MycartModel> arrayList = new ArrayList<>();
    List<String> spinList=new ArrayList<>();

    String qty[]={"1","2","3","4","5"};

    Context context;

    public MycartAdapter(ArrayList<MycartModel> arrayList, Context context) {


        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public MycartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mycart_row_item, parent, false);

        MycartAdapter.ViewHolder recyclerViewHolder = new MycartAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(MycartAdapter.ViewHolder holder, int position) {

        holder.itemIMG.setImageResource(arrayList.get(position).getProductPic());
        holder.productName.setText(arrayList.get(position).getProducrName());
        holder.productRate.setText(arrayList.get(position).getGetProducrRate());
        holder.rupee_mycart.setText(arrayList.get(position).getrupee_mycart());
        holder.appliedOffer.setText(arrayList.get(position).getAppliedOffer());
        holder.dileveryDte.setText(arrayList.get(position).getDeliveryDate());
       // holder.quantity.setText(arrayList.get(position).getAvailableOffer());
        //holder.availableOffer.setText(arrayList.get(position).getAvailableOffer());

        ArrayAdapter<String> adapter= new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, qty);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);

        holder.quantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setGravity(Gravity.CENTER);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  itemIMG;
        TextView productName,productRate,appliedOffer,availableOffer,dileveryDte,tvQuantity,rupee_mycart;
        Spinner quantity;

        public ViewHolder(View itemView) {
            super(itemView);
            spinList.clear();
            spinList.add("1");
            spinList.add("2");
            spinList.add("3");
            spinList.add("4");
            spinList.add("5");
            spinList.add("6");
            spinList.add("7");
            spinList.add("8");

            itemIMG = (ImageView) itemView.findViewById(R.id.img);
            productName=(TextView)itemView.findViewById(R.id.pro_name);
            rupee_mycart=itemView.findViewById(R.id.rupee_mycart);

            appliedOffer=itemView.findViewById(R.id.appliedOfer);
            availableOffer=itemView.findViewById(R.id.availableOffer);
       //     tvQuantity=itemView.findViewById(R.id.tvQuantity);
            dileveryDte=itemView.findViewById(R.id.dileverydate);
            productRate=itemView.findViewById(R.id.dileverydate);
            quantity=itemView.findViewById(R.id.quantity);
            ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1,spinList);
           quantity.setAdapter(arrayAdapter);


        }
    }
}
