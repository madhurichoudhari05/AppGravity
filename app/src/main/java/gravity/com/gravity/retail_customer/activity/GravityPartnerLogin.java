package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import gravity.com.gravity.R;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by Admin on 3/12/2018.
 */

public class GravityPartnerLogin extends AppCompatActivity implements View.OnClickListener {

    FrameLayout distributor, dealer, next;
    TextView tvDistributor, tvDealer, tvNext;
    LinearLayout linear_partner_login;
    Context context;
    ImageView img_back;
    int count = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gra_partner_login);

        context = GravityPartnerLogin.this;
        distributor = findViewById(R.id.dealer_distributor);
        dealer = findViewById(R.id.frame_dealer);
        linear_partner_login = findViewById(R.id.linear_partner_login);
        img_back=findViewById(R.id.img_back);

        linear_partner_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesBoolean(context, AppConstants.GUESR_USER,false);
                CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL,false);

                startActivity(new Intent(context, UserHomeActivity.class));
            }
        });
        next = findViewById(R.id.frame_next);

        tvDistributor = findViewById(R.id.a_g_part_tv_distributor);
        tvDealer = findViewById(R.id.a_g_part_tv_dealer);
        tvNext = findViewById(R.id.a_g_part_tv_next);
        dealer.setBackgroundResource(R.drawable.rounded_corner_gray_border_black_bg_layout);
        tvDealer.setTextColor(Color.parseColor("#FFFFFF"));

        tvNext.setOnClickListener(this);
        tvDealer.setOnClickListener(this);
        tvDistributor.setOnClickListener(this);
        img_back.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.a_g_part_tv_dealer:
                dealer.setBackgroundResource(R.drawable.rounded_corner_gray_border_black_bg_layout);
                tvDealer.setTextColor(Color.parseColor("#FFFFFF"));
                CommonUtils.savePreferencesString(context, AppConstants.DEALER_LOGIN, "dealer_login");
                CommonUtils.savePreferencesString(context, AppConstants.DISTRIBUTER_LOGIN, "distributor_login");

                CommonUtils.savePreferencesString(context, AppConstants.distributor_login, "0");
                CommonUtils.savePreferencesString(context, AppConstants.dealer_login, "1");
                distributor.setBackgroundResource(R.drawable.rounded_corner_gray_border_layout);
                tvDistributor.setTextColor(Color.parseColor("#333333"));
                count++;
                break;
            case R.id.a_g_part_tv_distributor:
                //  CommonUtils.savePreferencesBoolean(GravityPartnerLogin.this,"distributor_login",true);
                distributor.setBackgroundResource(R.drawable.rounded_corner_gray_border_black_bg_layout);
                tvDistributor.setTextColor(Color.parseColor("#FFFFFF"));
                CommonUtils.savePreferencesString(context, AppConstants.distributor_login, "1");
                CommonUtils.savePreferencesString(context, AppConstants.dealer_login, "0");

                CommonUtils.savePreferencesString(context, AppConstants.DEALER_LOGIN, "distributor_login");
                CommonUtils.savePreferencesString(context, AppConstants.DISTRIBUTER_LOGIN, "dealer_login");

                dealer.setBackgroundResource(R.drawable.rounded_corner_gray_border_layout);
                tvDealer.setTextColor(Color.parseColor("#333333"));
                count++;
                break;

            case R.id.a_g_part_tv_next:

                startActivity(new Intent(GravityPartnerLogin.this, SignInSignUpUserActivity.class));
                break;
            case R.id.img_back:
                onBackPressed();
                break;

        }
    }
}
