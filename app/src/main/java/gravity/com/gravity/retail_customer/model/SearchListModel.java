package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 3/5/2018.
 */

public class SearchListModel {
    int img_product,img_like;

    public int getImg_product() {
        return img_product;
    }

    public void setImg_product(int img_product) {
        this.img_product = img_product;
    }

    public int getImg_like() {
        return img_like;
    }

    public void setImg_like(int img_like) {
        this.img_like = img_like;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_price() {
        return pro_price;
    }

    public void setPro_price(String pro_price) {
        this.pro_price = pro_price;
    }

    public String getPro_rating() {
        return pro_rating;
    }

    public void setPro_rating(String pro_rating) {
        this.pro_rating = pro_rating;
    }

    String pro_name,pro_price,pro_rating;




}
