package gravity.com.gravity.retail_customer.fragment;


import android.Manifest;
import android.content.Context;

import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Typeface;


import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.content.Intent;


import android.os.Bundle;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;


import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;


import com.google.android.gms.location.places.AutocompleteFilter;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;


import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import gravity.com.gravity.DataInterface.FileUploadInterface;
import gravity.com.gravity.R;

import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.retofit.ApiInterface;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;
import gravity.com.gravity.plumber.activity.RequestDetails;
import gravity.com.gravity.plumber.fragment.FragMyServiceRequest;
import gravity.com.gravity.plumber.model.CustomerModel;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.retail_customer.model.PlumUserModel;
import gravity.com.gravity.retail_customer.model.PlumberListResponse;
import gravity.com.gravity.retail_customer.model.PlumberUserDetailsResponse;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.service.Retrofit.retrofit.MadhuFileUploader;
import gravity.com.gravity.service.Retrofit.retrofit.RetrofitHandler;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import gravity.com.gravity.utils.FunNoParam;
import gravity.com.gravity.utils.GpsLocationReceiver;
import gravity.com.gravity.utils.gps.GPSTracker2;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

/**
 * Created by User on 6/4/2018.
 */




public class ShippingLocation extends Fragment implements OnMapReadyCallback,AdapterView.OnItemClickListener{
    private String img_path="";
    String product_name="", prod_category="", product_subCategory="", descripion="";
    private GoogleMap map;
    Context context ;
    Button btn_locate_plmbr;
    SupportMapFragment mMapFragment;
    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection";
    String USER_ID = "";
    public static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private ArrayList<String> imageList;
    private String warranty="";
    String latitude="",longitude="";
    PlaceAutocompleteFragment places;
    private  static View view;
    GoogleMap mMap;
    GoogleMap mMapplumber;
    GpsLocationReceiver receiver;
    GPSTracker2 tracker;
    Location loc;
    TextView plumber_name,plumber_mob,plum_service_charge;
    CircleImageView plumber_pic;
    private APIInterface apiInterface;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private LinearLayout llCustomerDetails;
    private ApiInterface mInterface;
    Bundle plmbrDetail;

    private static final String REQUEST_PERMISSIONS_WHO_PREFIX = "@android:requestPermissions:";

    private static final String LOG_TAG = "Autocomplete Places";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
   // https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters


    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyAL3y2ti1MIN4b_OhHGx9mksG1tfJ_8IGY";
    private String complian_id="";
    private FcmMessage chat;
    private  String plumberLat="";
    private  String plumberLong="";
    private List<Location> locations;
    private TextView catogory_txt,tvViewAll;

    private GoogleMap googleMap;
    private MarkerOptions options = new MarkerOptions();
    private ArrayList<LatLng> latlngs = new ArrayList<>();
    private String cityGps = "";
    private String stateNameGps = "";
    private String Complain_id="",plumber_id="";
    private PlumberUserDetailsResponse plumberUserDetailsResponse;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.shipping_location, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        context = getActivity();

        apiInterface = APIClient.getClient().create(APIInterface.class);
        plumberUserDetailsResponse=new PlumberUserDetailsResponse();

        imageList = new ArrayList<>();
        tracker = new GPSTracker2(context);
        plumber_pic=view.findViewById(R.id.plumber_pic);
        tvViewAll=view.findViewById(R.id.tvViewAll);


        catogory_txt=view.findViewById(R.id.catogory_txt);
        llCustomerDetails=view.findViewById(R.id.llCustomerDetails);
        plumber_name=view.findViewById(R.id.tv_plumber_name);
        plumber_mob=view.findViewById(R.id.tv_mobileNo);
        plum_service_charge=view.findViewById(R.id.tv_service_charge);
        btn_locate_plmbr=view.findViewById(R.id.btn_locate_plmbr);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        plum_service_charge.setText("₹ 200");
        USER_ID = CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_ID);




        if(getArguments()!=null){

            USER_ID = CommonUtils.getPreferencesString(getActivity(), AppConstants.USER_ID);

           // Toast.makeText(context,getArguments().getString(IConstants.IApp.PARAM_2),Toast.LENGTH_LONG).show();
            Log.e("chag",getArguments().getString(IConstants.IApp.PARAM_2)+"");
            chat=new Gson().fromJson(getArguments().getString(IConstants.IApp.PARAM_2),FcmMessage.class);

            if(chat!=null) {
                btn_locate_plmbr.setVisibility(View.GONE);
                llCustomerDetails.setVisibility(View.VISIBLE);
                tvViewAll.setPaintFlags(tvViewAll.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
                Log.e("getMyPicLog",chat.getMyPic());
                Log.e("customerLog",chat.getMyPic()+":::"+chat.getToId());
                Log.e("plumber_idLog",chat.getMyId());
                Log.e("complianLog",chat.getPostId());
                Log.e("USER_IDLog",USER_ID);
                plumber_id=chat.getMyId();
                complian_id=chat.getPostId();
               // callCustomerDetailAPI(chat.getPostId(),chat.getMyId(),chat.getToId());
                callUserPlumberDetails(chat.getPostId(),chat.getMyId(),chat.getToId());
            }

                else {
                btn_locate_plmbr.setVisibility(View.VISIBLE);

                //after testing frgment plumberdetail visivility would be gone
                llCustomerDetails.setVisibility(View.GONE);

            }
        }

        if (CommonUtils.getPreferences(context,AppConstants.PLUMBER_LIST_KEY).equalsIgnoreCase("PlumberList")) {


           // Toast.makeText(context, "PlumberList", Toast.LENGTH_SHORT).show();
        }






        llCustomerDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle=new Bundle();
                bundle.putSerializable("AllDetails",plumberUserDetailsResponse);
                FragPlumberDtails fragment = new FragPlumberDtails();
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
               // callUserPlumberDetails();

            }
        });

        tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=new Bundle();
                bundle.putSerializable("AllDetails",plumberUserDetailsResponse);
                FragPlumberDtails fragment = new FragPlumberDtails();
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
               // callUserPlumberDetails();


            }
        });




        plumber_mob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:9452132576"));
                startActivity(intent);
            }
        });

        checkPermission();
        if (receiver != null) {
            checkGpsEnable();
        }


        btn_locate_plmbr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(CommonUtils.getPreferences(context,AppConstants.CURRENT_LAT)!=null&&CommonUtils.getPreferences(context,AppConstants.CURRENT_LONGI)!=null) {

                    latitude=CommonUtils.getPreferences(context,AppConstants.CURRENT_LAT);
                    longitude=CommonUtils.getPreferences(context,AppConstants.CURRENT_LONGI);
                    //  Toast.makeText(getActivity(),longitude  , Toast.LENGTH_SHORT).show();
                    // Toast.makeText(getActivity(), place.getName() + ":::" + address, Toast.LENGTH_SHORT).show();
                    Log.e("Currentlat",latitude);
                    Log.e("Currentlongitude",longitude);

                    // map.addMarker(new MarkerOptions().position(str));
                    callLocationData();

                }

                else {

                    callLocationData();
                }




            }
        });


       /* AutoCompleteTextView autoCompView = (AutoCompleteTextView)view.findViewById(R.id.autoCompleteTextView);
        autoCompView.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.place_list_item));
        autoCompView.setOnItemClickListener(this);

*/
       //  PlaceAutocompleteFragment places= (PlaceAutocompleteFragment)getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

//   <---------------code start for autocomplete fragment------------------------->
         places = (PlaceAutocompleteFragment)getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);


        cityGps = CommonUtils.getPreferencesString(context, AppConstants.CurrentCity);
        stateNameGps = CommonUtils.getPreferencesString(context, AppConstants.StateName);

        if ((cityGps != null) && (!TextUtils.isEmpty(cityGps))) {

            places.setText(cityGps);
         //   places.fo
        }

        ImageView searchIcon = (ImageView) ((LinearLayout) places.getView()).getChildAt(0);
        //View imageView=(View) ((LinearLayout) places.getView()).getChildAt(2);
        searchIcon.setVisibility(View.GONE);

        //LatLng latLng=new LatLng()




        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        places.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                final LatLng location = place.getLatLng();

                if(location!=null) {
                    latitude= String.valueOf(place.getLatLng().latitude);
                    longitude=String.valueOf(place.getLatLng().longitude);
                  //  Toast.makeText(getActivity(),longitude  , Toast.LENGTH_SHORT).show();
                   // Toast.makeText(getActivity(), place.getName() + ":::" + address, Toast.LENGTH_SHORT).show();
                    Log.e("firbaselat",latitude);
                    Log.e("firebaselongitude",longitude);
                    LatLng str = place.getLatLng();
                }

                else {


                    if(CommonUtils.getPreferences(context,AppConstants.CURRENT_LAT)!=null&&CommonUtils.getPreferences(context,AppConstants.CURRENT_LONGI)!=null) {

                        latitude = CommonUtils.getPreferences(context, AppConstants.CURRENT_LAT);
                        longitude = CommonUtils.getPreferences(context, AppConstants.CURRENT_LONGI);

                        Log.e("Currentlat", latitude);
                        Log.e("Currentlongitude", longitude);
                    }


                }


            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getActivity(), status.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        //   <---------------code end for autocomplete fragment------------------------->

      /*  product_name=getArguments().getString("PRODUCT_NAME");
        prod_category=getArguments().getString("CATEGORY");
        product_subCategory=getArguments().getString("SUB_CATEGORY");
        descripion=getArguments().getString("PRODUCT_DESCRIPTION");*/


       if(getArguments()!=null) {
           imageList.clear();
           product_name = getArguments().getString("PRODUCT_NAME");
           prod_category = getArguments().getString("CATEGORY");
           product_subCategory = getArguments().getString("SUB_CATEGORY");
           descripion = getArguments().getString("PRODUCT_DESCRIPTION");
           warranty = getArguments().getString("WARRENT");

       }


       // Toast.makeText(getActivity(), "productName" + product_name + " " + prod_category, Toast.LENGTH_SHORT).show();
      //  Toast.makeText(getActivity(), "imageSize>>" + imageList.size(), Toast.LENGTH_SHORT).show();
        Log.e("imagepath",img_path);
        SupportMapFragment mapFrag = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
        return view;
    }

    private void callUserPlumberDetails(String complain_no, String myId, String toId) {

            Call<PlumUserModel> call=apiInterface.getPlumberUserDetails(plumber_id,USER_ID,complain_no, "2");
            call.enqueue(new Callback<PlumUserModel>() {
                @Override
                public void onResponse(Call<PlumUserModel> call, Response<PlumUserModel> response) {
                  //  plumberUserList.clear();
                    PlumUserModel plumberListDetailsModelList = response.body();
                    if (plumberListDetailsModelList != null) {
                        if (plumberListDetailsModelList.getResponse() != null &&plumberListDetailsModelList.getResponse()!=null) {

                            plumberUserDetailsResponse=plumberListDetailsModelList.getResponse();
                               //  plumberUserList.addAll(plumberListDetailsModelList.getResponse());

                                    if(plumberListDetailsModelList.getResponse().getComplaint().getComplaintNo()!=null) {
                                        Complain_id = plumberListDetailsModelList.getResponse().getComplaint().getComplaintNo();
                                        Log.e("complainObse", complian_id);
                                        Log.e("complainServer", plumberListDetailsModelList.getResponse().getComplaint().getComplaintNo());
                                        if(plumberListDetailsModelList.getResponse().getPlumber().getFirstName()!=null) {

                                            plumber_name.setText(CommonUtils.NameCaps(plumberListDetailsModelList.getResponse().getPlumber().getFirstName()));
                                            plum_service_charge.setText("₹ 280");

                                        }
                                        if(plumberListDetailsModelList.getResponse().getPlumber().getPhone()!=null) {

                                            plumber_mob.setText(plumberListDetailsModelList.getResponse().getPlumber().getPhone());

                                        }
                                    }
                                    if (plumberListDetailsModelList.getResponse().getPlumber().getProfilePic()!=null) {
                                        plumber_pic.setImageResource(R.mipmap.user);
                                    } else{
                                        Picasso.with(context).load(plumberListDetailsModelList.getResponse().getPlumber().getProfilePic()).into(plumber_pic);
                                    }


                                    if(plumberListDetailsModelList.getResponse().getPlumber().getLatitude()!=null&&plumberListDetailsModelList.getResponse().getPlumber().getLatitude()!=null) {
                                        plumberLat = plumberListDetailsModelList.getResponse().getPlumber().getLatitude();
                                        plumberLong = plumberListDetailsModelList.getResponse().getPlumber().getLongitude();
                                    }

                                }
                                else {
                            Toast.makeText(context, "Data not found", Toast.LENGTH_SHORT).show();

                        }

                    }



                        //  Toast.makeText(getContext(), "Plumber list Size"+PlumberListDetailsModelList.getPlumberListDetailsModel().size(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<PlumUserModel> call, Throwable throwable) {
                    Log.d("ASD", "onFailure: ");
                }
            });
        }




    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(context, per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    protected void checkPermission() {

        if (isPerGiven(Manifest.permission.ACCESS_FINE_LOCATION)
                && isPerGiven(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            return;
        }

        String[] arr = new String[2];
        arr[0] = Manifest.permission.ACCESS_FINE_LOCATION;
        arr[1] = Manifest.permission.ACCESS_COARSE_LOCATION;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arr, 1);
        }
    }

    private boolean checkGpsEnable() {
        if (!tracker.isGPSEnabled()) {
            setReciever();
            tracker.showSettingsAlert();
            return false;
        } else {
            tracker.dismissAlert();
            loc=tracker.getLocation();

            Timer t = new Timer();
            t.scheduleAtFixedRate(
                    new TimerTask() {
                        @Override
                        public void run() {

                            if(loc==null){
                                loc=tracker.getLocation();
                            }else {
                                HashMap<String, String> map = tracker.saveLocation(loc);
                                // city = map.get("1");
                                //  state = map.get("2");


                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //updateAdapters();

                                    }
                                });
                            }

                            t.cancel();

                        }

                    },
                    0,
                    1000);


            return true;
        }
    }

    private void setReciever() {
        if (receiver != null) {
            return;
        }
        receiver = new GpsLocationReceiver();
        receiver.setmListener(new FunNoParam() {
            @Override
            public void done() {



            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        context.registerReceiver(receiver, filter);
    }


    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
       // Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();


    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

          //  URL url = new URL(sb.toString());
            String strURL="https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+input+"&types=establishment&language=en&key=AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
            URL url = new URL(strURL);

            System.out.println("URL: " + url);
            Log.d("URL",""+url);

            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;

    }
    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }




    private void callLocationData() {

        //img_path = getArguments().getString("Image");

        imageList.clear();

        if(getArguments()!=null)
        {

        if (!getArguments().getString("BillImage").equalsIgnoreCase("")) {
            imageList.add(getArguments().getString("BillImage"));
        }


    }
        CommonUtils.showProgress(context);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();

        Call<ResponseBody> call=null;



        if (imageList != null && imageList.size() > 0) {

            Map<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("user_id", USER_ID);
            hashMap.put("category_name",prod_category);
            hashMap.put("subcategory_name",product_subCategory);
            hashMap.put("product_name",product_name );
            hashMap.put("complaint_description", descripion);
            hashMap.put("warranty", warranty);
            hashMap.put("longitude", latitude);
            hashMap.put("latitude", longitude);
            MadhuFileUploader uploader = new MadhuFileUploader();
            uploader.uploadFile(hashMap, imageList, "warranty_paper", context);
            Log.e("retoImageupload", hashMap.toString());

            call = service.raise_complaint(uploader.getPartMap(), uploader.getPartBodyArr());

        }
        else {

            RequestBody user_id1 = RequestBody.create(MediaType.parse("text/plain"),USER_ID);
            RequestBody category_name = RequestBody.create(MediaType.parse("text/plain"),prod_category);
            RequestBody subcategory_name = RequestBody.create(MediaType.parse("text/plain"),product_subCategory);
            RequestBody product_name1 = RequestBody.create(MediaType.parse("text/plain"),product_name);
            RequestBody complaint_description = RequestBody.create(MediaType.parse("text/plain"),descripion);
            RequestBody warranty1 = RequestBody.create(MediaType.parse("text/plain"),warranty);
            RequestBody longitude1 = RequestBody.create(MediaType.parse("text/plain"),longitude);
            RequestBody latitude1 = RequestBody.create(MediaType.parse("text/plain"),latitude);
            call = service.uploadFileWithPartMap(user_id1, category_name,subcategory_name,product_name1,complaint_description,warranty1,
                    longitude1,latitude1 );

        }


            call.enqueue(new Callback<ResponseBody>() {
            public boolean status;

            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();
                String str = "";

                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {

                    JSONObject jsonObject = new JSONObject(str);
                    int status = jsonObject.getInt("status");
                    String messageimg = jsonObject.getString("msg");


                    if (status==1) {
                     //   Toast.makeText(context, messageimg, Toast.LENGTH_SHORT).show();
                        if (messageimg != null && !messageimg.isEmpty()) {
                             Toast.makeText(context, "Your complain is registered sucessfully.", Toast.LENGTH_SHORT).show();
                            complian_id= jsonObject.getString("complaint_no");

                            Bundle bundle=new Bundle();
                            bundle.putString(AppConstants.LAT,latitude);
                            bundle.putString(AppConstants.LONGI,longitude);
                            bundle.putString(AppConstants.COMPLAIN_ID,complian_id);

                            fragmentManager = getActivity().getSupportFragmentManager();//getSupportFragmentManager();
                            FragPlumberList fragment = new FragPlumberList();
                            fragment.setArguments(bundle);
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            fragmentTransaction.commit();

                            } else {

                            Toast.makeText(context, "Your complain is  not registered .", Toast.LENGTH_SHORT).show();

                            Log.e("userpic", "null");}
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                CommonUtils.dismissProgress();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {


     /*   mMap = googleMap;
        Marker m1 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(38.609556, -1.139637))
                .anchor(0.5f, 0.5f)
                .title("Title1")
                .snippet("Snippet1")
                .icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_media_pause)));


        Marker m2 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(40.4272414,-3.7020037))
                .anchor(0.5f, 0.5f)
                .title("Title2")
                .snippet("Snippet2")
                .icon(BitmapDescriptorFactory.fromResource(android.R.drawable.ic_media_pause)));*/





       // googleMap=googleMap;

    /*    mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);*/



        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
      //  mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        loc=tracker.getLocation();
        double lat=tracker.getLatitude();
        double longitute=tracker.getLongitude();

        latlngs.add(new LatLng(lat, longitute));
        //latlngs.add(new LatLng(28.535516100000002, 77.3910265));
       // latlngs.add(new LatLng(28.5870353, 77.3161091));


        if(!plumberLat.equals("")&& !plumberLong.equals("")){
            latlngs.add(new LatLng(Double.valueOf(plumberLat),
                    Double.valueOf(plumberLong)));

            latlngs.add(new LatLng(28.535516100000002, 77.3910265));
             latlngs.add(new LatLng(28.5870353, 77.3161091));
            Log.e("LatC",String.valueOf(lat));
            Log.e("LatCLong",String.valueOf(lat));
            Log.e("LatCplLat",plumberLat);
            Log.e("LatCplLong",plumberLong);
            Log.e("LatCplLongohk",plumberLong);
            //latlngs.add(new LatLng(19.136326, 72.827660));

           /* LatLng latLongPlumber=new LatLng(Double.valueOf(plumberLat),Double.valueOf(plumberLong));


            CameraPosition cameraPositionPlumber = new CameraPosition.Builder()
                    .target(latLongPlumber)
                    .zoom(17).build();
            //Zoom in and animate the camera.
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPositionPlumber));*/



            for (LatLng point : latlngs) {
                options.position(point);
                options.title("someTitle");
                options.snippet("someDesc");
                Log.e("latlngssize",String.valueOf(latlngs.size()));
                // options .anchor(0.5f, 0.5f);
                googleMap.addMarker(options);

                CameraPosition cameraPosition1 = new CameraPosition.Builder()
                        .target(point)
                        .zoom(20).build();
                //Zoom in and animate the camera.
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

            } }

            else {

            for (LatLng point : latlngs) {
                options.position(point);
                options.title("someTitle");
                options.snippet("someDesc");
                Log.e("latlngssize",String.valueOf(latlngs.size()));
                // options .anchor(0.5f, 0.5f);
                googleMap.addMarker(options);

                CameraPosition cameraPosition1 = new CameraPosition.Builder()
                        .target(point)
                        .zoom(20).build();
                //Zoom in and animate the camera.
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

            }
        }

      /*  mMap = googleMap;
        mMapplumber = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        loc=tracker.getLocation();
        double lat=tracker.getLatitude();
        double longitute=tracker.getLongitude();
        System.out.println("GPS LOCATION IS"+lat+", "+longitute);
        Log.d("GPS",lat+""+longitute);
        LatLng noida = new LatLng(lat, longitute);
        float zoomLevel = 16.0f; //This goes up to 21
        mMap.getCameraPosition();
        mMap.addMarker(new MarkerOptions().position(noida).title(""));
        pointToPosition(noida);
        LatLng mumbai = new LatLng(Double.valueOf(plumberLat), Double.valueOf(plumberLong));
        float zoomLevel1 = 16.0f; //This goes up to 21
        mMapplumber.getCameraPosition();
        mMapplumber.addMarker(new MarkerOptions().position(mumbai).title(""));
        pointToPositionPlumber(mumbai);*/
    }

    protected Marker createMarker(double latitude, double longitude, String title, String snippet, int iconResID) {

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }


    private void pointToPosition(LatLng position) {
        //Build camera position
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(position)
                .zoom(14).build();
        //Zoom in and animate the camera.
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }



    private void callPlaceAutocompleteActivityIntent() {

    }
    public void unRegister() {
        if (receiver != null) {
            context.unregisterReceiver(receiver);
        }
    }

    @Override
    public void onDestroy() {
        unRegister();
        super.onDestroy();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(getActivity()!=null) {
            android.app.FragmentManager fragmentManager = getActivity().getFragmentManager();
            android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(places);
            //Use commitAllowingStateLoss() if getting exception

            places = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
            checkPermission();
            if (receiver != null) {
                checkGpsEnable();
            }






        // showLandingFragment(vv);



    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean isCompGranted = false;
        for (String perm : permissions) {
            if (!isPerGiven(perm)) {
                isCompGranted = true;
            }
        }

        if (isCompGranted) {
            checkPermission();
        } else {
        }

    }

    private void callCustomerDetailAPI(String complian_id, String pumber_id, String customer_id) {

        Call<CustomerModel> call = apiInterface.getPushToCustDEtails(customer_id,complian_id,pumber_id);
        call.enqueue(new Callback<CustomerModel>() {
            @Override
            public void onResponse(Call<CustomerModel> call, Response<CustomerModel> responseBody) {

                CustomerModel customerModel=responseBody.body();

                if (responseBody != null) {
                    if (customerModel.getStatus() == 1) {
                        if(customerModel.getResponse()!=null&&customerModel.getResponse().size()>0) {
                            if(customerModel.getResponse().get(0).getComplaintNo()!=null) {
                                Complain_id = customerModel.getResponse().get(0).getComplaintNo();
                                Log.e("complainObse", complian_id);
                                Log.e("complainServer", customerModel.getResponse().get(0).getComplaintNo());
                                if(customerModel.getResponse().get(0).getFirstName()!=null) {

                                    plumber_name.setText(CommonUtils.NameCaps(customerModel.getResponse().get(0).getFirstName()));
                                    // plum_service_charge.setText(customerModel.getResponse().get(0).getAdhaarNo());
                                    plum_service_charge.setText("₹ 280");

                                    //   plmbrDetail=new Bundle();
                                }  // plmbrDetail.putString("NAME",CommonUtils.NameCaps(customerModel.getResponse().get(0).getFirstName()+" "+customerModel.getResponse().get(0).getLastName()));
                                if(customerModel.getResponse().get(0).getFirstName()!=null) {

                                    plumber_mob.setText(customerModel.getResponse().get(0).getPhone());

                                }    // plmbrDetail.putString("MOBILE",customerModel.getResponse().get(0).getPhone());
                                // plmbrDetail.putString("MOBILE",customerModel.getResponse().get(0).getPhone());
                            }
                            if (customerModel.getResponse().get(0).getProfilePic()!=null) {
                                plumber_pic.setImageResource(R.mipmap.user);
                            } else{
                                Picasso.with(context).load(customerModel.getResponse().get(0).getProfilePic()).into(plumber_pic);
                            }


                            if(customerModel.getResponse().get(0).getLatitude()!=null&&customerModel.getResponse().get(0).getLatitude()!=null) {
                                plumberLat = customerModel.getResponse().get(0).getLatitude();
                                plumberLong = customerModel.getResponse().get(0).getLongitude();
                            }

                        } } else {
                        Toast.makeText(context, "Fail Please try again 3", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CustomerModel> call, Throwable t) {
                Toast.makeText(context, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void pointToPositionPlumber(String latitude, String longitude) {

    }


}
 class PlaceAutocompleteAdapter extends RecyclerView.Adapter<PlaceAutocompleteAdapter.PlaceViewHolder> implements Filterable {

    public interface PlaceAutoCompleteInterface{
        public void onPlaceClick(ArrayList<PlaceAutocomplete> mResultList, int position);
    }

    Context mContext;
    PlaceAutoCompleteInterface mListener;
    private static final String TAG = "PlaceAutocompleteAdapter";
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    ArrayList<PlaceAutocomplete> mResultList;

    private GoogleApiClient mGoogleApiClient;
    private LatLngBounds mBounds;
    private int layout=R.layout.auto_places_places;
    private AutocompleteFilter mPlaceFilter;

    public PlaceAutocompleteAdapter(Context context, int resource, GoogleApiClient googleApiClient,
                                    LatLngBounds bounds, AutocompleteFilter filter){
        this.mContext = context;
        layout = resource;
        mGoogleApiClient = googleApiClient;
        mBounds = bounds;
        mPlaceFilter = filter;
        this.mListener = (PlaceAutoCompleteInterface)mContext;
    }


    public void clearList(){
        if(mResultList!=null && mResultList.size()>0){
            mResultList.clear();
        }
    }


    public void setBounds(LatLngBounds bounds) {
        mBounds = bounds;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    mResultList = getAutocomplete(constraint);
                    if (mResultList != null) {
                        // The API successfully returned results.
                        results.values = mResultList;
                        results.count = mResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    //notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private ArrayList<PlaceAutocomplete> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.i("", "Starting autocomplete query for: " + constraint);

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    mBounds, mPlaceFilter);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
//                Toast.makeText(mContext, "Error contacting API: " + status.toString(),
//                        Toast.LENGTH_SHORT).show();
                Log.e("", "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.i("", "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Copy the results into our own data structure, because we can't hold onto the buffer.
            // AutocompletePrediction objects encapsulate the API response (place ID and description).

            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                AutocompletePrediction prediction = iterator.next();
                // Get the details of this prediction and copy it into a new PlaceAutocomplete object.
                resultList.add(new PlaceAutocomplete(prediction.getPlaceId(), (CharSequence) STYLE_BOLD));
            }

            // Release the buffer now that all data has been copied.
            autocompletePredictions.release();

            return resultList;
        }
        Log.e("", "Google API client is not connected for autocomplete query.");
        return null;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(layout, viewGroup, false);
        PlaceViewHolder mPredictionHolder = new PlaceViewHolder(convertView);
        return mPredictionHolder;
    }


    @Override
    public void onBindViewHolder(PlaceViewHolder mPredictionHolder, final int i) {
        mPredictionHolder.mAddress.setText(mResultList.get(i).description);

        mPredictionHolder.mAddress.setText("mumbai");

        mPredictionHolder.mParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPlaceClick(mResultList,i);
            }
        });

    }

    @Override
    public int getItemCount() {
        if(mResultList != null)
            return mResultList.size();
        else
            return 0;
    }

    public PlaceAutocomplete getItem(int position) {
        return mResultList.get(position);
    }



    public class PlaceViewHolder extends RecyclerView.ViewHolder {
        //        CardView mCardView;
        public RelativeLayout mParentLayout;
        public TextView mAddress;

        public PlaceViewHolder(View itemView) {
            super(itemView);
            mParentLayout = (RelativeLayout)itemView.findViewById(R.id.predictedRow);
            mAddress = (TextView)itemView.findViewById(R.id.address);
        }

    }
    public class PlaceAutocomplete {

        public CharSequence placeId;
        public CharSequence description;

        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }




 }
