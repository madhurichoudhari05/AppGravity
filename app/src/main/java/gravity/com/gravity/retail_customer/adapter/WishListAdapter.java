package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.ProductDetailActivity;
import gravity.com.gravity.retail_customer.model.WishListModel;

/**
 * Created by user on 2/16/2018.
 */

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.ViewHolder> {

    ArrayList<WishListModel> arrayList = new ArrayList<>();
    List<String> spinList=new ArrayList<>();

    Context context;

    public WishListAdapter(ArrayList<WishListModel> arrayList, Context context) {


        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public WishListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {



        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_row_item, parent, false);

        WishListAdapter.ViewHolder recyclerViewHolder = new WishListAdapter.ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductDetailActivity.class));
            }
        });

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(WishListAdapter.ViewHolder holder, int position) {

        holder.itemIMG.setImageResource(arrayList.get(position).getPro_img_ID());
        holder.productName.setText(arrayList.get(position).getProductName());
       holder.remove.setImageResource(arrayList.get(position).getImg_remove());
        holder.productPrice.setText(arrayList.get(position).getProductPrice());
        holder.proRate.setText(arrayList.get(position).getRating());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  itemIMG,remove;
        TextView productName,productPrice,proRate;

        public ViewHolder(View itemView) {
            super(itemView);

            itemIMG = (ImageView) itemView.findViewById(R.id.img);
            productName=(TextView)itemView.findViewById(R.id.pro_name);
           productPrice=itemView.findViewById(R.id.price);
           proRate=itemView.findViewById(R.id.rating_star);
           remove=itemView.findViewById(R.id.remove);



        }
    }
}
