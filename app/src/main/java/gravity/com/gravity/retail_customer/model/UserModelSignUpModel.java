package gravity.com.gravity.retail_customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModelSignUpModel {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("OTP")
    @Expose
    private Integer oTP;
    @SerializedName("response")
    @Expose
    private List<UserModelSignUpPOJO> response = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getOTP() {
        return oTP;
    }

    public void setOTP(Integer oTP) {
        this.oTP = oTP;
    }

    public List<UserModelSignUpPOJO> getResponse() {
        return response;
    }

    public void setResponse(List<UserModelSignUpPOJO> response) {
        this.response = response;
    }
}
