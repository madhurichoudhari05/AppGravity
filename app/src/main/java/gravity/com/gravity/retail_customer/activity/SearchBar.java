package gravity.com.gravity.retail_customer.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import gravity.com.gravity.R;

/**
 * Created by admin on 3/22/2018.
 */

public class SearchBar extends AppCompatActivity {

ImageView img_back;
    AutoCompleteTextView searchEditText;
    private static final String[] COUNTRIES = new String[] {
            "Faucets", "showers", "tap", "fuzu", "Spain"
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.searchbar_activity);

        img_back = findViewById(R.id.img_back);
        searchEditText=findViewById(R.id.searchEditText);
        searchEditText.getDropDownVerticalOffset();


        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,COUNTRIES);
        searchEditText.setAdapter(adapter);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }
}
