package gravity.com.gravity.retail_customer.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.CustomerReviewAdapter;
import gravity.com.gravity.retail_customer.adapter.SlidingImage_Adapter;
import gravity.com.gravity.retail_customer.model.CustomerReviewModel;

/**
 * Created by Admin on 3/5/2018.
 */

public class ProductDetailActivity extends AppCompatActivity {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.drawable.home_a, R.drawable.home_b, R.drawable.homec};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    ImageView back;
    Context context;
    ArrayList<CustomerReviewModel> list = new ArrayList<>();

    RelativeLayout relativeLayout;
    TextView textView;
    RecyclerView recyclerView_review;
    CustomerReviewAdapter adapter1;
    FrameLayout review;
    NestedScrollView nested_scrollview;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        //   relativeLayout =findViewById(R.id.relative_w_riview);
        textView = findViewById(R.id.rupee);
        review = findViewById(R.id.review);
        nested_scrollview = findViewById(R.id.nested_scrollview);
        recyclerView_review = findViewById(R.id.recyclerView_review);
        textView.setText("₹ 1397");
        context = ProductDetailActivity.this;

       // ViewCompat.setNestedScrollingEnabled(recyclerView_review, false);
//       scrollView.setNestedScrollingEnabled(false);
      //  nested_scrollview.setNestedScrollingEnabled(false);
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, ReviewActivity.class));

            }
        });


        adapter1 = new CustomerReviewAdapter(list, context);
        recyclerView_review.setLayoutManager(new LinearLayoutManager(context));
        recyclerView_review.setAdapter(adapter1);
        recyclerView_review.invalidate();

       /* relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context,ActivityCustomerReviews.class));
            }
        });
*/
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imageSlider();

       /* RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);*/
        for (int i = 0; i < 4; i++) {

            int[] pic = new int[]{R.drawable.ic_launcher_background};

            CustomerReviewModel model = new CustomerReviewModel(pic[0], "Priya Mahur", "on 30 january 2018", "Verified Purchased", "sed ut udana padsta neo omnis iste natus", 1);
            list.add(model);
        }
        adapter1.notifyDataSetChanged();



    }

    private void imageSlider() {

        for (int i = 0; i < IMAGES.length; i++) ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);

        mPager.setAdapter(new SlidingImage_Adapter(ProductDetailActivity.this, ImagesArray));
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }
}
