package gravity.com.gravity.retail_customer.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.SearchBar;
import gravity.com.gravity.retail_customer.adapter.HomeNewProductAdapter;
import gravity.com.gravity.retail_customer.adapter.HomeProductAdapter;
import gravity.com.gravity.retail_customer.adapter.HomeSpecialSeriesAdapter;
import gravity.com.gravity.retail_customer.adapter.SlidingImage_Adapter;
import gravity.com.gravity.retail_customer.model.NewProductModel;
import gravity.com.gravity.retail_customer.model.ProductModel;
import gravity.com.gravity.utils.SpacesItemDecoration;

/**
 * Created by user on 2/20/2018.
 */

public class FragmentHome extends Fragment implements View.OnClickListener {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.drawable.home_a, R.drawable.home_b, R.drawable.homec};
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();
    RecyclerView productRecycler;
    HomeProductAdapter homeProductAdapter;
    ProductModel productModel;
    NewProductModel newProductModel;

    HomeSpecialSeriesAdapter homeSpecialSeriesAdapter;
    HomeNewProductAdapter homeNewProductAdapter;
    ArrayList<NewProductModel> newProductArrayList;
    ArrayList<ProductModel> productArrayList;
    Context context;
    RecyclerView recyclerNewProduct, recySpecialSeries;
    Button btnOffer, btnDiscount;
    TextView txtOfferDiscount;
    ImageView addToCard, wishList;
    CirclePageIndicator indicator;
    LinearLayout linear_search;
    ImageView img_back;
    private NestedScrollView nested;


    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        context = getActivity();

      /*  InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        */


        linear_search = view.findViewById(R.id.linear_search);

        linear_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, SearchBar.class));
            }
        });


        addToCard = view.findViewById(R.id.addToCard);
        wishList = getActivity().findViewById(R.id.account);
        recyclerNewProduct = view.findViewById(R.id.a_home_gridview);
        productRecycler = view.findViewById(R.id.recy_a_home);
        btnOffer = view.findViewById(R.id.a_home_btn_offer);
        btnDiscount = view.findViewById(R.id.a_home_btn_descount);
        txtOfferDiscount = view.findViewById(R.id.a_home_txt_offer_dis);
        recySpecialSeries = view.findViewById(R.id.a_home_re_spe_series);
        mPager = (ViewPager) view.findViewById(R.id.pager);
        indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        nested = view.findViewById(R.id.nested);


        btnOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDiscount.setTextColor(Color.WHITE);
                btnOffer.setTextColor(Color.BLACK);
                btnOffer.setBackgroundResource(R.drawable.rounded_corner_layout);
                btnDiscount.setBackgroundResource(R.drawable.rounded_corner_black_bg);
                txtOfferDiscount.setText("Offer");
            }
        });
        btnDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDiscount.setTextColor(Color.BLACK);
                btnOffer.setTextColor(Color.WHITE);
                btnDiscount.setBackgroundResource(R.drawable.rounded_corner_layout);
                btnOffer.setBackgroundResource(R.drawable.rounded_corner_black_bg);
                txtOfferDiscount.setText("Discounts");
            }
        });


        productArrayList = new ArrayList<>();
        imageSlider();
        ourProductList();
        setNewProduct();
        setSpecialSeries();

        nested.getParent().requestChildFocus(nested, nested);
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void imageSlider() {

        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);


        mPager.setAdapter(new SlidingImage_Adapter(getActivity(), ImagesArray));

        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();

        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }

    private void ourProductList() {

        productArrayList = new ArrayList<>();

        productModel = new ProductModel();
        productModel.setImgID(R.mipmap.home1);
        productArrayList.add(productModel);


        productModel = new ProductModel();
        productModel.setImgID(R.mipmap.home2);
        productArrayList.add(productModel);

        productModel = new ProductModel();
        productModel.setImgID(R.mipmap.home3);
        productArrayList.add(productModel);

        productModel = new ProductModel();
        productModel.setImgID(R.mipmap.home4);
        productArrayList.add(productModel);


        homeProductAdapter = new HomeProductAdapter(productArrayList, context);

        productRecycler.setLayoutManager(new LinearLayoutManager(context));
        productRecycler.setAdapter(homeProductAdapter);

    }


    private void setNewProduct() {


        newProductArrayList = new ArrayList<>();

        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("Tariam");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("Bathyrom Accesories");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.homec);
        newProductModel.setProductName("Telephonic shower");
        newProductArrayList.add(newProductModel);


        newProductModel = new NewProductModel();
        newProductModel.setImgID(R.drawable.home_a);
        newProductModel.setProductName("taps");
        newProductArrayList.add(newProductModel);

        homeNewProductAdapter = new HomeNewProductAdapter(newProductArrayList, context);
        recyclerNewProduct.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerNewProduct.addItemDecoration(new SpacesItemDecoration(10));
        recyclerNewProduct.setAdapter(homeNewProductAdapter);


    }

    private void setSpecialSeries() {

        productArrayList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            productModel = new ProductModel();
            productModel.setImgID(R.drawable.home_b);
            productArrayList.add(productModel);
        }
        homeSpecialSeriesAdapter = new HomeSpecialSeriesAdapter(productArrayList, context);
        recySpecialSeries.setLayoutManager(new LinearLayoutManager(context));
        //recySpecialSeries.setLayoutManager(new GridLayoutManager(context,1));
        recySpecialSeries.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        recySpecialSeries.addItemDecoration(new SpacesItemDecoration(10, 0));
        recySpecialSeries.setAdapter(homeSpecialSeriesAdapter);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.hamburger_menu:


        }


    }
}
