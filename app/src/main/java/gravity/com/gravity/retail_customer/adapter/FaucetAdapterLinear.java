package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.ProductDetailActivity;
import gravity.com.gravity.retail_customer.model.NewProductModel;

/**
 * Created by admin on 3/24/2018.
 */
public class FaucetAdapterLinear extends RecyclerView.Adapter<FaucetAdapterLinear.ViewHolder1> {

    NewProductModel productModel;
    ArrayList<NewProductModel> arrayList = new ArrayList<>();
    int count;

    Context context;

    public FaucetAdapterLinear(ArrayList<NewProductModel> arrayList, Context context, int count) {


        this.arrayList = arrayList;
        this.count = count;
        this.context = context;

    }

    @Override
    public FaucetAdapterLinear.ViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {



        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.faucet_row_item_verticle, parent, false);



        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductDetailActivity.class));
            }
        });


        return new ViewHolder1(view);


    }

    @Override
    public void onBindViewHolder (FaucetAdapterLinear.ViewHolder1 holder,int position){

        holder.pro_image.setImageResource(arrayList.get(position).getImgID());
        //  holder.pro_image2.setImageResource(arrayList.get(position).getImgID());
        holder.pro_name1.setText(arrayList.get(position).getProductName());
        // holder.pro_name2.setText(arrayList.get(position).getProductName());
        holder.product_discount.setText(arrayList.get(position).getProduct_discount());
        holder.product_price.setText(arrayList.get(position).getProduct_price());
        holder.product_rating_star.setText(arrayList.get(position).getProduct_rating_star());

    }

    @Override
    public int getItemCount () {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView pro_image, pro_image2;
        TextView pro_name1, product_price, product_discount, product_rating_star;


        public ViewHolder1(View itemView) {
            super(itemView);

            pro_image = itemView.findViewById(R.id.image);
            // pro_image2=itemView.findViewById(R.id.image2);
            pro_name1 = itemView.findViewById(R.id.product_name);
            //pro_name2=itemView.findViewById(R.id.product_name2);
            product_discount = itemView.findViewById(R.id.product_discount);

            product_price = itemView.findViewById(R.id.product_price);
            product_rating_star = itemView.findViewById(R.id.product_rating_star);

        }

        @Override
        public void onClick(View view) {
            context.startActivity(new Intent(context, ProductDetailActivity.class));
        }
    }
}
