package gravity.com.gravity.retail_customer.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import gravity.com.gravity.retail_customer.fragment.TabFragMapView;
import gravity.com.gravity.retail_customer.fragment.TabFragmentListView;

/**
 * Created by user on 2/15/2018.
 */

public class DealerListTabAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public DealerListTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
               // TabFragUserSignIn tab1=new TabFragUserSignIn();
                TabFragmentListView tab1=new TabFragmentListView();

                return tab1;
            case 1:
               // TabFragmentSignUp tab2 = new TabFragmentSignUp();
                TabFragMapView tab2=new TabFragMapView();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
