package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.CustomerReviewAdapter;
import gravity.com.gravity.retail_customer.model.CustomerReviewModel;


/**
 * Created by Admin on 3/15/2018.
 */

public class ActivityCustomerReviews extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<CustomerReviewModel> list = new ArrayList<>();
    CustomerReviewAdapter adapter;
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_reviews);

        recyclerView = findViewById(R.id.recyclerView);

        adapter = new CustomerReviewAdapter(list, context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);

        for (int i = 0; i < 10; i++) {

            int[] pic = new int[]{R.drawable.man_t};

            CustomerReviewModel model = new CustomerReviewModel(pic[0], "Priya Mahur", "on 30 january 2018", "Verified Purchased", "sed ut udana padsta neo omnis iste natus", 1);
            list.add(model);
            adapter.notifyDataSetChanged();
        }


    }
}
