package gravity.com.gravity.retail_customer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.FrgPlumberListAdapter;
import gravity.com.gravity.retail_customer.model.DealerListModel;
import gravity.com.gravity.retail_customer.model.PlumberListDetailsModel;
import gravity.com.gravity.retail_customer.model.PlumberListResponse;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 3/6/2018.
 */

public class FragPlumberList extends Fragment {

    private Context context;
    RecyclerView recyclerView;
    FrgPlumberListAdapter adapter;

    ArrayList<PlumberListDetailsModel> plumberList;
    RecyclerView recy_plmbr_list;
    private APIInterface apiInterface;
    public static ImageView ivFilter;

    private    String latitude="",longitude="",user_type="",complain_id="";


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_plumber_list, container, false);
        recy_plmbr_list=view.findViewById(R.id.recy_plmbr_list);
        ivFilter=view.findViewById(R.id.ivFilter);
        context=getActivity();
        plumberList =new ArrayList<>();
        apiInterface= APIClient.getClient().create(APIInterface.class);


        user_type=CommonUtils.getPreferencesString(context,AppConstants.ALL_USER);

        if(getArguments().getString(AppConstants.LAT)!=null&&getArguments().getString(AppConstants.LONGI)!=null)
        {
           latitude=getArguments().getString(AppConstants.LAT);
           longitude=getArguments().getString(AppConstants.LONGI);
           complain_id=getArguments().getString(AppConstants.COMPLAIN_ID);

           Log.e("latitudeplumber",latitude);
           Log.e("longitudeplumber",longitude);
           Log.e("userTypeplumber",user_type);
           Log.e("complain_id",complain_id);

        }


        CommonUtils.savePreferencesString(getActivity(), AppConstants.SCREEN,"plumberList");

        adapter=new FrgPlumberListAdapter(plumberList,getActivity(),this,complain_id);
        recy_plmbr_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        recy_plmbr_list.setAdapter(adapter);


        callPlumberListApi();

        return view;
    }

    private void callPlumberListApi() {
        Call<PlumberListResponse> call=apiInterface.getPlumberList(latitude, longitude);
        call.enqueue(new Callback<PlumberListResponse>() {
            @Override
            public void onResponse(Call<PlumberListResponse> call, Response<PlumberListResponse> response) {
                plumberList.clear();
                PlumberListResponse PlumberListDetailsModelList = response.body();
                if (PlumberListDetailsModelList.getPlumberListDetailsModel() != null &&PlumberListDetailsModelList.getPlumberListDetailsModel().size()>0) {

                  //  Toast.makeText(getContext(), "Plumber list Size"+PlumberListDetailsModelList.getPlumberListDetailsModel().size(), Toast.LENGTH_SHORT).show();

                    plumberList.addAll(PlumberListDetailsModelList.getPlumberListDetailsModel());
                    adapter.notifyDataSetChanged();
                }
                else {

                    Toast.makeText(getContext(), "Plumber is not available", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlumberListResponse> call, Throwable throwable) {
                Log.d("ASD", "onFailure: ");
            }
        });
    }

}