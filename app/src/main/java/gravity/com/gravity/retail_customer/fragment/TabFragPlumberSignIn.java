package gravity.com.gravity.retail_customer.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.ActivityForgotPassword;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.service.Retrofit.model.LoginModel;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 2/15/2018.
 */

public class TabFragPlumberSignIn extends Fragment {
    Button btnSignIn;
    TextView forgot_pass;
    EditText et_password, et_email;
    private APIInterface apiInterface;
    private String Email = "", Password = "";
    ImageView iv_show_pass;
    boolean isChecked = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_signin, container, false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        forgot_pass = view.findViewById(R.id.f_pass);
        et_email = view.findViewById(R.id.et_email);
        et_password = view.findViewById(R.id.et_password);
        iv_show_pass = view.findViewById(R.id.iv_show_pass);

        et_email.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                et_email.setCursorVisible(true);
                return false;
            }
        });
        et_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                et_password.setCursorVisible(true);
                return false;
            }
        });
        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesString(getActivity(), AppConstants.ALL_USER, "2");
                startActivity(new Intent(getActivity(), ActivityForgotPassword.class));
            }
        });

        iv_show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isChecked) {
                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye1);
                    isChecked = true;
                } else {
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye);
                    isChecked = false;
                }

            }


        });

        btnSignIn = view.findViewById(R.id.fra_tab_user_sign_btnsinin);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidation();
            }
        });
        return view;

    }

    private void doValidation() {
        Email = et_email.getText().toString();
        Password = et_password.getText().toString();

        if (TextUtils.isEmpty(Email)) {
            CommonUtils.snackBar("Please Enter Your Registerd Mobile No", et_password);
            et_email.requestFocus();

        } else if (TextUtils.isEmpty(Password)) {
            CommonUtils.snackBar("Please Enter Your Password", et_password);
            et_password.requestFocus();
        } else {

            callLoginApi();
            //Intent i=new Intent(getContext(), UserHomeActivity.class);
            //startActivity(i);

            //loginMethod(email1, pwd1);
        }

    }

    private void callLoginApi() {
        Call<LoginModel> stringCall = apiInterface.getLogin(et_email.getText().toString(), et_password.getText().toString(), FirebaseInstanceId.getInstance().getToken(), "4");
        stringCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                LoginModel LoginModel = response.body();
                if(LoginModel!=null){


                if (LoginModel.getStatus() != null) {
                    if (LoginModel.getStatus() == 1) {
                        CommonUtils.snackBar(LoginModel.getMsg(), et_email);
                        Log.e("listtransaction", "listtransaction::" + LoginModel.getStatus());
                        Log.e("listtransaction", "listtransaction::" + LoginModel.getResponse().get(0).getPlumberOnline());
                        if (LoginModel.getResponse().get(0).getId() != null) {

                            CommonUtils.savePreferencesString(getActivity(), AppConstants.PLUMBER_USER_ID, String.valueOf(LoginModel.getResponse().get(0).getId()));
                        }
                        CommonUtils.savePreferencesString(getActivity(), AppConstants.USER_ID, String.valueOf(LoginModel.getResponse().get(0).getId()));
                        if (LoginModel.getResponse().get(0).getPlumberOnline() != null) {
                            CommonUtils.savePreferencesString(getActivity(), AppConstants.PLUMBER_STATUS, LoginModel.getResponse().get(0).getPlumberOnline());
                        }
                        if (LoginModel.getResponse().get(0).getName() != null) {
                            CommonUtils.savePreferencesString(getActivity(), AppConstants.USER_NAME, LoginModel.getResponse().get(0).getName());
                        }
                        Intent intent = new Intent(getActivity(), UserHomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();

                  /*  Intent i = new Intent(getContext(), UserHomeActivity.class);
                    startActivity(i);
                    getActivity().finish();*/
                    }
                }

                } else {
                    CommonUtils.snackBar(LoginModel.getMsg(), et_email);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable throwable) {
CommonUtils.snackBar("Check Your Internet Connection",et_email);
            }
        });
    }


}
