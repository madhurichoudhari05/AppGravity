package gravity.com.gravity.retail_customer.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.SignInSignUpUserActivity;

/**
 * Created by user on 2/15/2018.
 */

public class TabFragmentSignUp extends Fragment {

    FrameLayout layout;
    Button  btnNext;
    Spinner spinner;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_signup, container, false);

        btnNext=view.findViewById(R.id.fra_tab_sigup_btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getContext(), SignInSignUpUserActivity.class);
                startActivity(i);

            }
        });



        spinner = view.findViewById(R.id.spinner_signup);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(getContext(),R.array.retail_cus,android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setDropDownVerticalOffset(100);


        return view;


    }

}
