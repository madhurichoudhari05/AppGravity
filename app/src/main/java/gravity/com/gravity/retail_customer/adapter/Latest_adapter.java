package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.NewsDetailActivity;
import gravity.com.gravity.retail_customer.model.LatestNewsModel;

/**
 * Created by Admin on 3/6/2018.
 */

public class Latest_adapter extends RecyclerView.Adapter<Latest_adapter.ViewHolder_latest> {
    FragmentTransaction fragmentTransaction;
    ArrayList<LatestNewsModel> arrayList;
    Context context;
    RelativeLayout recyclerView;


    public Latest_adapter(ArrayList<LatestNewsModel> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;


    }

    @Override
    public ViewHolder_latest onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_news_row_item, parent, false);




        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                NewsDetailActivity fragment = new NewsDetailActivity();

                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();


            }
        });


        return new ViewHolder_latest(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder_latest holder, int position) {

        LatestNewsModel latest_pojo = arrayList.get(position);


        holder.pic_latest.setImageResource(latest_pojo.pic_latest);
        if (position == 0) {
            holder.txt1_latest.setTextColor(Color.parseColor("#87CEEB"));
            holder.txt1_latest.setText(latest_pojo.getTxt1_latest());
            holder.linear_latest.setBackgroundColor(Color.parseColor("#87CEEB"));
        } else if (position % 2 == 1) {

            holder.txt1_latest.setTextColor(Color.parseColor("#FFA500"));
            holder.txt1_latest.setText(latest_pojo.getTxt1_latest());
            holder.linear_latest.setBackgroundColor(Color.parseColor("#FFA500"));

        } else if (position % 2 == 0) {
            holder.txt1_latest.setTextColor(Color.parseColor("#FFB6C1"));
            holder.txt1_latest.setText(latest_pojo.getTxt1_latest());
            holder.linear_latest.setBackgroundColor(Color.parseColor("#FFB6C1"));

        }

        holder.txt_time_latest.setText(latest_pojo.getTxt_time_latest());
        holder.txt2_latest.setText(latest_pojo.getTxt2_latest());
        holder.txt3_latest.setText(latest_pojo.getTxt3_latest());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder_latest extends RecyclerView.ViewHolder {
        ImageView pic_latest;
        TextView txt1_latest, txt_time_latest, txt2_latest, txt3_latest;
        LinearLayout linear_latest;

        public ViewHolder_latest(View itemView) {

            super(itemView);

            pic_latest = itemView.findViewById(R.id.pic_latest);

            txt1_latest = itemView.findViewById(R.id.txt1_latest);

            txt_time_latest = itemView.findViewById(R.id.txt_time_latest);

            txt2_latest = itemView.findViewById(R.id.txt2_latest);

            txt3_latest = itemView.findViewById(R.id.txt3_latest);

            linear_latest = itemView.findViewById(R.id.linear_latest);
        }
    }
}
