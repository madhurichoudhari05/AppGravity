package gravity.com.gravity.retail_customer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.NotificationAdapter;
import gravity.com.gravity.retail_customer.model.NotificationModel;

/**
 * Created by Admin on 3/6/2018.
 */

public class FragmentNotifications extends Fragment {
    NotificationAdapter notificationAdapter;
    NotificationModel notificationModel;
    ArrayList<NotificationModel> arrayList;
    Context context;
    RecyclerView recyclerView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        arrayList=new ArrayList<>();

        for(int i=0;i<10;i++){

            notificationModel=new NotificationModel();
            notificationModel.setNotification_icon(R.drawable.ic_notifications_black_24dp);
            notificationModel.setMessage("At vero et accusemus et esuo odio dignissioms qui blanditts prasentium VOLUMMTATUM DELENIT");
            notificationModel.setTitle("Your cart is waiting !");
            notificationModel.setTime("5 hour ago");
            notificationModel.setPic(R.drawable.homec);
            arrayList.add(notificationModel);

        }
        recyclerView=view.findViewById(R.id.a_notification_recycler);
        notificationAdapter=new NotificationAdapter(arrayList,context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(notificationAdapter);





        return view;
    }
}