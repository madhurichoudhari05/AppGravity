package gravity.com.gravity.retail_customer.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import gravity.com.gravity.plumber.model.ComplaintModel;

public class PlumberUserDetailsResponse implements Serializable {

    @SerializedName("complaint")
    @Expose
    private ComplianResponse complaint;
    @SerializedName("plumber")
    @Expose
    private PlumberResponse plumber;


    @SerializedName("customer")
    @Expose
    private UserResponse customer;

    public ComplianResponse getComplaint() {
        return complaint;
    }

    public void setComplaint(ComplianResponse complaint) {
        this.complaint = complaint;
    }

    public PlumberResponse getPlumber() {
        return plumber;
    }

    public void setPlumber(PlumberResponse plumber) {
        this.plumber = plumber;
    }

    public UserResponse getCustomer() {
        return customer;
    }

    public void setCustomer(UserResponse customer) {
        this.customer = customer;
    }


}
