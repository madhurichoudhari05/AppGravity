package gravity.com.gravity.retail_customer.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

import gravity.com.gravity.R;
import gravity.com.gravity.firebase.FcmMessengerViewModel;
import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;
import gravity.com.gravity.firebase.retofit.ApiInterface;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;
import gravity.com.gravity.firebase.retofit.model.TokenResponse;
import gravity.com.gravity.firebase.retofit.model.fcmRes.handler.ChstRetrofitHandler;
import gravity.com.gravity.retail_customer.fragment.FragmentDealerList;
import gravity.com.gravity.retail_customer.fragment.FragmentHome;
import gravity.com.gravity.retail_customer.fragment.FragmentMyAccount;
import gravity.com.gravity.retail_customer.fragment.FragmentMyOrder;
import gravity.com.gravity.retail_customer.fragment.FragmentNotifications;
import gravity.com.gravity.retail_customer.fragment.FragmentWishList;
import gravity.com.gravity.retail_customer.fragment.RegisterComplaint;
import gravity.com.gravity.retail_customer.fragment.ShippingLocation;
import gravity.com.gravity.retail_customer.fragment.TabFragUserSignUp;
import gravity.com.gravity.plumber.activity.ProfileActivity;
import gravity.com.gravity.plumber.fragment.FragMyServiceRequest;
import gravity.com.gravity.plumber.fragment.FragPayment;
import gravity.com.gravity.plumber.fragment.FragScanQRCode;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import gravity.com.gravity.utils.FunNoParam;
import gravity.com.gravity.utils.GpsLocationReceiver;
import gravity.com.gravity.utils.gps.GPSTracker2;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class UserHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    FragmentTransaction fragmentTransaction;
    RegisterComplaint registerComplantfragment;
    FragmentManager fragmentManager;
    boolean isLastFragmentSupportType=true;
    boolean lastFragmentShowed=false;
    ImageView hamburgerMenu;
    Context context;
    TextView title, leftTitle, rightTitle;
    ImageView account;
    NavigationView navigationView;
    FrameLayout right_pannel,frame_gps_on_off;
    ImageView addToCard;
    private APIInterface apiInterface;
    DrawerLayout drawer;
    CircleImageView profile;
    TextView profile_name;
    CircleImageView edit;
    Fragment fragment=null;
    Switch swLocation_on_off;
    GpsLocationReceiver receiver;
    GPSTracker2 tracker;
    Location loc;
    TextView tv_count_cart,tv_userName;
    private String USER_ID="", msg="";


    protected static final String TAG = "location-updates-sample";

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 60000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */
    protected Location mCurrentLocation;
    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;
    private FragmentHome homefragment;
    private TextView serviceRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        context = UserHomeActivity.this;

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

      //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView = findViewById(R.id.nav_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        setSupportActionBar(toolbar);
        USER_ID=CommonUtils.getPreferencesString(context,AppConstants.PLUMBER_USER_ID);
        title = findViewById(R.id.title);
        rightTitle = findViewById(R.id.right_title);
        account = findViewById(R.id.account);
        leftTitle = findViewById(R.id.left_title);
        addToCard = findViewById(R.id.addToCard);
        right_pannel = findViewById(R.id.right_pannel);
        profile_name = findViewById(R.id.profile_name);
        edit = findViewById(R.id.edit);
        frame_gps_on_off=findViewById(R.id.frame_gps_on_off);
        swLocation_on_off=findViewById(R.id.switch_button);
        tracker = new GPSTracker2(context);
        tv_count_cart=findViewById(R.id.tv_count_cart);


        addToCard.setOnClickListener(this);
                    /*For   Location*/












        // do something, the isChecked will be
        // true if the switch is in the On position

        //Toast.makeText(context, "switch ", Toast.LENGTH_SHORT).show();


        String shippingFragment = getIntent().getStringExtra(IConstants.IApp.PARAM_3);






            if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")) {
                CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_TUTORIAL,true);

                tv_count_cart.setVisibility(GONE);
                leftTitle.setTextSize(16);
                swLocation_on_off.setVisibility(View.VISIBLE);


                mRequestingLocationUpdates = false;
                mLastUpdateTime = "";

                if (!checkGpsEnable()) {
                    return;
                }

                updateValuesFromBundle(savedInstanceState);
                // Kick off the process of building a GoogleApiClient and requesting the LocationServices
                // API.
                buildGoogleApiClient();

                swLocation_on_off.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        loc = tracker.getLocation();

                        double lat = tracker.getLatitude();
                        double longitute = tracker.getLongitude();
                        if (isChecked) {
                            callOnlineApi();
                            startUpdatesButtonHandler1(swLocation_on_off);
                        } else {
                            callOfflineApi();
                            if (mRequestingLocationUpdates) {
                                mRequestingLocationUpdates = false;
                                setButtonsEnabledState();
                                stopLocationUpdates();
                            }

                        }

                    }
                });

                if (CommonUtils.getPreferencesBoolean(context, AppConstants.GUESR_USER) != false) {
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    tv_userName = navigationView.getHeaderView(0).findViewById(R.id.user_name);




/*
                    TextView serviceRequest=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                            findItem(R.id.nav_my_service_req));
                    serviceRequest.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
                    serviceRequest.setPadding(15,15,15,15);
                    serviceRequest.setBackground(getResources().getDrawable(R.drawable.solid_circle));
                    serviceRequest.setTextColor(getResources().getColor(R.color.white));
                    serviceRequest.setText("New");*/





                    tv_userName.setText(CommonUtils.NameCaps(CommonUtils.getPreferencesString(context, AppConstants.USER_NAME)));

                    rightTitle.setVisibility(View.VISIBLE);
                    leftTitle.setVisibility(View.VISIBLE);
                    title.setVisibility(GONE);
                    leftTitle.setText("MY SERVICE REQUEST");
                    rightTitle.setVisibility(GONE);
                    addToCard.setVisibility(GONE);
                    right_pannel.setVisibility(GONE);
                    frame_gps_on_off.setVisibility(View.VISIBLE);
                    // addToCard.setImageResource(R.mipmap.service);
                    navigationView.getMenu().clear();


                    navigationView.inflateMenu(R.menu.activity_plumber_drawer);

                 //   Toast.makeText(context, "2222222", Toast.LENGTH_SHORT).show();
//


/*

                    TextView serviceRequest=(TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                            findItem(R.id.nav_my_service_req));
                    serviceRequest.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
                    serviceRequest.setPadding(15,15,15,15);
                    serviceRequest.setBackground(getResources().getDrawable(R.drawable.solid_circle));
                    serviceRequest.setTextColor(getResources().getColor(R.color.white));
                    serviceRequest.setText("New");*/





                /* gallery.setGravity(Gravity.CENTER_VERTICAL);
    gallery.setTypeface(null, Typeface.BOLD);
    gallery.setTextColor(getResources().getColor(R.color.colorAccent));
    gallery.setText("99+");
    slideshow.setGravity(Gravity.CENTER_VERTICAL);
    slideshow.setTypeface(null,Typeface.BOLD);
    slideshow.setTextColor(getResources().getColor(R.color.colorAccent));
*/




                    fragmentManager = getSupportFragmentManager();
                    FragMyServiceRequest fragment = new FragMyServiceRequest();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.commit();


                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    profile_name.setText("Plumber");
                    profile_name.setTextSize(16);

                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    edit = navigationView.getHeaderView(0).findViewById(R.id.edit);
                    edit.setVisibility(GONE);
                    profile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(context, ProfileActivity.class));
                        }
                    });


                } else {
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    tv_count_cart.setVisibility(View.VISIBLE);
                    profile.setImageResource(R.mipmap.nav_guest);
                    profile_name.setText("Guest");
                    profile_name.setTextSize(16);
                    navigationView.getMenu().clear();
                    swLocation_on_off.setVisibility(View.VISIBLE);
                    navigationView.inflateMenu(R.menu.activity_plumber_drawer);
                    rightTitle.setVisibility(View.VISIBLE);
                    leftTitle.setVisibility(GONE);
                    title.setVisibility(View.VISIBLE);
                    leftTitle.setText("MY SERVICE REQUEST");
                    rightTitle.setVisibility(GONE);
                    tv_count_cart.setVisibility(View.VISIBLE);
                    addToCard.setImageResource(R.mipmap.service);
                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.user_nav_list);

/*Madhuri*/
                    fragmentManager = getSupportFragmentManager();
                    homefragment = new FragmentHome();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, homefragment);
                    fragmentTransaction.commit();


                    edit = navigationView.getHeaderView(0).findViewById(R.id.edit);
                    edit.setVisibility(GONE);
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);

                    profile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(context, ProfileActivity.class));
                        }
                    });

                }

            }
            else if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("0")) {
                CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_TUTORIAL,true);
                if (CommonUtils.getPreferencesBoolean(context, AppConstants.GUESR_USER) != false) {

                    mRequestingLocationUpdates = false;
                    mLastUpdateTime = "";
                    if (!checkGpsEnable()) {
                        return;
                    }

                    updateValuesFromBundle(savedInstanceState);
                    // Kick off the process of building a GoogleApiClient and requesting the LocationServices
                    // API.
                    buildGoogleApiClient();

                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.dealer_nav_menu_list);
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    tv_userName = navigationView.getHeaderView(0).findViewById(R.id.user_name);

                    tv_userName.setText(CommonUtils.NameCaps(CommonUtils.getPreferencesString(context, AppConstants.USER_NAME)));


                    title.setVisibility(View.VISIBLE);
                    rightTitle.setVisibility(GONE);
                    tv_count_cart.setVisibility(View.VISIBLE);
                    leftTitle.setVisibility(GONE);
                    tv_count_cart.setVisibility(View.VISIBLE);
                    fragmentManager = getSupportFragmentManager();
                    FragmentHome fragment = new FragmentHome();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.commit();
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(context, ActivityCustomerProfile.class));
                            drawer.closeDrawers();
                        }
                    });


                } else {
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    profile.setImageResource(R.mipmap.nav_guest);
                    profile_name.setText("Guest");
                    tv_count_cart.setVisibility(View.VISIBLE);
                    profile_name.setTextSize(16);
                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.user_nav_list);
                    title.setVisibility(View.VISIBLE);
                    rightTitle.setVisibility(GONE);
                    leftTitle.setVisibility(GONE);

                        fragmentManager = getSupportFragmentManager();
                        fragment = new FragmentHome();
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, fragment);
                        fragmentTransaction.commit();
                }
                // updateNavigationItems();

            }

            else if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("Shipnig")) {


             //   Toast.makeText(context, "shipping", Toast.LENGTH_SHORT).show();
                if (CommonUtils.getPreferencesBoolean(context, AppConstants.GUESR_USER) != false) {

                    mRequestingLocationUpdates = false;
                    mLastUpdateTime = "";
                    if (!checkGpsEnable()) {
                        return;
                    }

                   // updateValuesFromBundle(savedInstanceState);
                    // Kick off the process of building a GoogleApiClient and requesting the LocationServices
                    // API.
                //    buildGoogleApiClient();


                    navigationView.getMenu().clear();
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    tv_userName = navigationView.getHeaderView(0).findViewById(R.id.user_name);

                    tv_userName.setText(CommonUtils.NameCaps(CommonUtils.getPreferencesString(context, AppConstants.USER_NAME)));

                    navigationView.inflateMenu(R.menu.dealer_nav_menu_list);
                    title.setVisibility(View.VISIBLE);
                    rightTitle.setVisibility(GONE);
                    tv_count_cart.setVisibility(View.VISIBLE);
                    leftTitle.setVisibility(GONE);
                    tv_count_cart.setVisibility(View.VISIBLE);

                    if (shippingFragment != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString(IConstants.IApp.PARAM_2, getIntent().getStringExtra(IConstants.IApp.PARAM_2));
                        ShippingLocation shippingLocation = new ShippingLocation();
                        shippingLocation.setArguments(bundle);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.add(R.id.container, shippingLocation);
                        fragmentTransaction.commit();
                    }


                   /* fragmentManager = getSupportFragmentManager();
                    FragmentHome fragment = new FragmentHome();
                    fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, fragment);
                    fragmentTransaction.commit();*/



                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(context, ActivityCustomerProfile.class));
                            drawer.closeDrawers();
                        }
                    });


                } else {
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    profile.setImageResource(R.mipmap.nav_guest);
                    profile_name.setText("Guest");
                    tv_count_cart.setVisibility(View.VISIBLE);
                    profile_name.setTextSize(16);
                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.user_nav_list);
                    title.setVisibility(View.VISIBLE);
                    rightTitle.setVisibility(GONE);
                    leftTitle.setVisibility(GONE);


                }
                // updateNavigationItems();

            }
            else if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("1")) {



                profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                tv_userName = navigationView.getHeaderView(0).findViewById(R.id.user_name);

                CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_TUTORIAL,true);
                profile_name.setText("");
                fragmentManager = getSupportFragmentManager();
                FragmentHome fragment = new FragmentHome();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
                if (CommonUtils.getPreferencesBoolean(context, AppConstants.GUESR_USER) != false) {
                    if (CommonUtils.getPreferencesString(context, AppConstants.dealer_login).equalsIgnoreCase("1")) {

                        navigationView.getMenu().clear();
                        navigationView.inflateMenu(R.menu.retail_customer_list);
                        rightTitle.setVisibility(GONE);
                        leftTitle.setVisibility(GONE);
                        title.setVisibility(View.VISIBLE);
                        profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                        profile_name.setText("Dealer");
                        profile_name.setTextSize(16);
                    } else if (CommonUtils.getPreferencesString(context, AppConstants.distributor_login).equalsIgnoreCase("1")) {
                        navigationView.getMenu().clear();
                        navigationView.inflateMenu(R.menu.distributor_list);
                        rightTitle.setVisibility(GONE);
                        leftTitle.setVisibility(GONE);
                        title.setVisibility(View.VISIBLE);
                        profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                        profile_name.setText("Distributor");
                        profile_name.setTextSize(16);
                    }
                } else {
                    profile = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
                    profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_name);
                    profile.setImageResource(R.mipmap.nav_guest);


                    profile_name.setText("Guest");
                    profile_name.setTextSize(16);
                    navigationView.getMenu().clear();
                    navigationView.inflateMenu(R.menu.user_nav_list);
                    rightTitle.setVisibility(GONE);
                    leftTitle.setVisibility(GONE);
                    title.setVisibility(View.VISIBLE);

                }
            }
    /*    account.setImageResource(R.mipmap.firstname);

        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title.setVisibility(View.GONE);
                leftTitle.setVisibility(View.VISIBLE);
                leftTitle.setText("My Account");
                account.setImageResource(R.mipmap.search);

                fragmentManager = getSupportFragmentManager();
                FragmentMyAccount fragment = new FragmentMyAccount();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }
        });

        account.setImageResource(R.mipmap.firstname);*/
            hamburgerMenu = findViewById(R.id.hamburger_menu);
            hamburgerMenu.setOnClickListener(this);

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();


            navigationView.setNavigationItemSelectedListener(this);
        }



    @Override
    public void onBackPressed() {
        //FragmentManager manager = getSupportFragmentManager();
       int i=getSupportFragmentManager().getBackStackEntryCount();
     //   Toast.makeText(context, "Stck count"+i, Toast.LENGTH_SHORT).show();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
       // Fragment fragment = null;

        if (id == R.id.nav_home) {
            title.setVisibility(View.VISIBLE);
            title.setText("Gravity");
            leftTitle.setVisibility(GONE);



            fragmentManager = getSupportFragmentManager();
            FragmentHome fragment = new FragmentHome();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
            // Handle the camera action
        }
        else if(id==R.id.nav_home_dealer){
            title.setVisibility(View.VISIBLE);
            title.setText("Gravity");
            leftTitle.setVisibility(GONE);
            fragmentManager = getSupportFragmentManager();
            FragmentHome fragment = new FragmentHome();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        }
        else if(id==R.id.nav_home_user){
            title.setVisibility(View.VISIBLE);
            title.setText("Gravity");
            leftTitle.setVisibility(GONE);
            fragmentManager = getSupportFragmentManager();
            FragmentHome fragment = new FragmentHome();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        }



        else if (id == R.id.nav_product) {
            //startActivity(new Intent(UserHomeActivity.this, Faucets.class));

            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRODUCTS");
            fragmentManager = getSupportFragmentManager();
            Faucets fragment = new Faucets();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_product_cate) {
            if (item.getTitle() == "PRODUCTS CATAGORY") {
//                startActivity(new Intent(UserHomeActivity.this, ProductCategoryActivity.class));

                title.setVisibility(GONE);
                leftTitle.setVisibility(View.VISIBLE);
                leftTitle.setText("PRODUCT CATAGORY");
                fragmentManager = getSupportFragmentManager();
                ProductCategoryActivity fragment = new ProductCategoryActivity();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();

            } else if (id == R.id.nav_gravity_videos) {
                startActivity(new Intent(UserHomeActivity.this, VideoCatagory.class));
            }

        }
        else if (id == R.id.gravity_vedio__user) {
            startActivity(new Intent(UserHomeActivity.this, VideoCatagory.class));
        }

        else if (id == R.id.nav_locate_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("Dealer List");
            account.setVisibility(GONE);


            if (item.getTitle() == "LOCATE DEALER") {
                fragmentManager = getSupportFragmentManager();
                FragmentDealerList fragment = new FragmentDealerList();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            } else {
                title.setVisibility(GONE);
                leftTitle.setVisibility(View.VISIBLE);
                leftTitle.setText("PRODUCTS");
                fragmentManager = getSupportFragmentManager();
                Faucets fragment = new Faucets();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }

        }
        else if(id==R.id.product_user) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRODUCTS");
            fragmentManager = getSupportFragmentManager();
            Faucets fragment = new Faucets();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.nav_l_news) {
            if (item.getTitle() == "LATEST NEWS") {
                // startActivity(new Intent(UserHomeActivity.this, LatestNewsActivity.class));
                title.setVisibility(GONE);
                leftTitle.setVisibility(View.VISIBLE);
                leftTitle.setText("LATEST NEWS");
                fragmentManager = getSupportFragmentManager();
                LatestNewsActivity fragment = new LatestNewsActivity();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();

            } else {
                fragmentManager = getSupportFragmentManager();
                TabFragUserSignUp fragment = new TabFragUserSignUp();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();

            }
        }
        /*else if(id==R.id.signup_user){
            leftTitle.setText("GRAVITY SIGNUP");
            title.setVisibility(GONE);
            fragmentManager = getSupportFragmentManager();
            TabFragUserSignUp fragment = new TabFragUserSignUp();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        }*/

        else if (id == R.id.nav_home_dealer) {
          /*  title.setVisibility(View.GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRODUCT CATAGORY");*/
            title.setText("GRAVITY");
            title.setVisibility(View.VISIBLE);
            fragmentManager = getSupportFragmentManager();
            FragmentHome fragment = new FragmentHome();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_product_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRODUCT ");
            fragmentManager = getSupportFragmentManager();
            Faucets fragment = new Faucets();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_product_cate_dealer) {

            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRODUCT CATAGORY");
            fragmentManager = getSupportFragmentManager();
            ProductCategoryActivity fragment = new ProductCategoryActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_locate_dealer1) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("DEALER ");
            fragmentManager = getSupportFragmentManager();
            FragmentDealerList fragment = new FragmentDealerList();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_l_news_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("LATEST NEWS ");
            fragmentManager = getSupportFragmentManager();
            LatestNewsActivity fragment = new LatestNewsActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_notification_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("NOTIFICATION ");
            fragmentManager = getSupportFragmentManager();
            FragmentNotifications fragment = new FragmentNotifications();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_wishList_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("WISHLIST ");
            fragmentManager = getSupportFragmentManager();
            FragmentWishList fragment = new FragmentWishList();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_myorder_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("MY ORDERS ");
            fragmentManager = getSupportFragmentManager();
            FragmentMyOrder fragment = new FragmentMyOrder();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);

            fragmentTransaction.commit();
        }
        else if(id==R.id.registercomplaint){

            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("REGISTER COMPLAINTS ");

            //android.app.FragmentManager fragmentManager = context.get();
            registerComplantfragment = new RegisterComplaint();
            fragmentTransaction=getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, registerComplantfragment);
            fragmentTransaction.commit();
        }

        else if (id == R.id.nav_account_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("MY ACCOUNT ");
            fragmentManager = getSupportFragmentManager();
            FragmentMyAccount fragment = new FragmentMyAccount();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_privace_policy_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRIVACY POLICY");
            fragmentManager = getSupportFragmentManager();
            TermConditionActivity fragment = new TermConditionActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_t_condi_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("TERMS & CONDITION");
            fragmentManager = getSupportFragmentManager();
            TermConditionActivity fragment = new TermConditionActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_contact_us_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("CONTACT US");
            fragmentManager = getSupportFragmentManager();
            ContactUsActivity fragment = new ContactUsActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_feedback_dealer) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("FEEDBACK");
            fragmentManager = getSupportFragmentManager();
            FeedbackActivity fragment = new FeedbackActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_logout_dealer) {

            CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, false);
            startActivity(new Intent(context, HomeLoginActivity.class));
            finish();
        } else if (id == R.id.nav_notification) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("Notications (13)");
            account.setVisibility(GONE);

            if (item.getTitle() == "NOTIFICATION") {

                fragmentManager = getSupportFragmentManager();
                FragmentNotifications fragment = new FragmentNotifications();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();

            } else {
                leftTitle.setVisibility(GONE);
                title.setVisibility(View.VISIBLE);

                startActivity(new Intent(context, SignInSignUpUserActivity.class));

            }

        }

        //current testing

        else if(id==R.id.gravity_login_user) {

            CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL,false);

            leftTitle.setVisibility(GONE);
            title.setVisibility(View.VISIBLE);
            startActivity(new Intent(context, HomeLoginActivity.class));
            finish();


        }

        else if (id == R.id.nav_wishList) {
            title.setVisibility(GONE);
            leftTitle.setText("WishList");
            leftTitle.setVisibility(View.VISIBLE);
            account.setVisibility(GONE);

            if (item.getTitle() == "WISHLIST") {
                fragmentManager = getSupportFragmentManager();
                FragmentWishList fragment = new FragmentWishList();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            } else {
                leftTitle.setText("Dealer");

                fragmentManager = getSupportFragmentManager();
                FragmentDealerList fragment = new FragmentDealerList();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            }

        }
        else if(id==R.id.locate_dealer_user){
            leftTitle.setText("Dealer");

            fragmentManager = getSupportFragmentManager();
            FragmentDealerList fragment = new FragmentDealerList();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        }


        else if (id == R.id.nav_myorder) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("My Order");
            account.setImageResource(R.mipmap.search);
            account.setVisibility(View.VISIBLE);
            account.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, SearchBar.class);
                    startActivity(intent);
                }
            });
            fragmentManager = getSupportFragmentManager();
            FragmentMyOrder fragment = new FragmentMyOrder();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();


        } else if (id == R.id.nav_account) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("My Account");
            account.setImageResource(R.mipmap.search);


            if (item.getTitle() == "MY ACCOUNT") {
                account.setVisibility(View.VISIBLE);
                leftTitle.setText("MY ACCOUNT");
                leftTitle.setVisibility(View.VISIBLE);
                fragmentManager = getSupportFragmentManager();
                FragmentMyAccount fragment = new FragmentMyAccount();
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.commit();
            } else {

                startActivity(new Intent(context, LatestNewsActivity.class));
                title.setVisibility(View.VISIBLE);
                leftTitle.setVisibility(GONE);
                account.setVisibility(GONE);
            }

        }

        else if(id==R.id.latest_news_user) {
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("LATEST NEWS ");
            fragmentManager = getSupportFragmentManager();
            LatestNewsActivity fragment = new LatestNewsActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();
        }
        else if(id==R.id.contact_us_user){
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("CONTACT US");
            fragmentManager = getSupportFragmentManager();
            ContactUsActivity fragment = new ContactUsActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        }
        else if (id == R.id.nav_privace_policy) {

        } else if (id == R.id.nav_t_condi) {
            // startActivity(new Intent(UserHomeActivity.this, TermConditionActivity.class));
            leftTitle.setVisibility(View.VISIBLE);
            rightTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("TERM AND CONDITIONS");
            rightTitle.setVisibility(GONE);
            right_pannel.setVisibility(View.VISIBLE);
            addToCard.setVisibility(GONE);
            addToCard.setImageResource(R.mipmap.service);
            title.setVisibility(GONE);
            fragmentManager = getSupportFragmentManager();
            TermConditionActivity fragment = new TermConditionActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();


        } else if (id == R.id.nav_contact_us) {
            //   startActivity(new Intent(context, ContactUsActivity.class));
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("CONTACT US");
            fragmentManager = getSupportFragmentManager();
            ContactUsActivity fragment = new ContactUsActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_feedback) {
            //   startActivity(new Intent(UserHomeActivity.this, FeedbackActivity.class));
            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("FEEDBACK AND RATINGS");
            fragmentManager = getSupportFragmentManager();
            FeedbackActivity fragment = new FeedbackActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();


        }
       /* else if(id==R.id.nav_logout_user){

            CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, false);
            startActivity(new Intent(context, HomeLoginActivity.class));
            finish();

        }
       */ else if (id == R.id.nav_logout) {

            Toast.makeText(context, "You are logout sucessfully ", Toast.LENGTH_SHORT).show();

            callOfflineApi();
            CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, false);
            startActivity(new Intent(context, HomeLoginActivity.class));
            finish();
            Toast.makeText(context, "You are logout sucessfully ", Toast.LENGTH_SHORT).show();

        }
        else if (id == R.id.nav_my_service_req) {
            CommonUtils.savePreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE, "0");










            leftTitle.setVisibility(View.VISIBLE);
            rightTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("ALL SERVICE REQUEST");
            rightTitle.setVisibility(GONE);
            right_pannel.setVisibility(View.VISIBLE);
            addToCard.setVisibility(GONE);
           // addToCard.setImageResource(R.mipmap.service);
            title.setVisibility(GONE);
            fragmentManager = getSupportFragmentManager();
            FragMyServiceRequest fragment = new FragMyServiceRequest();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_completed_request) {
            leftTitle.setVisibility(View.VISIBLE);
            rightTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("MY COMPLETED REQUEST");
            rightTitle.setVisibility(GONE);
            title.setVisibility(GONE);
            addToCard.setVisibility(View.GONE);
            right_pannel.setVisibility(View.VISIBLE);
            CommonUtils.savePreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE, "1");
            fragmentManager = getSupportFragmentManager();
            FragMyServiceRequest fragment = new FragMyServiceRequest();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_complaints_assigned) {

            leftTitle.setVisibility(View.VISIBLE);
            rightTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("MY COMPLAINTS REQUEST");
            rightTitle.setVisibility(GONE);
            title.setVisibility(GONE);
            addToCard.setVisibility(View.GONE);
            right_pannel.setVisibility(View.VISIBLE);
            CommonUtils.savePreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE, "2");

            fragmentManager = getSupportFragmentManager();
            FragMyServiceRequest fragment = new FragMyServiceRequest();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_add_payment_detail) {
            fragmentManager = getSupportFragmentManager();
            title.setVisibility(View.VISIBLE);
            leftTitle.setVisibility(View.VISIBLE);
            right_pannel.setVisibility(GONE);
            addToCard.setVisibility(GONE);
            rightTitle.setVisibility(GONE);
            leftTitle.setText("ADD PAYMENT DETAILS");
            title.setVisibility(GONE);
            FragPayment fragment = new FragPayment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();


        } else if (id == R.id.nav_product_cate) {

            title.setVisibility(GONE);
            leftTitle.setVisibility(View.VISIBLE);
            leftTitle.setText("PRODUCT CATAGORY");
            fragmentManager = getSupportFragmentManager();
            ProductCategoryActivity fragment = new ProductCategoryActivity();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_scan_qr_code) {
            leftTitle.setVisibility(View.VISIBLE);
            title.setVisibility(GONE);
            leftTitle.setText("SCAN QR-CODE");
            rightTitle.setVisibility(GONE);
            addToCard.setVisibility(GONE);
            right_pannel.setVisibility(GONE);

            fragmentManager = getSupportFragmentManager();
            FragScanQRCode fragment = new FragScanQRCode();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_points_earned) {
            CommonUtils.savePreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE, "3");
            leftTitle.setVisibility(View.VISIBLE);
            rightTitle.setVisibility(GONE);
            leftTitle.setText("MY POINTS REQUEST");
            rightTitle.setText("My Service Request");
            title.setVisibility(GONE);
            addToCard.setVisibility(View.GONE);
            right_pannel.setVisibility(View.VISIBLE);

            fragmentManager = getSupportFragmentManager();
            FragMyServiceRequest fragment = new FragMyServiceRequest();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_logout) {
           // callOfflineApi();
            CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL, false);
            startActivity(new Intent(context, HomeLoginActivity.class));
            finish();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.hamburger_menu:
                drawer.openDrawer(Gravity.START);

                break;
            case R.id.addToCard:
                startActivity(new Intent(UserHomeActivity.this, MyCartActivity.class));
                break;


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")&&(CommonUtils.getPreferencesString(context,AppConstants.ALL_USER).equalsIgnoreCase("0"))) {
            checkPermission();
            if (receiver != null) {
                checkGpsEnable();
            }

            
        }



       // showLandingFragment(vv);



    }

    private void callOnlineApi() {



        Log.e("User_id",USER_ID);


        Call<ResponseBody> call = apiInterface.getOnLine(USER_ID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {

                        String res = response.body().string();

                        Log.e("location","locationApi"+res);
                        JSONObject jsonObject = new JSONObject(res);
                        String msg=jsonObject.getString("msg");
                        int status=jsonObject.getInt("status");
                        if (status==1)
                        {

                            CommonUtils.snackBar(msg, swLocation_on_off);
                        }
                        else {
                            CommonUtils.snackBar(msg, swLocation_on_off);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error Occured", swLocation_on_off);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                CommonUtils.snackBar("Check Your Credentials", swLocation_on_off);
            }
        });
    }


    private void callOfflineApi() {



        Log.e("User_id",USER_ID);


        Call<ResponseBody> call = apiInterface.getOffLine(USER_ID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {

                        String res = response.body().string();
                        if(res!=null){
                        Log.e("location","locationApi"+res);
                        JSONObject jsonObject = new JSONObject(res);

                        if(jsonObject.getString("msg")!=null){
                         msg=jsonObject.getString("msg");}
                        int status=jsonObject.getInt("status");
                        if (status==1)
                        {

                            CommonUtils.snackBar(msg, swLocation_on_off);
                        }
                        else {
                            CommonUtils.snackBar(msg, swLocation_on_off);
                        }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error Occured", swLocation_on_off);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                CommonUtils.snackBar("Check Your Credentials", swLocation_on_off);
            }
        });
    }

    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(context, per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    protected void checkPermission() {

        if (isPerGiven(Manifest.permission.ACCESS_FINE_LOCATION)
                && isPerGiven(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            return;
        }

        String[] arr = new String[2];
        arr[0] = Manifest.permission.ACCESS_FINE_LOCATION;
        arr[1] = Manifest.permission.ACCESS_COARSE_LOCATION;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(arr, 1);
        }
    }


    private boolean checkGpsEnable() {
        if (!tracker.isGPSEnabled()) {
            setReciever();
            tracker.showSettingsAlert();
            return false;
        } else {
            tracker.dismissAlert();
            loc=tracker.getLocation();

            Timer t = new Timer();
            t.scheduleAtFixedRate(
                    new TimerTask() {
                        @Override
                        public void run() {

                                if(loc==null){
                                    loc=tracker.getLocation();
                                }else {
                                    HashMap<String, String> map = tracker.saveLocation(loc);

                                    // city = map.get("1");
                                  //  state = map.get("2");


                                    UserHomeActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //updateAdapters();



                                        }
                                    });
                                }

                            t.cancel();

                        }

                    },
                    0,
                    1000);


            return true;
        }
    }

    private void setReciever() {
        if (receiver != null) {
            return;
        }
        receiver = new GpsLocationReceiver();
        receiver.setmListener(new FunNoParam() {
            @Override
            public void done() {

            }
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
        context.registerReceiver(receiver, filter);
    }




    /*for Location*/


    private void updateValuesFromBundle(Bundle savedInstanceState) {
       Log.e(TAG, "Updating values from bundle");
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(REQUESTING_LOCATION_UPDATES_KEY);
                setButtonsEnabledState();
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
            }
           updateUI();
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
       Log.e(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }
    private void setButtonsEnabledState() {
        if (mRequestingLocationUpdates) {
          swLocation_on_off.setChecked(true);
        } else {
            swLocation_on_off.setChecked(false);
        }
    }


    private void updateUI() {
//      Log.e("location:::Latitude",String.valueOf(mCurrentLocation.getLatitude()));
    //  Log.e("location:::LatitudeL",latitude);
       // Toast.makeText(context, "change"+String.valueOf(mCurrentLocation.getLatitude()), Toast.LENGTH_SHORT).show();


     /*   if(String.valueOf(mCurrentLocation.getLatitude())!=null&&String.valueOf(mCurrentLocation.getLongitude())!=null) {
            callUpdateAPI(String.valueOf(mCurrentLocation.getLatitude()),String.valueOf(mCurrentLocation.getLongitude()));
        }*/

    }


    protected void startLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    @Override
    public void onConnected(Bundle connectionHint) {
       Log.e(TAG, "Connected to GoogleApiClient");

        // If the initial location was never previously requested, we use
        // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
        // its value in the Bundle and check for it in onCreate(). We
        // do not request it again unless the user specifically requests location updates by pressing
        // the Start Updates button.
        //
        // Because we cache the value of the initial location in the Bundle, it means that if the
        // user launches the activity,
        // moves to a new location, and then changes the device orientation, the original location
        // is displayed as the activity is re-created.
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
          //
             updateUI();
        }

        // If the user presses the Start Updates button before GoogleApiClient connects, we set
        // mRequestingLocationUpdates to true (see startUpdatesButtonHandler()). Here, we check
        // the value of mRequestingLocationUpdates and if it is true, we start location updates.
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateUI();
       Log.e(TAG, "lOCATIONcHANGE"+String.valueOf(location.getLatitude())+":"+String.valueOf(location.getLongitude()));
      //  Toast.makeText(this, getResources().getString(R.string.location_updated_message), Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
       Log.e(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
       Log.e(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }
    /**
     * Stores activity data in the Bundle.
     */
  /*  public void onSaveInstanceState(Bundle savedInstanceState) {


        boolean isMain;
        if(null != savedInstanceState) {
            savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
            savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
            savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
            super.onSaveInstanceState(savedInstanceState);
        }
    }*/
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Handles the Start Updates button and requests start of location updates. Does nothing if
     * updates have already been requested.
     */
    public void startUpdatesButtonHandler1(View view) {
            if (null != mRequestingLocationUpdates) {
                mRequestingLocationUpdates = true;
                setButtonsEnabledState();
                startLocationUpdates();
            }
        }

    /**
     * Handles the Stop Updates button, and requests removal of location updates. Does nothing if
     * updates were not previously requested.
     */
    public void stopUpdatesButtonHandler(View view) {
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            setButtonsEnabledState();
            stopLocationUpdates();
        }
    }
    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void callUpdateAPI(String lat, String longi) {


        Log.e("callApi",
                lat+"::"+longi);
        Log.e("User_id",USER_ID);


        Call<ResponseBody> call = apiInterface.updateLocation(USER_ID,lat,longi , "1");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {

                        String res = response.body().string();

                        Log.e("location","locationApi"+res);
                        JSONObject jsonObject = new JSONObject(res);
                        String msg=jsonObject.getString("msg");
                        int status=jsonObject.getInt("status");
                        if (status==1)
                        {
                            CommonUtils.snackBar(msg+": "+lat+"   "+longi, swLocation_on_off);
                        }
                        else {
                            CommonUtils.snackBar(msg+": "+lat+"   "+longi, swLocation_on_off);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error Occured", swLocation_on_off);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                CommonUtils.snackBar("Check Your Credentials", swLocation_on_off);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")) {
            if (mGoogleApiClient == null) {
                buildGoogleApiClient();
            } else {

                mGoogleApiClient.connect();
                mRequestingLocationUpdates = true;
            }
        }


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")) {
            // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
            if (swLocation_on_off.isChecked()) {
                if (mGoogleApiClient.isConnected()) {
                    stopLocationUpdates();
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")) {
            mGoogleApiClient.disconnect();

        }
        else {
            mGoogleApiClient.disconnect();

        }
    }



    /**
     * Requests location updates from the FusedLocationApi.
     */

    /**
     * Ensures that only one button is enabled at any time. The Start Updates button is enabled
     * if the user is not requesting location updates. The Stop Updates button is enabled if the
     * user is requesting location updates.
     */



}
