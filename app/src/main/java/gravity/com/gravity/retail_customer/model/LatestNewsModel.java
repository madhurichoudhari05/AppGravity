package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 3/6/2018.
 */

public class LatestNewsModel {
   public int pic_latest;
    String txt1_latest;

    public void setTxt1_latest(String txt1_latest) {
        this.txt1_latest = txt1_latest;
    }

    public String getTxt_time_latest() {
        return txt_time_latest;
    }

    public void setTxt_time_latest(String txt_time_latest) {
        this.txt_time_latest = txt_time_latest;
    }

    public String getTxt2_latest() {
        return txt2_latest;
    }

    public void setTxt2_latest(String txt2_latest) {
        this.txt2_latest = txt2_latest;
    }

    public String getTxt3_latest() {
        return txt3_latest;
    }

    public void setTxt3_latest(String txt3_latest) {
        this.txt3_latest = txt3_latest;
    }

    String txt_time_latest;
    String txt2_latest;
    String txt3_latest;
    String linear_latest;


    LatestNewsModel() {
    }


    public  LatestNewsModel(int pic_latest, String txt1_latest, String txt_time_latest, String txt2_latest, String txt3_latest, String linear_latest) {

        this.linear_latest = linear_latest;
        this.pic_latest = pic_latest;
        this.txt1_latest = txt1_latest;
        this.txt_time_latest = txt_time_latest;
        this.txt3_latest = txt3_latest;
        this.txt2_latest = txt2_latest;
    }

    public String getLinear_latest() {
        return linear_latest;
    }

    public void setLinear_latest(String linear_latest) {
        this.linear_latest = linear_latest;
    }

    public int getPic_latest() {
        return pic_latest;
    }

    public void setPic_latest(int pic_latest) {
        this.pic_latest = pic_latest;
    }

    public String getTxt1_latest() {
        return txt1_latest;
    }

}