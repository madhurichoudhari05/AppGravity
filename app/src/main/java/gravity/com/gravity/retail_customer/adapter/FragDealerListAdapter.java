package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.DealerListModel;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by user on 2/16/2018.
 */

public class FragDealerListAdapter extends RecyclerView.Adapter<FragDealerListAdapter.ViewHolder> {

    DealerListModel dealerListModel;
    ArrayList<DealerListModel> arrayList = new ArrayList<>();

    Context context;

    public FragDealerListAdapter(ArrayList<DealerListModel> arrayList, Context context) {


        this.arrayList = arrayList;

        this.context = context;

    }

    @Override
    public FragDealerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;


            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.frag_dealer_list_row_item, parent, false);


        FragDealerListAdapter.ViewHolder recyclerViewHolder = new FragDealerListAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(FragDealerListAdapter.ViewHolder holder, int position) {

        if(position%6==0){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#81b20c"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#81b20c"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#81b20c"));
        }else if(position%6==1){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FFA500"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#FFA500"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#FFA500"));
        }else if(position%6==2){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#0000FF"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#0000FF"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#0000FF"));
        }else if(position%6==3){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#BDB76B"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#BDB76B"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#BDB76B"));
        }else if(position%6==4){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#00FFFF"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#00FFFF"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#00FFFF"));
        }else if(position%6==5){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#8FBC8F"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#8FBC8F"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#8FBC8F"));
        }else if(position%6==6){
            holder.relativeLayout.setBackgroundColor(Color.parseColor("#FF0000"));
            setTextViewDrawableColor(holder.contactNo,Color.parseColor("#FF0000"));
            setTextViewDrawableColor(holder.distance,Color.parseColor("#FF0000"));
        }


        holder.profile_pic.setImageResource(arrayList.get(position).getProfile_pic());


      //  holder.pro_image2.setImageResource(arrayList.get(position).getImgID());
        holder.dealerName.setText(arrayList.get(position).getDealer_name());
        holder.contactNo.setText(arrayList.get(position).getDealer_contact());
        holder.distance.setText(arrayList.get(position).getDealer_distaqnce());
       // holder.pro_name2.setText(arrayList.get(position).getProductName());


    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  profile_pic;
        RelativeLayout relativeLayout;
        TextView dealerName,contactNo,distance;
        Button btn_send_req;

        public ViewHolder(View itemView) {
            super(itemView);

            profile_pic = (ImageView) itemView.findViewById(R.id.profile_pic);
            dealerName=itemView.findViewById(R.id.name);
            contactNo=itemView.findViewById(R.id.mobileNo);
            distance=itemView.findViewById(R.id.distance);
            relativeLayout=itemView.findViewById(R.id.rl_lauout);
            btn_send_req=itemView.findViewById(R.id.btn_send_req);



        }
    }
    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter((color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
