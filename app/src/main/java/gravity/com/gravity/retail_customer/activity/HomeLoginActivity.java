package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.firebase.iid.FirebaseInstanceId;

import gravity.com.gravity.R;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by user on 2/19/2018.
 */

public class HomeLoginActivity extends AppCompatActivity implements View.OnClickListener{
    TextView retailcustomer,gPartner,plumber;
    Button btnLogin;
    Context context;
    LinearLayout skip;
    ImageView play;
    private int position = 0;


    // Request code for user select video file.
    private static final int REQUEST_CODE_SELECT_VIDEO_FILE = 1;

    // Request code for require android READ_EXTERNAL_PERMISSION.
    private static final int REQUEST_CODE_READ_EXTERNAL_PERMISSION = 2;

    // Used when update video progress thread send message to progress bar handler.
    private static final int UPDATE_VIDEO_PROGRESS_BAR = 3;


    private static final String TAG_RUNTIME_PERMISSION = "TAG_RUNTIME_PERMISSION";

    private static final int PERMISSION_REQUEST_CODE = 1;

    MediaController mediaController;
    private String fireBaseKey="";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_login);

        retailcustomer=findViewById(R.id.a_h_login_txt_reta_customer);
        gPartner=findViewById(R.id.a_h_login_txt_g_partner);
        plumber=findViewById(R.id.a_h_login_txt_plumber);
        btnLogin=findViewById(R.id.a_h_login_btnLogin);
        skip=findViewById(R.id.skip);
        context=HomeLoginActivity.this;
        play=findViewById(R.id.play);

        btnLogin.setOnClickListener(this);
        plumber.setOnClickListener(this);
        retailcustomer.setOnClickListener(this);
        gPartner.setOnClickListener(this);
        skip.setOnClickListener(this);


          getToken();

        CommonUtils.savePreferencesString(context,AppConstants.ALL_USER,"0");

        final VideoView videoView = (VideoView)findViewById(R.id.VideoView);



        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaController=new MediaController(context);

                mediaController.setAnchorView(videoView);

                DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
                android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoView.getLayoutParams();
                params.width =  metrics.widthPixels;
                params.height = metrics.heightPixels;
                params.leftMargin = 0;
                videoView.setLayoutParams(params);

                //Uri uri= Uri.parse("rtsp://r2---sn-a5m7zu76.c.youtube.com/Ck0LENy73wIaRAnTmlo5oUgpQhMYESARFEgGUg5yZWNvbW1lbmRhdGlvbnIhAWL2kyn64K6aQtkZVJdTxRoO88HsQjpE1a8d1GxQnGDmDA==/0/0/0/video.3gp");
                videoView.setBackgroundResource(0);
                Uri uri= Uri.parse("https://youtu.be/3iRlCNlxw2o");
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                videoView.start();


                //videoView.setVideoPath("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
               // videoView.setVideoPath("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
               // videoView.start();
                play.setVisibility(View.GONE);

            }
        });




        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {

            public void onPrepared(MediaPlayer mediaPlayer)
            {
                // if we have a position on savedInstanceState, the video
                // playback should start from here
                videoView.seekTo(position);

                System.out.println("vidio is ready for playing");

                if (position == 0)
                {
                    videoView.start();
                } else
                {
                    // if we come from a resumed activity, video playback will
                    // be paused
                    videoView.pause();
                }
            }
        });

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play.setVisibility(View.VISIBLE);
                videoView.stopPlayback();


            }
        });




    }







    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.a_h_login_txt_reta_customer:

                CommonUtils.savePreferencesString(context,AppConstants.ALL_USER,"0");
                retailcustomer.setTextColor(Color.WHITE);
                retailcustomer.setBackgroundColor(getResources().getColor(R.color.light_black));
                plumber.setTextColor(Color.BLACK);
                gPartner.setTextColor(Color.BLACK);
               plumber.setBackgroundColor(Color.WHITE);
                gPartner.setBackgroundColor(Color.WHITE);



                break;
            case R.id.a_h_login_txt_g_partner:
                CommonUtils.savePreferencesString(context,AppConstants.ALL_USER,"1");

                gPartner.setTextColor(Color.WHITE);
                gPartner.setBackgroundColor(getResources().getColor(R.color.light_black));

                plumber.setTextColor(Color.BLACK);
                retailcustomer.setTextColor(Color.BLACK);

                plumber.setBackgroundColor(Color.WHITE);
                retailcustomer.setBackgroundColor(Color.WHITE);

                break;

            case R.id.a_h_login_txt_plumber:
                CommonUtils.savePreferencesString(context,AppConstants.ALL_USER,"2");
                plumber.setTextColor(Color.WHITE);
                plumber.setBackgroundColor(getResources().getColor(R.color.light_black));

                retailcustomer.setTextColor(Color.BLACK);
                gPartner.setTextColor(Color.BLACK);

                gPartner.setBackgroundColor(Color.WHITE);
                retailcustomer.setBackgroundColor(Color.WHITE);

                break;

            case R.id.a_h_login_btnLogin:

                if(!CommonUtils.getPreferencesBoolean(context,AppConstants.FIRST_TIME_TUTORIAL)) {


                    if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("0")) {
                        CommonUtils.savePreferencesBoolean(context, AppConstants.GUESR_USER, true);
                        Intent i = new Intent(context, SignInSignUpUserActivity.class);
                        startActivity(i);
                       // finish();
                    } else if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("1")) {
                        CommonUtils.savePreferencesBoolean(context, AppConstants.GUESR_USER, true);
                        startActivity(new Intent(context, GravityPartnerLogin.class));
                        //finish();
                    } else if (CommonUtils.getPreferencesString(context, AppConstants.ALL_USER).equalsIgnoreCase("2")) {
                        CommonUtils.savePreferencesBoolean(context, AppConstants.GUESR_USER, true);
                        startActivity(new Intent(context, PlumberSignInSignUp.class));
                        //finish();
                    }
                }

                else {

                    startActivity(new Intent(context, UserHomeActivity.class));
                    finish();


                    }
                break;

            case R.id.skip:

                CommonUtils.savePreferencesBoolean(context, AppConstants.GUESR_USER,false);
                CommonUtils.savePreferencesBoolean(context, AppConstants.FIRST_TIME_TUTORIAL,false);
                startActivity(new Intent(HomeLoginActivity.this,UserHomeActivity.class));

              //  CommonUtils.savePreferencesBoolean(context,AppConstants.GUESR_USER,true);
                break;



        }


    }


    private void getToken() {


        fireBaseKey = CommonUtils.getPreferencesString(this, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);

            return;
        } else {
            fireBaseKey = CommonUtils.getPreferencesString(this, AppConstants.FIREBASE_KEY);
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    /* Run this method after user choose grant read external storage permission or not. */



}
