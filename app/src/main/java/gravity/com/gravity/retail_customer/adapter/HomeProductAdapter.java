package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.ProductModel;

/**
 * Created by user on 2/16/2018.
 */

public class HomeProductAdapter extends RecyclerView.Adapter<HomeProductAdapter.ViewHolder> {

    ProductModel productModel;
    ArrayList<ProductModel> arrayList = new ArrayList<>();

    Context context;

    public HomeProductAdapter(ArrayList<ProductModel> arrayList, Context context) {


        this.arrayList = arrayList;

        this.context = context;

    }

    @Override
    public HomeProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_product_row_item, parent, false);

        HomeProductAdapter.ViewHolder recyclerViewHolder = new HomeProductAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(HomeProductAdapter.ViewHolder holder, int position) {

        holder.user_image.setImageResource(arrayList.get(position).getImgID());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  user_image;

        public ViewHolder(View itemView) {
            super(itemView);

            user_image = (ImageView) itemView.findViewById(R.id.image);

        }
    }
}
