package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.CustomerReviewModel;

/**
 * Created by user on 2/16/2018.
 */

public class CustomerReviewAdapter extends RecyclerView.Adapter<CustomerReviewAdapter.ViewHolder> {
    CustomerReviewModel notiModel;

    ArrayList<CustomerReviewModel> arrayList = new ArrayList<>();
    CustomerReviewAdapter adapter;
    CustomerReviewModel model;
    Context context;

    public CustomerReviewAdapter(ArrayList<CustomerReviewModel> arrayList, Context context) {


        this.arrayList = arrayList;

        this.context = context;

    }

    @Override
    public CustomerReviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_review_row_item, parent, false);

        CustomerReviewAdapter.ViewHolder recyclerViewHolder = new CustomerReviewAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(CustomerReviewAdapter.ViewHolder holder, int position) {

        model=arrayList.get(position);

        holder.profile_image.setImageResource(model.getProfile_image());
        holder.txt1_review.setText(model.getTxt1_review());
        holder.txt2_review.setText(model.getTxt2_review());
        holder.txt3_review.setText(model.getTxt3_review());
        holder.txt4_review.setText(model.getTxt4_review());
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profile_image;
        RatingBar ratingBar_review;
        TextView txt1_review, txt2_review, txt3_review, txt4_review;

        public ViewHolder(View itemView) {
            super(itemView);


            profile_image = itemView.findViewById(R.id.profile_image);
            ratingBar_review = itemView.findViewById(R.id.ratingBar_review);

            txt1_review = itemView.findViewById(R.id.txt1_review);
            txt2_review = itemView.findViewById(R.id.txt2_review);
            txt3_review = itemView.findViewById(R.id.txt3_review);
            txt4_review = itemView.findViewById(R.id.txt4_review);


        }
    }
}
