package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 3/12/2018.
 */

public class ReceivedOrderModel {

    int product_pic;
    String orderID;
    String productSpecification;

    public int getProduct_pic() {
        return product_pic;
    }

    public void setProduct_pic(int product_pic) {
        this.product_pic = product_pic;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getProductSpecification() {
        return productSpecification;
    }

    public void setProductSpecification(String productSpecification) {
        this.productSpecification = productSpecification;
    }

    public String getDiliveryDate() {
        return diliveryDate;
    }

    public void setDiliveryDate(String diliveryDate) {
        this.diliveryDate = diliveryDate;
    }

    public String getReceieptOrder() {
        return receieptOrder;
    }

    public void setReceieptOrder(String receieptOrder) {
        this.receieptOrder = receieptOrder;
    }

    String diliveryDate;
    String receieptOrder;

}
