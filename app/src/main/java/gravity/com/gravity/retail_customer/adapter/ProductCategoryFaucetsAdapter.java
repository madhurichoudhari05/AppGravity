package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.NewProductModel;

/**
 * Created by user on 2/16/2018.
 */

public class ProductCategoryFaucetsAdapter extends RecyclerView.Adapter<ProductCategoryFaucetsAdapter.ViewHolder> {

    NewProductModel productModel;
    ArrayList<NewProductModel> arrayList = new ArrayList<>();

    Context context;

    public ProductCategoryFaucetsAdapter(ArrayList<NewProductModel> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;

    }

    @Override
    public ProductCategoryFaucetsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.a_pro_cate_faucets_row_item, parent, false);

        ProductCategoryFaucetsAdapter.ViewHolder recyclerViewHolder = new ProductCategoryFaucetsAdapter.ViewHolder(view);

        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(ProductCategoryFaucetsAdapter.ViewHolder holder, int position) {

        holder.pro_image.setImageResource(arrayList.get(position).getImgID());
      //  holder.pro_image2.setImageResource(arrayList.get(position).getImgID());
        holder.pro_name1.setText(arrayList.get(position).getProductName());
       // holder.pro_name2.setText(arrayList.get(position).getProductName());

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView  pro_image, pro_image2;
        TextView pro_name1,pro_name2;

        public ViewHolder(View itemView) {
            super(itemView);

            pro_image = (ImageView) itemView.findViewById(R.id.image);
           // pro_image2=itemView.findViewById(R.id.image2);
            pro_name1=itemView.findViewById(R.id.product_name);
            //pro_name2=itemView.findViewById(R.id.product_name2);
        }
    }
}
