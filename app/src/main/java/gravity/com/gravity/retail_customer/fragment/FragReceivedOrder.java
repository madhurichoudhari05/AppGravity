package gravity.com.gravity.retail_customer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.FragRecievedAdapter;
import gravity.com.gravity.retail_customer.model.ReceivedOrderModel;

/**
 * Created by Admin on 3/12/2018.
 */

public class FragReceivedOrder extends Fragment {

RecyclerView recyclerView;
ReceivedOrderModel orderModel;
ArrayList<ReceivedOrderModel> arrayList;
    FragRecievedAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_recieved_order, container, false);

        recyclerView=view.findViewById(R.id.recyclerView);

        arrayList=new ArrayList<>();

        for(int i=0;i<10;i++) {
            orderModel = new ReceivedOrderModel();
            orderModel.setProduct_pic(R.mipmap.c);
            orderModel.setOrderID("order state_id: GA113454");
            orderModel.setProductSpecification("Neque porro quisqum est ,qut do");
            orderModel.setDiliveryDate("12 jan-2018");
            orderModel.setReceieptOrder("Repeat Order");
            arrayList.add(orderModel);

        }
        adapter=new FragRecievedAdapter(arrayList,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        return view;
    }

}
