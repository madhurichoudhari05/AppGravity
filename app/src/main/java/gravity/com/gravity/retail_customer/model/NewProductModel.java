package gravity.com.gravity.retail_customer.model;

/**
 * Created by user on 2/16/2018.
 */

public class NewProductModel {


    private String productName, product_price, product_discount, product_rating_star;
    private int imgID = 0;

    public String getProduct_discount() {
        return product_discount;
    }

    public void setProduct_discount(String product_discount) {
        this.product_discount = product_discount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getImgID() {
        return imgID;
    }

    public void setImgID(int imgID) {
        this.imgID = imgID;
    }


    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_rating_star() {
        return product_rating_star;
    }

    public void setProduct_rating_star(String product_rating_star) {
        this.product_rating_star = product_rating_star;
    }
}
