package gravity.com.gravity.retail_customer.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import gravity.com.gravity.R;

/**
 * Created by Admin on 3/12/2018.
 */

public class FragmentMyAccount extends Fragment {
    CardView cardNotification, changepass;
    ImageView imgNotification;
    TextView tvNotiSetting, tvChangePass;
    LinearLayout llNotification;
    CardView cardview_wishlist;
    FragmentManager manager;
    FragmentTransaction transaction;
NavigationView navigationView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        tvNotiSetting = view.findViewById(R.id.tv_noti_setting);
        cardNotification = view.findViewById(R.id.card_noti_setting);
        llNotification = view.findViewById(R.id.ll_noti_more);
        changepass = view.findViewById(R.id.card_chang_pass);
        tvChangePass = view.findViewById(R.id.tv_change_pass);
        cardview_wishlist = view.findViewById(R.id.cardview_wishlist);
        cardview_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                cardview_wishlist.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {




                Fragment fragment= new FragmentWishList();
                Fragment fragment1 = new FragmentWishList();
                manager = getFragmentManager();
                transaction=manager.beginTransaction();
                transaction.replace(R.id.container,fragment);

                getFragmentManager().popBackStackImmediate();

                transaction.commit();}
                },100);

            }
        });

        cardNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changepass.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvNotiSetting.setTextColor(Color.parseColor("#FFFFFF"));
                tvChangePass.setTextColor(Color.parseColor("#000000"));

                if (llNotification.getVisibility() == View.GONE) {
                    cardNotification.setBackgroundColor(Color.parseColor("#AC233B"));
                    llNotification.setVisibility(View.VISIBLE);
                } else {
                    llNotification.setVisibility(View.GONE);
                }
            }
        });

        changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changepass.setBackgroundColor(Color.parseColor("#AC233B"));
                cardNotification.setBackgroundColor(Color.parseColor("#FFFFFF"));
                tvChangePass.setTextColor(Color.parseColor("#FFFFFF"));
                tvNotiSetting.setTextColor(Color.parseColor("#000000"));


                changePassDialog();
              //  changepass.setBackgroundColor(Color.parseColor("#FFFFFF"));



            }
        });

        return view;
    }


    private void changePassDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getActivity().getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setLayout(width, height);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialoge_change_pass);
        dialog.show();


    }

}
