package gravity.com.gravity.retail_customer.model;

/**
 * Created by user on 2/19/2018.
 */

public class MycartModel {
    int productPic, quantity;
    String producrName,getProducrRate, appliedOffer,availableOffer,deliveryDate,rupee_mycart;


    public int getProductPic() {
        return productPic;
    }

    public void setProductPic(int productPic) {
        this.productPic = productPic;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getProducrName() {
        return producrName;
    }
    public  void setrupee_mycart(String rupee_mycart){
        this.rupee_mycart=rupee_mycart;
    }

    public String getrupee_mycart(){
        return  rupee_mycart;
    }

    public void setProducrName(String producrName) {
        this.producrName = producrName;
    }

    public String getGetProducrRate() {
        return getProducrRate;
    }

    public void setGetProducrRate(String getProducrRate) {
        this.getProducrRate = getProducrRate;
    }

    public String getAppliedOffer() {
        return appliedOffer;
    }

    public void setAppliedOffer(String appliedOffer) {
        this.appliedOffer = appliedOffer;
    }

    public String getAvailableOffer() {
        return availableOffer;
    }

    public void setAvailableOffer(String availableOffer) {
        this.availableOffer = availableOffer;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }


}
