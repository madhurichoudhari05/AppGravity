package gravity.com.gravity.retail_customer.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.model.CatagoryModel;

public class CatagoryAdapter  extends RecyclerView.Adapter<CatagoryAdapter.InnerClass> {

    ArrayList<CatagoryModel> arrayList;
    Context context;

    public CatagoryAdapter(ArrayList<CatagoryModel> arrayList,Context context){

        this.arrayList = arrayList;
        this.context = context;

    }


    @Override
    public InnerClass onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catagory_single,parent,false);



        return new InnerClass(view);
    }

    @Override
    public void onBindViewHolder(InnerClass holder, int position) {

        CatagoryModel pojo =arrayList.get(position);

        holder.pic_catagory1.setImageResource(pojo.getPic_catagory1());
        holder.pic2_catagory1.setImageResource(pojo.getPic2_catagory1());
        holder.txt1_catagory1.setText(pojo.getTxt1_catagory1());
        holder.txt2_catagory1.setText(pojo.getTxt2_catagory1());
        if(position==0){
            holder.linear_catagory.setBackgroundColor(Color.parseColor("#DC143C"));
        }
        else{
            holder.linear_catagory.setBackgroundColor(Color.parseColor("#DC143C"));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class InnerClass extends RecyclerView.ViewHolder {

        ImageView pic_catagory1,pic2_catagory1;
        TextView txt1_catagory1,txt2_catagory1;
        LinearLayout linear_catagory;

        public InnerClass(View itemView) {
            super(itemView);

            pic_catagory1=itemView.findViewById(R.id.pic_catagory1);
            pic2_catagory1=itemView.findViewById(R.id.pic2_catagory1);
            txt1_catagory1=itemView.findViewById(R.id.txt1_catagory1);
            txt2_catagory1=itemView.findViewById(R.id.txt2_catagory1);
            linear_catagory=itemView.findViewById(R.id.linear_catagory12);



        }
    }
}


