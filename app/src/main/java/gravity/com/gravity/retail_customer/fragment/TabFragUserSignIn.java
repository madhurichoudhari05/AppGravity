package gravity.com.gravity.retail_customer.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.ActivityForgotPassword;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 2/15/2018.
 */

public class TabFragUserSignIn extends Fragment {
     Button btnSignIn;
    TextView forgot_pass;
    ImageView p_eye;
    private APIInterface apiInterface;
    EditText et_email, et_password;
    String user_type = "1";
    boolean isChecked = true;
    String user_id="", name="";
    ImageView iv_show_pass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_signin, container, false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        forgot_pass = view.findViewById(R.id.f_pass);
        et_email = view.findViewById(R.id.et_email);
        et_password = view.findViewById(R.id.et_password);
        iv_show_pass=view.findViewById(R.id.iv_show_pass);
        et_email.setHint("Email ID/Mobile No");

        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtils.savePreferencesString(getActivity(),AppConstants.ALL_USER,"0");
                startActivity(new Intent(getActivity(), ActivityForgotPassword.class));
            }
        });

        iv_show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isChecked) {
                    et_password.setTransformationMethod(new PasswordTransformationMethod());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye1);
                    isChecked = true;
                } else {
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    et_password.getSelectionEnd();
                    et_password.setSelection(et_password.length());
                    iv_show_pass.setImageResource(R.drawable.ic_eye);
                    isChecked = false;
                }
            }
        });

        btnSignIn = view.findViewById(R.id.fra_tab_user_sign_btnsinin);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_email.getText().toString().equals("") && et_password.getText().toString().equals("")) {
                    CommonUtils.snackBar("Enter Your Fields", et_email);

                } else {
                    callsignInAPI();
                }
            }
        });

        return view;

    }

    private void callsignInAPI() {

        Call<ResponseBody> call = apiInterface.signInUser(et_email.getText().toString(), et_password.getText().toString(), FirebaseInstanceId.getInstance().getToken(), user_type);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {

                        String res = response.body().string();
                        if(res!=null){
                        JSONObject jsonObject = new JSONObject(res);
                        String msg=jsonObject.getString("msg");
                        int status=jsonObject.getInt("status");
                        if(msg!=null) {
                            if (status == 1) {
                                JSONArray array = jsonObject.getJSONArray("response");
                                if (array != null){
                                    for (int i = 0; i < array.length(); i++) {

                                        JSONObject jsonObject1 = array.getJSONObject(i);

                                        if (jsonObject1 != null){
                                            if(jsonObject1.getString("id")!=null){
                                             user_id = jsonObject1.getString("id");}
                                            if(jsonObject1!=null){
                                                String name = jsonObject1.getString("name");
                                            }



                                        CommonUtils.savePreferencesString(getActivity(), AppConstants.USER_NAME, name);
                                        CommonUtils.savePreferencesString(getActivity(), AppConstants.USER_ID, user_id);

                                        CommonUtils.savePreferencesBoolean(getActivity(), AppConstants.GUESR_USER, true);
                                        CommonUtils.savePreferencesString(getActivity(), AppConstants.USER_NAME, jsonObject1.getString("name"));
                                        CommonUtils.snackBar(msg, et_email);
                                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                                        CommonUtils.savePreferencesString(getActivity(), AppConstants.ALL_USER, "0");

                                        Intent intent = new Intent(getActivity(), UserHomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        //  intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


                                        startActivity(intent);
                                        getActivity().finish();
                             /*   startActivity(new Intent(getActivity(), UserHomeActivity.class));
                                getActivity().finish();*/
                                    }

                                    }
                            }

                            } else {
                                CommonUtils.snackBar(msg, et_email);
                            }

                        }
                        }else {
                            CommonUtils.snackBar("Null Response",et_email);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error Occured", et_email);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                CommonUtils.snackBar("Check Your Internet Connection", et_email);
            }
        });
    }

}
