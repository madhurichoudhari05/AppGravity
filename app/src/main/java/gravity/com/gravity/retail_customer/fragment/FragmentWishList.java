package gravity.com.gravity.retail_customer.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.WishListAdapter;
import gravity.com.gravity.retail_customer.model.WishListModel;

/**
 * Created by Admin on 3/6/2018.
 */

public class FragmentWishList extends Fragment {
    RecyclerView recyclerItem;
    ArrayList<WishListModel> itemList;
    WishListModel wishListModel;
    WishListAdapter wishListAdapter;
    Context context;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        context = getActivity();
        View view = inflater.inflate(R.layout.fragment_wish_list, container, false);

        recyclerItem=view.findViewById(R.id.a_wislst_recy_item);
        itemList=new ArrayList();

        wishListModel=new WishListModel();
        wishListModel.setPro_img_ID(R.drawable.home_t);
        wishListModel.setRating("3.0");
        wishListModel.setProductName("Lorem inspum");
        wishListModel.setProductPrice("₹ 1999");
        wishListModel.setImg_remove(R.mipmap.delete);
        itemList.add(wishListModel);
        for(int i=0;i<10;i++){
            wishListModel=new WishListModel();
            wishListModel.setPro_img_ID(R.drawable.home_t);
            wishListModel.setRating("5.0");
            wishListModel.setProductName("Lorem inspum");
            wishListModel.setProductPrice("₹ 1999");
            wishListModel.setImg_remove(R.mipmap.delete);
            itemList.add(wishListModel);
        }

        wishListAdapter=new WishListAdapter(itemList,context);

        recyclerItem.setLayoutManager(new LinearLayoutManager(context));
        recyclerItem.setAdapter(wishListAdapter);






        return view;
    }
}