package gravity.com.gravity.retail_customer.model;

/**
 * Created by Admin on 2/27/2018.
 */

public class WishListModel {

    int pro_img_ID,img_remove;
        String productName;

    public int getPro_img_ID() {
        return pro_img_ID;
    }

    public void setPro_img_ID(int pro_img_ID) {
        this.pro_img_ID = pro_img_ID;
    }

    public int getImg_remove() {
        return img_remove;
    }

    public void setImg_remove(int img_remove) {
        this.img_remove = img_remove;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    String productPrice,rating;


}
