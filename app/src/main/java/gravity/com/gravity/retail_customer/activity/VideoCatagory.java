package gravity.com.gravity.retail_customer.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.adapter.VideoAdapter;
import gravity.com.gravity.retail_customer.model.ItemoffsetDecoration;
import gravity.com.gravity.retail_customer.model.VideoModel;

public class VideoCatagory extends AppCompatActivity{
    RecyclerView recyclerView;
    VideoAdapter video_adapter;
    ArrayList<VideoModel> arrayList = new ArrayList();
    Context context;
    int spanCount = 3; // 3 columns
    int spacing = 20; // 50px
    boolean includeEdge = true;
    ImageView back_vedio;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_catagory);
        context = VideoCatagory.this;
        back_vedio = findViewById(R.id.back_vedio);
        back_vedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.recycler_video);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        video_adapter = new VideoAdapter(arrayList, context);
        recyclerView.addItemDecoration(new ItemoffsetDecoration(spanCount,spacing,includeEdge));

        recyclerView.setAdapter(video_adapter);

        for(int i=0;i<7;i++){

            int[] folder_pic = new int[]{R.drawable.folder};

            VideoModel pojo = new VideoModel(folder_pic[0],"Catogory 1");
            arrayList.add(pojo);
        }
    }
}