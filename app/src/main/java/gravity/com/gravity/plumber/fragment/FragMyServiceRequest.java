package gravity.com.gravity.plumber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.adapter.MyServiceRequestTabAdapter;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by Admin on 3/6/2018.
 */

public class FragMyServiceRequest extends Fragment {

    Context context;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_order, container, false);

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("All Requests"));

        tabLayout.addTab(tabLayout.newTab().setText("Completed"));
        tabLayout.addTab(tabLayout.newTab().setText("In Progress"));
        tabLayout.addTab(tabLayout.newTab().setText("Earnings"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        context=getActivity();

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);

        final MyServiceRequestTabAdapter adapter = new MyServiceRequestTabAdapter
        (getFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(0);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        if(CommonUtils.getPreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE).equalsIgnoreCase("0")){
            viewPager.setCurrentItem(0);
        }else  if(CommonUtils.getPreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE).equalsIgnoreCase("1")){
            viewPager.setCurrentItem(1);
        } if(CommonUtils.getPreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE).equalsIgnoreCase("2")){
            viewPager.setCurrentItem(2);
        } if(CommonUtils.getPreferencesString(context, AppConstants.PLUMBER_REQUEST_SERVICE).equalsIgnoreCase("3")){
            viewPager.setCurrentItem(3);
        }


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return view;
    }
}