package gravity.com.gravity.plumber.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import gravity.com.gravity.plumber.fragment.InProgressFrag;
import gravity.com.gravity.plumber.fragment.FragTabCompletedService;
import gravity.com.gravity.plumber.fragment.FragTabEarnedPoint;
import gravity.com.gravity.plumber.fragment.FragTabRequestService;

/**
 * Created by user on 2/15/2018.
 */

public class MyServiceRequestTabAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public MyServiceRequestTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
               // TabFragUserSignIn tab1=new TabFragUserSignIn();

                FragTabRequestService tab1=new FragTabRequestService();
                return tab1;
            case 1:
               // TabFragmentSignUp tab2 = new TabFragmentSignUp();
                FragTabCompletedService tab2=new FragTabCompletedService();
                return tab2;

            case 2:
                InProgressFrag tab3=new InProgressFrag();
                return tab3;

            case 3:
                FragTabEarnedPoint tab4=new FragTabEarnedPoint();
                return tab4;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
