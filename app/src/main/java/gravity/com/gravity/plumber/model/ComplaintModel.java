package gravity.com.gravity.plumber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComplaintModel {





String txt_ca,txt2_ca,txt3_ca,txt4_ca,txt5_ca,txt6_ca,txt7_ca,txt1_ca;
int img_ca;
public  ComplaintModel(){}

public ComplaintModel(String txt_ca, String txt1_ca, String txt2_ca, String txt3_ca, String txt4_ca, String txt5_ca, String txt6_ca, int img_ca){
this.txt_ca = txt_ca;

    this.txt2_ca = txt2_ca;
    this.txt3_ca = txt3_ca;
    this.txt4_ca = txt4_ca;
    this.txt5_ca = txt5_ca;
    this.txt6_ca = txt1_ca;
    this.txt7_ca = txt6_ca;
    this.img_ca = img_ca;
}

    public String getTxt_ca() {
        return txt_ca;
    }

    public void setTxt_ca(String txt_ca) {
        this.txt_ca = txt_ca;
    }

    public String getTxt1_ca() {
        return txt1_ca;
    }

    public void setTxt1_ca(String txt1_ca) {
        this.txt1_ca = txt1_ca;
    }

    public String getTxt2_ca() {
        return txt2_ca;
    }

    public void setTxt2_ca(String txt2_ca) {
        this.txt2_ca = txt2_ca;
    }

    public String getTxt3_ca() {
        return txt3_ca;
    }

    public void setTxt3_ca(String txt3_ca) {
        this.txt3_ca = txt3_ca;
    }

    public String getTxt4_ca() {
        return txt4_ca;
    }

    public void setTxt4_ca(String txt4_ca) {
        this.txt4_ca = txt4_ca;
    }

    public String getTxt5_ca() {
        return txt5_ca;
    }

    public void setTxt5_ca(String txt5_ca) {
        this.txt5_ca = txt5_ca;
    }

    public String getTxt6_ca() {
        return txt6_ca;
    }

    public void setTxt6_ca(String txt6_ca) {
        this.txt6_ca = txt6_ca;
    }

    public String getTxt7_ca() {
        return txt7_ca;
    }

    public void setTxt7_ca(String txt7_ca) {
        this.txt7_ca = txt7_ca;
    }

    public int getImg_ca() {
        return img_ca;
    }

    public void setImg_ca(int img_ca) {
        this.img_ca = img_ca;
    }
}
