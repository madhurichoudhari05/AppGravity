package gravity.com.gravity.plumber.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.model.ComplaintModel;
import gravity.com.gravity.retail_customer.model.ReceivedOrderModel;

public class EarningAdater extends RecyclerView.Adapter<EarningAdater.InnerEarning> {

    Context context;
    ArrayList<String> list = new ArrayList<>();


    public EarningAdater(Context activity, ArrayList<String> arrayList) {
        this.context = activity;
        this.list = arrayList;
    }

    @Override
    public InnerEarning onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.points_earn_single,parent,false);
        return new InnerEarning(view);
    }

    @Override
    public void onBindViewHolder(InnerEarning holder, int position) {

        holder.name.setText(list.get(0));
        holder.ratings.setText(list.get(1));
        holder.points.setText(list.get(2));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class InnerEarning extends RecyclerView.ViewHolder {

        TextView name,ratings,points;

        public InnerEarning(View itemView) {

            super(itemView);

            name = itemView.findViewById(R.id.name);
            ratings = itemView.findViewById(R.id.ratings);
            points = itemView.findViewById(R.id.points);
        }
    }
}
