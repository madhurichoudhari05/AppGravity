package gravity.com.gravity.plumber.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import gravity.com.gravity.DataInterface.StateInterface;
import gravity.com.gravity.R;
import gravity.com.gravity.plumber.model.StateResponsePOJO;
import gravity.com.gravity.retail_customer.fragment.TabFragPlumberSignUp;
import gravity.com.gravity.utils.CommonUtils;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.InnerState> {
    ArrayList<String> list;
    Context context;
    TabFragPlumberSignUp tabFragPlumberSignUp;
    int id=0;
    ArrayList<String> stateList;
    ArrayList<String> filterList;
    private StateInterface stateInterfacel;


    public StateAdapter(ArrayList<String> list, Context context, TabFragPlumberSignUp tabFragPlumberSignUp,int id,StateInterface stateInterface) {
        this.stateList = list;
        this.context = context;
        this.tabFragPlumberSignUp = tabFragPlumberSignUp;
        filterList = new ArrayList<>();
        this.filterList.addAll(stateList);
        this.id=id;
        stateInterfacel= stateInterface;

    }

    @Override
    public InnerState onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.state_single, parent, false);

        return new InnerState(view);
    }

    @Override
    public void onBindViewHolder(final InnerState holder, final int position) {

        if (CommonUtils.getIntPreferences(context, "place") == position) {
                holder.itemView.setBackgroundColor(Color.parseColor("#BA2F44"));
                holder.txt_state.setTextColor(Color.WHITE);
            }

         else {
            holder.itemView.setBackgroundColor(Color.WHITE);
            holder.txt_state.setTextColor(Color.BLACK);
        }
        holder.txt_state.setText(stateList.get(position));




        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   holder.txt_state.setText(view.);
                CommonUtils.savePreferencesBoolean(context, "linear_background", true);
                CommonUtils.saveIntPreferences(context, "place", position);
                id=holder.itemView.getId();

                StateResponsePOJO pojo=new StateResponsePOJO();

                String state_id=pojo.getStateId();


               // stateInterfacel.getId(id);

           //     Log.e("ID",state_id);
                tabFragPlumberSignUp.setState(position, stateList,state_id);

            }
        });
    }


    @Override
    public int getItemCount() {
        return stateList.size();
    }

    public void filter(String charText) {

        charText = charText.toLowerCase(Locale.getDefault());
        stateList.clear();
        if (charText.length() == 0) {
            stateList.addAll(filterList);
        } else {
            for (String wp : filterList) {
                if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    stateList.add(wp);
                }
            }
        }
        notifyDataSetChanged();

    }

    public class InnerState extends RecyclerView.ViewHolder {
        TextView txt_state;
        LinearLayout linear_state;
        public InnerState(View itemView) {
            super(itemView);
            txt_state = itemView.findViewById(R.id.txt_state);
            linear_state = itemView.findViewById(R.id.linear_state);
        }
    }
}
