package gravity.com.gravity.plumber.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;

import gravity.com.gravity.R;
import gravity.com.gravity.firebase.FcmMessengerViewModel;
import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.MyApplication;
import gravity.com.gravity.firebase.db.database.AppDatabase;
import gravity.com.gravity.firebase.db.entities.ChatMessageDto;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;
import gravity.com.gravity.firebase.retofit.ApiInterface;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;
import gravity.com.gravity.firebase.retofit.model.fcmRes.handler.ChstRetrofitHandler;
import gravity.com.gravity.firebase.retofit.model.fcmRes.handler.ExecutorHandler;
import gravity.com.gravity.plumber.model.CustomerModel;
import gravity.com.gravity.plumber.model.TokenModel;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static gravity.com.gravity.utils.CommonUtils.dismissProgress;

public class RequestDetails extends AppCompatActivity {
    TextView btn_progress, start, complete, hold, completed;
    ImageView back,ivProductImage;
    Context context;
    Button btn_accept, btn_decline;
    LinearLayout linear_act_dec;
    FrameLayout frame_start,frame_completed;
    private APIInterface apiInterface;

    protected AppDatabase mAppDb;
    protected ExecutorService mExecutor;
    private  String plumber_user_id="",customer_user_id="",profilepic="",plumberName="",complain_id;

    private TextView tv_plumber_name,tv_plumber_phone,tv_plumber_location,tv_plumber_service,tv_plumber_category,
            tv_plumber_date,tv_plumber_time,tv_plumber_request,tv_plumber_title;



    public static Intent getPendingIntent(Context activity, int type) {
        Intent i = new Intent(activity, HomeActivity.class);
        i.putExtra(IConstants.IApp.PARAM_1, type);
        return i;
    }




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.requestdetails);
        context=RequestDetails.this;
        mAppDb= MyApplication.getDb();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mExecutor= ExecutorHandler.getInstance().getExecutor();
     //   mInterface = ChstRetrofitHandler.getInstance().getApi();



       /* for(UserStatusDto user:userDetail){
            user.setRequestStatus(true);
            user.setApproveStatus(true);
            mExecutor.execute(() -> mAppDb.userDao().insert(user));
        }*/

        setObserver();
        btn_progress = findViewById(R.id.btn_progress);
        btn_accept = findViewById(R.id.btn_accept);
        btn_decline = findViewById(R.id.btn_decline);
        linear_act_dec = findViewById(R.id.linear_act_dec);
        back = findViewById(R.id.back);
        plumber_user_id= CommonUtils.getPreferences(context,AppConstants.PLUMBER_USER_ID);
        profilepic=  CommonUtils.getPreferences(context,AppConstants.PROFILE_PIC);
        plumberName =CommonUtils.getPreferences(context,AppConstants.USER_NAME);

        tv_plumber_name=findViewById(R.id.tv_plumber_name);
        tv_plumber_phone=findViewById(R.id.tv_plumber_phone);
        tv_plumber_location=findViewById(R.id.tv_plumber_location);
        tv_plumber_service=findViewById(R.id.tv_plumber_service);
        tv_plumber_category=findViewById(R.id.tv_plumber_category);
        ivProductImage=findViewById(R.id.ivProductImage);
        tv_plumber_date=findViewById(R.id.tv_plumber_date);
        tv_plumber_time=findViewById(R.id.tv_plumber_time);
        tv_plumber_request=findViewById(R.id.tv_plumber_request);
        tv_plumber_title=findViewById(R.id.tv_plumber_title);

       /* if(getIntent()!=null&&getIntent().getStringExtra(AppConstants.REQUEST_DETAILS)!=null){

            if(getIntent().getStringExtra(AppConstants.REQUEST_DETAILS).equalsIgnoreCase("RequestDetails"))

                btn_progress.setVisibility(View.VISIBLE);
                btn_progress.setText(" in Progress");
               linear_act_dec.setVisibility(View.GONE);


        }
        else {

        }*/




        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_progress.setVisibility(View.VISIBLE);
                linear_act_dec.setVisibility(View.GONE);

                for(UserStatusDto user:userDetail) {
                    approveForChat(user);
                }

               callPlumberAssigmnAPI();

                btn_progress.setText("In Progress");

            }
        });

        btn_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_progress.setVisibility(View.VISIBLE);
                btn_progress.setText("cancelled");
                linear_act_dec.setVisibility(View.GONE);



            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

      /*  if (CommonUtils.getPreferencesString(context, AppConstants.WORK_IN).equalsIgnoreCase("1")) {
            Log.e("ASD", "" + CommonUtils.getPreferencesString(context, AppConstants.WORK_IN).equalsIgnoreCase("1"));
            btn_progress.setText("Pending");
        } else if (CommonUtils.getPreferencesString(context, AppConstants.WORK_IN).equalsIgnoreCase("2")) {
            btn_progress.setText("In Progress");
        } else if (CommonUtils.getPreferencesString(context, AppConstants.WORK_IN).equalsIgnoreCase("3")) {
            btn_progress.setText("Hold");
        } else if (CommonUtils.getPreferencesString(context, AppConstants.WORK_IN).equalsIgnoreCase("4")) {
            //btn_progress.setText("Completed");
        }*/


        btn_progress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(RequestDetails.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Window window = RequestDetails.this.getWindow();
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

                int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
                int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
                //  int height = WindowManager.LayoutParams.WRAP_CONTENT;
                //int width = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setLayout(width, height);

                dialog.setCanceledOnTouchOutside(true);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.status_dialog);

                start = dialog.findViewById(R.id.start);
                complete = dialog.findViewById(R.id.complete);
                frame_start = dialog.findViewById(R.id.frame_start);
                frame_completed= dialog.findViewById(R.id.frame_completed);
                if(CommonUtils.getPreferencesString(context,AppConstants.WORK_IN).equalsIgnoreCase("1")){
                    start.setBackgroundColor(Color.parseColor("#BA2F44"));
                    complete.setBackgroundColor(Color.WHITE);
                    start.setTextColor(Color.WHITE);
                    complete.setTextColor(Color.BLACK);

                }
                else if(CommonUtils.getPreferencesString(context,AppConstants.WORK_IN).equalsIgnoreCase("2")){
                    complete.setBackgroundColor(Color.parseColor("#BA2F44"));
                    start.setBackgroundColor(Color.WHITE);
                    complete.setTextColor(Color.WHITE);
                    start.setTextColor(Color.BLACK);
                }
                dialog.show();


               /* hold = dialog.findViewById(R.id.hold);
                completed = dialog.findViewById(R.id.completed);
*/
                start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        frame_start.setBackgroundColor(Color.parseColor("#BA2F44"));
                        frame_completed.setBackgroundColor(Color.WHITE);
                          CommonUtils.saveStringPreferences(context, AppConstants.WORK_IN, "1");
                        start.setTextColor(Color.WHITE);
                        complete.setTextColor(Color.BLACK);

                        complete.setBackgroundColor(Color.WHITE);
                        btn_progress.setText("Start Job");


                        dialog.dismiss();
                        // btn_progress.setText("Pending");
                        //dialog.dismiss();
                    }
                });

                complete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       CommonUtils.saveStringPreferences(context, AppConstants.WORK_IN, "2");
                        frame_completed.setBackgroundColor(Color.parseColor("#BA2F44"));
                        frame_start.setBackgroundColor(Color.WHITE);
                        complete.setTextColor(Color.WHITE);
                        start.setTextColor(Color.BLACK);

                        btn_progress.setText("Completed");
                        dialog.dismiss();
                        start.setBackgroundColor(Color.WHITE);
                        //btn_progress.setText("In Progress");
                        //dialog.dismiss();
                    }
                });
              /*  hold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CommonUtils.saveStringPreferences(context, AppConstants.WORK_IN, "3");
                        hold.setBackgroundColor(Color.parseColor("#BA2F44"));
                        hold.setTextColor(Color.WHITE);
                        inprogress.setTextColor(Color.BLACK);
                        pending.setTextColor(Color.BLACK);
                        completed.setTextColor(Color.BLACK);
                        completed.setBackgroundColor(Color.WHITE);
                        pending.setBackgroundColor(Color.WHITE);
                        inprogress.setBackgroundColor(Color.WHITE);
                        //btn_progress.setText("Hold");
                        // dialog.dismiss();
                    }
                });

                completed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CommonUtils.saveStringPreferences(context, AppConstants.WORK_IN, "4");
                        completed.setBackgroundColor(Color.parseColor("#BA2F44"));
                        completed.setTextColor(Color.WHITE);
                        inprogress.setTextColor(Color.BLACK);
                        hold.setTextColor(Color.BLACK);
                        pending.setTextColor(Color.BLACK);
                        pending.setBackgroundColor(Color.WHITE);
                        inprogress.setBackgroundColor(Color.WHITE);
                        hold.setBackgroundColor(Color.WHITE);
                        //btn_progress.setText("Completed");
                        //dialog.dismiss();
                    }
                });*/
/*

                Button button=dialog.findViewById(R.id.varify_acco);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

*/


                //   Button button=dialog.findViewById(R.id.varify_acco);

             /*   button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
*/


            }
        });


    }

  /*  private void approveForChat(UserStatusDto user) {
        HashMap<String,String> map=new HashMap<>();
        map.put("msg","approved");
        map.put("key",FirebaseInstanceId.getInstance().getToken());

        FcmMessage msg = new FcmMessage(
                CommonUtils.getPreferences(context, AppConstants.USER_ID),
                CommonUtils.getPreferences(context, AppConstants.USER_NAME),
                user.getToId(),
                System.currentTimeMillis() + "",
                new Gson().toJson(map),
                IConstants.IFcm.FCM_APP_REQUEST,
                user.getToImage(), "56"
        );
       // showProgress("Wait");
        fcmVM.sendFcmMessage(msg, user.getFcmKey(), false);
    }*/

    private void approveForChat(UserStatusDto user) {
        HashMap<String,String> map=new HashMap<>();
        map.put("msg","approved");
       // map.put("key",FirebaseInstanceId.getInstance().getToken());
        map.put("key",CommonUtils.getPreferences(context,AppConstants.FIREBASE_KEY));

        FcmMessage msg = new FcmMessage(
                plumber_user_id+"",
                plumberName+""  ,
                //user.getToId()+"",
                customer_user_id+"",
                System.currentTimeMillis() + "",
                new Gson().toJson(map),
                IConstants.IFcm.FCM_APP_REQUEST,
                "355"+"", complain_id
        );
        // showProgress("Wait");
       // fcmVM.sendFcmMessage(msg, FirebaseInstanceId.getInstance().getToken(), false);
        Log.e("fcmkey",user.getFcmKey());
        fcmVM.sendFcmMessage(msg, user.getFcmKey(), false);
    }

    FcmMessengerViewModel fcmVM;
    private List<UserStatusDto> userDetail=new ArrayList<>();
    private List<ChatMessageDto> complainList=new ArrayList<>();
    private FcmMessengerViewModel messenger;



    private void setObserver() {
        if (messenger == null) {
                fcmVM = ViewModelProviders.of(this).get(FcmMessengerViewModel.class);
                messenger = ViewModelProviders.of(this).get(FcmMessengerViewModel.class);
                messenger.getUserAppList(false, IConstants.IFcm.MY_APPROVAL).observe(this, res->{
                userDetail.clear();
                userDetail.addAll(res);



                    if(userDetail.size()>0) {
                    for (UserStatusDto user:userDetail){

                        if (user.getToId() != null) {
                            customer_user_id=user.getToId();
                            complain_id=user.getToImage();

                           Log.e("userid",user.getToId());
                           Log.e("complain_id",complain_id);
                            callCustomerDetailAPI(complain_id);

                        }

                    }

                }

            });
          /*  fcmVM.getChatDBOnType(IConstants.IFcm.FCM_NEW_REQUEST, customer_user_id).observe(this, chat -> {
                if (chat!=null&&chat.size() > 0) {
                    complain_id = chat.get(0).getPostId();
                    Log.e("requestcomplain_id", complain_id);
                    callCustomerDetailAPI(complain_id);
                    } else {
                    Toast.makeText(context, "fail getting postid", Toast.LENGTH_SHORT).show();
                }
            });*/


        }
    }



    private void callPlumberAssigmnAPI() {

        Call<ResponseBody> call = apiInterface.getAssignPlumber(plumber_user_id,customer_user_id,complain_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                if (response != null) {
                    try {

                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String msg=jsonObject.getString("msg");
                        int status=jsonObject.getInt("status");
                        if (status==1)
                        {
                            CommonUtils.snackBar("Server  save data", tv_plumber_category);


                        } else {
                            CommonUtils.snackBar(msg, tv_plumber_category);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    CommonUtils.snackBar("Error Occured", tv_plumber_category);
                }



            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RequestDetails.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }






    private void dismissProg() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }
    }

    private void showProg() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.show();
            } catch (Exception e) {
            }
        }
    }

    private ProgressDialog progressDialog;

    private void callCustomerDetailAPI(String complain_id) {

        Call<CustomerModel> call = apiInterface.getUserDetail(customer_user_id,"Raise",complain_id);
        call.enqueue(new Callback<CustomerModel>() {
            @Override
            public void onResponse(Call<CustomerModel> call, Response<CustomerModel> responseBody) {

                CustomerModel customerModel=responseBody.body();

                if (responseBody != null) {

                    if (customerModel.getStatus() == 1) {

                        if(customerModel.getResponse()!=null&&customerModel.getResponse().size()>0) {


                            if(customerModel.getResponse().get(0).getDate()!=null) {
                                String date = customerModel.getResponse().get(0).getDate();


                                if(customerModel.getResponse().get(0).getLatitude()!=null&&customerModel.getResponse().get(0).getLongitude()!=null) {
                                    getLocation(customerModel.getResponse().get(0).getLatitude(), customerModel.getResponse().get(0).getLongitude());


                                    String[] parts = date.split(" ");
                                    String currentDate = parts[0]; // 004
                                    String time = parts[1];

                              /*  String[] dateArray = date.split(" 00:");
                                String currentDate = dateArray[0];
                                String time = dateArray[1];*/
                                    //   time=time.replace(" 00:"," ");
                                    //   String time=dateArray[1];

                                    tv_plumber_date.setText(currentDate);
                                    tv_plumber_time.setText(time);


                                } }
                            tv_plumber_name.setText(customerModel.getResponse().get(0).getFirstName());
                            tv_plumber_phone.setText(customerModel.getResponse().get(0).getPhone());
                            tv_plumber_service.setText(customerModel.getResponse().get(0).getComplaintNo());

                              tv_plumber_category.setText(customerModel.getResponse().get(0).getCategory());

                            // tv_plumber_title.setText(customerModel.getResponse().get(0).getJobStatus());
                            tv_plumber_request.setText(customerModel.getResponse().get(0).getComplaintDescription());



                           // Picasso.with(context).load(customerModel.getResponse().get(0).getCategory()).into(ivProductImage);


                           /* Picasso.with(context)
                                    .load(customerModel.getResponse().get(0)
                                            .getProfilePic())
                                    .placeholder(R.drawable.home_t)
                                    .error(R.drawable.home_t)
                                    .into(ivProductImage);*/

                           // Toast.makeText(context, "UserName"+customerModel.getResponse().get(0).getComplaintNo(), Toast.LENGTH_SHORT).show();






                        } } else {
                        Toast.makeText(context, "Fail Please try again 3", Toast.LENGTH_SHORT).show();
                    }






                } else {
                    Toast.makeText(RequestDetails.this, "Error Occured", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CustomerModel> call, Throwable t) {
                Toast.makeText(RequestDetails.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getLocation(String latitudec, String longitudec) {



        if (latitudec!=null&& !latitudec.equals("")) {
            double latitude = Double.valueOf(latitudec);
            double longitude =Double.valueOf(longitudec);
            try {



                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses.size() > 0) {
                    String cityName = addresses.get(0).getLocality();
                    String stateName = addresses.get(0).getAdminArea();
                    String s = ""
                            + addresses.get(0).getAddressLine(0) + "\n";
                           /* + addresses.get(0).getFeatureName() + "\n"
                            + "Locality: "
                            + addresses.get(0).getLocality() + "\n"
                            + addresses.get(0).getPremises() + "\n"
                            + "Admin Area: ";*/
                           /* + addresses.get(0).getAdminArea() + "\n"
                            + "Country code: "
                            + addresses.get(0).getCountryCode() + "\n"
                            + "Country name: "
                            + addresses.get(0).getCountryName() + "\n"
                            + "Phone: " + addresses.get(0).getPhone()
                            + "\n" + "Postbox: "
                            + addresses.get(0).getPostalCode() + "\n"
                            + "SubLocality: "
                            + addresses.get(0).getSubLocality() + "\n"
                            + "SubAdminArea: "
                            + addresses.get(0).getSubAdminArea() + "\n"
                            + "SubThoroughfare: "
                            + addresses.get(0).getSubThoroughfare()
                            + "\n" + "Thoroughfare: "
                            + addresses.get(0).getThoroughfare() + "\n"
                            + "URL: " + addresses.get(0).getUrl();*/

                    Log.e("locationstate","locationstate"+s);
                    // Toast.makeText(context, "city:::::statename" + cityName+""+stateName, Toast.LENGTH_SHORT).show();

                    tv_plumber_location.setText(s);
                   // tv_plumber_location.setText(s+""+cityName);


                    Log.e("CurrentCity","CurrentCity"+cityName);
                    Log.e("CurrentCity","Currentstate"+stateName);

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            Log.e("NotLocation","NotLocation");

        }

    }


}
