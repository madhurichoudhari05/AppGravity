package gravity.com.gravity.plumber.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import gravity.com.gravity.DataInterface.FileUploadInterface;
import gravity.com.gravity.R;
import gravity.com.gravity.retail_customer.activity.UserHomeActivity;
import gravity.com.gravity.service.Retrofit.retrofit.MadhuFileUploader;
import gravity.com.gravity.service.Retrofit.retrofit.RetrofitHandler;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonBaseActivity;
import gravity.com.gravity.utils.CommonUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;

import static gravity.com.gravity.utils.CommonUtils.dismissProgress;
import static gravity.com.gravity.utils.CommonUtils.reduceImageSize;
import static gravity.com.gravity.utils.CommonUtils.tvNo;

/**
 * Created by User on 4/17/2018.
 */

public class AadharDetail extends  CommonBaseActivity{

    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String picturePath = "";

    ImageView img_aadhar_attach;
    String USER_ID = "";
    String UserType="";
    private Context context;
    private EditText etAdaharNumber;
    ArrayList<String> mPickedImages;
    private Button btnSubimt;
    private ArrayList<String> imagelist=new ArrayList<>();
    private ImageView ivAadharcardImage,iv_default_adhar;
    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;
    private String[] PERMISSION_CAMERA={Manifest.permission.CAMERA};
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA};
    File destination=null;
    String mCurrentPhotoPath="";
    private String str="";
    String userName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aadhar_detail);
        context=AadharDetail.this;
        etAdaharNumber=findViewById(R.id.etAdaharNumber);
        ivAadharcardImage=findViewById(R.id.ivAadharcardImage);
        img_aadhar_attach=findViewById(R.id.img_aadhar_attach);
        iv_default_adhar=findViewById(R.id.iv_default_adhar);
        btnSubimt=findViewById(R.id.btnSubimt);

        mPickedImages = new ArrayList<>();
        Intent i=getIntent();
        USER_ID = i.getStringExtra("USERID");
        UserType=i.getStringExtra("USER_TYPE");
        userName=i.getStringExtra("USER_NAME");
        Log.d("USER_ID",USER_ID);

        img_aadhar_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openCameraGalleryDialog();


            }
        });





        btnSubimt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etAdaharNumber.getText().toString())) {
                    CommonUtils.snackBar("Please Enter Your Aadhar Number",img_aadhar_attach);
                }else if (etAdaharNumber.getText().toString().length()<12){
                    CommonUtils.snackBar("Adhar Number Should Be of 12 Digits",img_aadhar_attach);
                }else if(etAdaharNumber.getText().toString().length()>12){
                    CommonUtils.snackBar("Adhar Number Should Be of 12 Digits",img_aadhar_attach);
                }
                else   if(ivAadharcardImage.getDrawable()==null) {
                    CommonUtils.snackBar("Please attach Aadhar Card",img_aadhar_attach);
                }
                else {
                    uploadAdhar();

                }

            }
        });

    }
    private void uploadAdhar() {
        CommonUtils.showProgress(context);
        Map<String, String> params = new HashMap<String, String>();
        if(USER_ID.toString()!=null) {
            params.put("id", USER_ID);
        }
        String otp=getIntent().getStringExtra("OTP");

        if(otp.toString()!=null) {
            params.put("otp", otp);
        }
        if(UserType!=null){
            params.put("user_type",UserType);
        }
        CommonUtils.saveStringPreferences(context,AppConstants.PLUMBER_USER_ID,USER_ID);
        if(!TextUtils.isEmpty(etAdaharNumber.getText().toString())) {

            params.put("adhaar_no", etAdaharNumber.getText().toString());
        }



        /*Madhu*/

        MadhuFileUploader uploader = new MadhuFileUploader();
        uploader.uploadFile(params,imagelist , "file", context);


        for (String s : imagelist) {
            Log.e("madhuImage", s);
        }


        Log.e("jsonUpdateProfle", params.toString());

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call = service.uploadFileProfileile(uploader.getPartMap(), uploader.getPartBodyArr());
        // Call<ResponseBody> call = service.uploadFileFromLib(uploader.getPartMap(),uploader.getPartBody());
        call.enqueue(new Callback<ResponseBody>() {
            public boolean status;

            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {

                dismissProgress();
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);



                        int status = jsonObject.getInt("status");
                        String msg = jsonObject.getString("status");
                        String details = jsonObject.getString("response");
                        JSONObject jsondetail = new JSONObject(details);
                        String onlineStatus = jsondetail.getString("plumber_online");
                        // CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,true);
                        if (status==1) {


                            CommonUtils.savePreferencesString(context, AppConstants.PLUMBER_STATUS,onlineStatus);
                            CommonUtils.savePreferencesString(context,AppConstants.USER_NAME,userName);
                            CommonUtils.snackBar(msg,etAdaharNumber);
                            startActivity(new Intent(context,UserHomeActivity.class));
                            finish();
                        }
                        else {
                            CommonUtils.snackBar(msg,etAdaharNumber);

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                //   dialog.dismiss();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = "Socket Time Out";
                } else if (t instanceof IOException) {
                    msg = "IO exception";

                } else if (t instanceof HttpException) {
                    msg = "http exception";
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }



    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry, There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(context, getApplicationContext()
                        .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList = getPackageManager()
                            .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }


    private void activityForGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select : "),
                REQUEST_GALLERY);
    }




    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        if (selectedImage == null) {
            Toast.makeText(context, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }
        }


       /* galleryOperation(data);
        imagelist.add(picturePath);
        Bitmap bitmap2=CommonUtils.getBitMapFromImageURl(picturePath, (Activity) context);
        ivAadharcardImage.setImageBitmap(bitmap2);
        Log.e("galleryIsha",picturePath);*/

    }

    @Override
    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


        if (isPermissionGranted) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                activityForCamera();
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                activityForGallery();
            }
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    imagelist.clear();
                    imagelist.add(mCurrentPhotoPath);
                    Log.e("camera",mCurrentPhotoPath);
                     Bitmap bitmap=CommonUtils.getBitMapFromImageURl(mCurrentPhotoPath, (Activity) context);
                     ivAadharcardImage.setImageBitmap(bitmap);
                    iv_default_adhar.setVisibility(View.GONE);
                    break;
                case REQUEST_GALLERY:
                    imagelist.clear();
                    galleryOperation(data);
                    imagelist.add(picturePath);
                    Bitmap bitmap2=CommonUtils.getBitMapFromImageURl(picturePath, (Activity) context);
                    iv_default_adhar.setVisibility(View.GONE);
                    ivAadharcardImage.setImageBitmap(bitmap2);
                    Log.e("galleryIsha",picturePath);

                    break;

            }
        }
    }
    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();


        return image;
    }


    public void openCameraGalleryDialog() {
        final CharSequence[] items = {"Open Camera", "Open Gallery", "Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(
                this);
        builder.setTitle("Select : ");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera")) {
                    cameraPermissionMethod();
                } else if (items[item].equals("Open Gallery")) {
                    gallleryPermissionMethod();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }private void cameraPermissionMethod() {
        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            activityForCamera();
        }}private void gallleryPermissionMethod() {
        if (requestPermission(GALLERY_REQUEST_CODE, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE})) {
            activityForGallery();
        }
    }}
