package gravity.com.gravity.plumber.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.ComplaintsAssingned;
import gravity.com.gravity.plumber.activity.RequestDetails;
import gravity.com.gravity.plumber.adapter.ComplainAdapter;
import gravity.com.gravity.plumber.adapter.ComplainProgressAdapter;
import gravity.com.gravity.plumber.adapter.RequestAdapter;
import gravity.com.gravity.plumber.model.ComplaintModel;
import gravity.com.gravity.plumber.model.CustomerModel;
import gravity.com.gravity.plumber.model.progressCompleteModel;
import gravity.com.gravity.plumber.model.ProgressCompleteDetails;
import gravity.com.gravity.plumber.model.UserSpecificPOJO;
import gravity.com.gravity.plumber.model.progressCompleteModel;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 3/12/2018.
 */

public class InProgressFrag extends Fragment {

    RecyclerView recyclerView;
/*
ReceivedOrderModel orderModel;
ArrayList<ReceivedOrderModel> arrayList;
    FragRecievedAdapter adapter;*/

    ComplaintModel model;
    Button btn_view_All;
    private Context mcontext;
    private String User_id="";
    ComplainProgressAdapter adapter;
    private APIInterface apiInterface;
    List<ProgressCompleteDetails> list;
    private TextView tvComDetail;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_request_service, container, false);
        mcontext=getActivity();
        User_id= CommonUtils.getPreferencesString(mcontext, AppConstants.USER_ID);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        list = new ArrayList<>();
        btn_view_All=view.findViewById(R.id.btn_view_All);
        tvComDetail=view.findViewById(R.id.tvComDetail);
        recyclerView=view.findViewById(R.id.recycler_view);
        adapter = new ComplainProgressAdapter(getActivity(), list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        callUserSpecificRequest();

        btn_view_All.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),ComplaintsAssingned.class));
            }
        });


        return view;

    }

    private void callUserSpecificRequest() {

        Call<progressCompleteModel> call = apiInterface.getProgress("2",CommonUtils.getPreferencesString(getActivity(), AppConstants.PLUMBER_USER_ID));
        call.enqueue(new Callback<progressCompleteModel>() {
            @Override
            public void onResponse(Call<progressCompleteModel> call, Response<progressCompleteModel> response) {
                if (response != null) {

                    list.clear();

                    if(response.isSuccessful())
                    {

                        progressCompleteModel requesrListModel=response.body();

                        if(requesrListModel.getStatus()==1){

                            list=requesrListModel.getResponse();


                            if(list!=null&&list.size()>0) {
                                Log.e("progress","progress"+list.size());


                                if(list.size()==20)
                                {

                                    adapter = new ComplainProgressAdapter(getActivity(), list);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    recyclerView.setAdapter(adapter);
                                    btn_view_All.setVisibility(View.VISIBLE);

                                }
                                else {
                                    adapter = new ComplainProgressAdapter(getActivity(), list);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    recyclerView.setAdapter(adapter);

                                }
                            }
                            else {
                                tvComDetail.setVisibility(View.VISIBLE);
                                tvComDetail.setText("All your completed Request from Customers will come here.");

                            }


                        }else {

                            CommonUtils.snackBar(requesrListModel.getMsg(),btn_view_All);
                        }


                    }







                   /* try {

                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String msg=jsonObject.getString("msg");
                        int status=jsonObject.getInt("status");


                            adapter=new RequestAdapter(getActivity(),list);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(adapter);


                        } else {
                            CommonUtils.snackBar(msg, btn_view_All);
                        }*/

                } else {
                    CommonUtils.snackBar("Error Occured", btn_view_All);
                }
            }

            @Override
            public void onFailure(Call<progressCompleteModel> call, Throwable t) {
                CommonUtils.snackBar("Check Your Internet Connection", btn_view_All);
            }
        });
    }


    private void callCustomerDetailAPI(String complain_id) {

        Call<CustomerModel> call = apiInterface.getINprogresUserDetail(User_id,"2",complain_id);
        call.enqueue(new Callback<CustomerModel>() {
            @Override
            public void onResponse(Call<CustomerModel> call, Response<CustomerModel> responseBody) {

                CustomerModel customerModel=responseBody.body();

                if (responseBody != null) {

                    if (customerModel.getStatus() == 1) {
                        }


                else {


                        Toast.makeText(mcontext, "NO any Record Found.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(mcontext, "Error Occured", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CustomerModel> call, Throwable t) {
                Toast.makeText(mcontext, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
