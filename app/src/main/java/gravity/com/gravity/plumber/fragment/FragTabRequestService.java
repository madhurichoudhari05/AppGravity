package gravity.com.gravity.plumber.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.RequestAssigned;
import gravity.com.gravity.plumber.adapter.RequestAdapter;
import gravity.com.gravity.plumber.model.PlumberRequesrListModel;
import gravity.com.gravity.plumber.model.UserSpecificPOJO;
import gravity.com.gravity.service.Retrofit.retrofit.APIClient;
import gravity.com.gravity.service.Retrofit.retrofit.APIInterface;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 3/12/2018.
 */

public class FragTabRequestService extends Fragment {
RecyclerView recyclerView;

/*
ReceivedOrderModel orderModel;
ArrayList<ReceivedOrderModel> arrayList;
    FragRecievedAdapter adapter;*/

    UserSpecificPOJO model;
    RequestAdapter adapter;
    List<UserSpecificPOJO> list;

    private APIInterface apiInterface;
    private TextView tvComDetail;
    Button btn_view_All;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.tab_fragment_request_service, container, false);
        recyclerView=view.findViewById(R.id.recycler_view);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        btn_view_All=view.findViewById(R.id.btn_view_All);
        tvComDetail=view.findViewById(R.id.tvComDetail);

        list = new ArrayList<>();


        adapter = new RequestAdapter(getActivity(), list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        callUserSpecificRequest();

       /* for (int i = 0; i < 10; i++) {
            int[] pic = new int[]{R.drawable.home_t};
            model = new ComplaintModel("Low Water Pressure", "Tony Starck", "2032015456", "Tony Starck", "Satetelite Ahemdabad",  "Delivered 29-01-2018", "10:00pm",pic[0]);
            list.add(model);
            adapter.notifyDataSetChanged();
        }*/
       /* adapter=new ComplainAdapter(getActivity(),list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
*/

        btn_view_All.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),RequestAssigned.class));
            }
        });


        return view;
    }


    private void callUserSpecificRequest() {

        Call<PlumberRequesrListModel> call = apiInterface.specificComplaint(CommonUtils.getPreferencesString(getActivity(),AppConstants.PLUMBER_USER_ID));
        call.enqueue(new Callback<PlumberRequesrListModel>() {
            @Override
            public void onResponse(Call<PlumberRequesrListModel> call, Response<PlumberRequesrListModel> response) {
                if (response != null) {

                 list.clear();

                    if(response.isSuccessful())
                    {

                        PlumberRequesrListModel requesrListModel=response.body();

                        if(requesrListModel.getStatus()==1){

                            list=requesrListModel.getData();


                            if(list!=null&&list.size()>0) {

                                if(list.size()==20)
                                {

                                    adapter = new RequestAdapter(getActivity(), list);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    recyclerView.setAdapter(adapter);
                                    btn_view_All.setVisibility(View.VISIBLE);

                                }
                                else {
                                    adapter = new RequestAdapter(getActivity(), list);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    recyclerView.setAdapter(adapter);

                                }
                            }
                            else {
                                tvComDetail.setVisibility(View.VISIBLE);
                                tvComDetail.setText("All your request from Customers will come here.");

                            }


                        }else {

                            CommonUtils.snackBar(requesrListModel.getMsg(),btn_view_All);
                        }


                    }







                   /* try {

                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String msg=jsonObject.getString("msg");
                        int status=jsonObject.getInt("status");


                            adapter=new RequestAdapter(getActivity(),list);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(adapter);


                        } else {
                            CommonUtils.snackBar(msg, btn_view_All);
                        }*/

                } else {
                    CommonUtils.snackBar("Error Occured", btn_view_All);
                }
            }

            @Override
            public void onFailure(Call<PlumberRequesrListModel> call, Throwable t) {
                CommonUtils.snackBar("Check Your Internet Connection", btn_view_All);
            }
        });
    }

}
