package gravity.com.gravity.plumber.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.ComplaintDetails;
import gravity.com.gravity.plumber.activity.RequestDetails;
import gravity.com.gravity.plumber.model.ComplaintModel;



public class ComplaintAssignedAdapter extends RecyclerView.Adapter<ComplaintAssignedAdapter.InnerComplaint> {
    Context context;
    ArrayList<ComplaintModel> list = new ArrayList<>();

    public ComplaintAssignedAdapter(Context context, ArrayList<ComplaintModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public InnerComplaint onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_assigned_single, parent, false);
        return new InnerComplaint(view);
    }

    @Override
    public void onBindViewHolder(InnerComplaint holder, int position) {
        ComplaintModel model = list.get(position);
        holder.txt_ca.setText(model.getTxt_ca());
        holder.txt2_ca.setText(model.getTxt2_ca());
        holder.txt3_ca.setText(model.getTxt3_ca());
        holder.txt4_ca.setText(model.getTxt4_ca());
        holder.txt5_ca.setText(model.getTxt5_ca());
        holder.txt6_ca.setText(model.getTxt6_ca());
        holder.txt7_ca.setText(model.getTxt7_ca());
        holder.img_ca.setImageResource(model.getImg_ca());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ComplaintDetails.class));
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class InnerComplaint extends RecyclerView.ViewHolder {
        TextView txt_ca, txt2_ca, txt3_ca, txt4_ca, txt5_ca, txt6_ca, txt7_ca, txt1_ca;
        ImageView img_ca;

        public InnerComplaint(View itemView) {
            super(itemView);

            txt_ca = itemView.findViewById(R.id.txt1_ca);
            txt2_ca = itemView.findViewById(R.id.t2_ca);
            txt3_ca = itemView.findViewById(R.id.t3_ca);
            txt4_ca = itemView.findViewById(R.id.t4_ca);
            txt5_ca = itemView.findViewById(R.id.t5_ca);
            txt6_ca = itemView.findViewById(R.id.t6_ca);
            txt7_ca = itemView.findViewById(R.id.t7_ca);
            img_ca = itemView.findViewById(R.id.img_ca);

        }
    }
}
