package gravity.com.gravity.plumber.activity;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.adapter.stateadapter_profile;

public class Edit_profile_activity extends AppCompatActivity {
    Spinner spinner;
    ArrayList<String> country;
    RelativeLayout relative_country;
    AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    stateadapter_profile stateAdapter;
    EditText et_state;
    TextView txt_country;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editscreen);
        txt_country = findViewById(R.id.txt_country);
        relative_country = findViewById(R.id.relative_country);
        relative_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog();
            }
        });

    }
    private void prepareStateList(ArrayList<String> stateList) {
        stateList.add("Afghanistan");
        stateList.add("Albania");
        stateList.add("Algeria");
        stateList.add("Americansamoa");
        stateList.add("Andorra");
        stateList.add("Angola");
        stateList.add("Anguilla");
        stateList.add("Antarctica");
        stateList.add("Antiguaand Barbuda");
        stateList.add("Argentina");
        stateList.add("Armenia");
        stateList.add("Aruba");
        stateList.add("Australia");
        stateList.add("Bahamas");
        stateList.add("Bahrain");
        stateList.add("Bangladesh");
        stateList.add("Barbados");
        stateList.add("Belarus");
        stateList.add("Belgium");
        stateList.add("Belize");
        stateList.add("Benin");
        stateList.add("Bermuda");
        stateList.add("Bhutan");
        stateList.add("Bolivia");
        stateList.add("Bosnia");
        stateList.add("Botswana");
        stateList.add("India");
        stateList.add("Pakistan");
        stateList.add("Nepal");
        stateList.add("Bhutan");
        stateList.add("Japan");
        stateList.add("Iran");
        stateList.add("Iraq");
        stateList.add("UAE");
    }

    private void openDialog() {
        builder = new AlertDialog.Builder(this);
        alertDialog = builder.create();

     /*   LayoutInflater layoutInflater=LayoutInflater.from(getActivity());
        View view=layoutInflater.inflate(R.layout.state_list,null,false);
     */
        View view = LayoutInflater.from(this).inflate(R.layout.state_list, null, false);
        alertDialog.setView(view);
        et_state = view.findViewById(R.id.et_state);



        ArrayList<String> stateList = new ArrayList<>();
        prepareStateList(stateList);

        RecyclerView recyclerView = view.findViewById(R.id.recyler_state);
        stateAdapter = new stateadapter_profile(stateList, this,Edit_profile_activity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(stateAdapter);
        alertDialog.show();

        et_state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                stateAdapter.filter(editable.toString());

            }
        });


    }
    public void setState(int position, ArrayList<String> list) {

        txt_country.setText(list.get(position));
        txt_country.setTextColor(Color.parseColor("#8f8f8f"));

        alertDialog.dismiss();

    }
}
