package gravity.com.gravity.plumber.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.model.ComplaintModel;
import gravity.com.gravity.plumber.model.UserSpecificPOJO;


public class ComplaintsAssingned extends AppCompatActivity {
    RecyclerView recycler_ca;
    ComplaintModel model;
    RequestAdapter adapter;
    ArrayList<UserSpecificPOJO> list = new ArrayList<>();
    Context context;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint_assigned);
        recycler_ca = findViewById(R.id.recycler_ca);
        context=ComplaintsAssingned.this;
        adapter = new RequestAdapter(context, list);
        recycler_ca.setLayoutManager(new LinearLayoutManager(ComplaintsAssingned.this));
        recycler_ca.setAdapter(adapter);

        for (int i = 0; i < 10; i++) {
            int[] pic = new int[]{R.mipmap.search};
            model = new ComplaintModel("Low Water Pressure", "Tony Starck", "2032015456", "Tony Starck", "Satetelite Ahemdabad", "Complaint Number", "Delivered 29-01-2018",  pic[0]);
           // list.add(model);
            adapter.notifyDataSetChanged();
        }
    }
}
