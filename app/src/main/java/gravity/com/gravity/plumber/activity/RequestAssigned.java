package gravity.com.gravity.plumber.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.adapter.RequestAssignedAdapter;
import gravity.com.gravity.plumber.model.ComplaintModel;

public class RequestAssigned extends AppCompatActivity {

    RecyclerView recyclerView;
/*
ReceivedOrderModel orderModel;
ArrayList<ReceivedOrderModel> arrayList;
    FragRecievedAdapter adapter;*/
Context context;
    ComplaintModel model;
    RequestAssignedAdapter adapter;
    ArrayList<ComplaintModel> list = new ArrayList<>();

ImageView back_requestassigned;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_assigned);
        recyclerView = findViewById(R.id.recycler_view);
        context = RequestAssigned.this;
        back_requestassigned = findViewById(R.id.back_requestassigned);
        back_requestassigned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        adapter = new RequestAssignedAdapter(RequestAssigned.this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
        for (int i = 0; i < 10; i++) {
            int[] pic = new int[]{R.drawable.home_t};
            model = new ComplaintModel("Low Water Pressure", "Mark Kofit", "2032015456", "Tony Starck", "Satetelite Ahemdabad", "Complaint Number", "10:40 pm", pic[0]);
            list.add(model);
            adapter.notifyDataSetChanged();
        }
       /* adapter=new ComplainAdapter(RequestAssigned.this,list);
        recyclerView.setLayoutManager(new LinearLayoutManager(RequestAssigned.this));
        recyclerView.setAdapter(adapter);
*/
    }
}
