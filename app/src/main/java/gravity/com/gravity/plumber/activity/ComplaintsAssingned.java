package gravity.com.gravity.plumber.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.adapter.ComplainAdapter;
import gravity.com.gravity.plumber.adapter.ComplaintAssignedAdapter;
import gravity.com.gravity.plumber.model.ComplaintModel;


public class ComplaintsAssingned extends AppCompatActivity {
    RecyclerView recycler_ca;
    ComplaintModel model;
    ImageView back_complaintassigned;
    ComplaintAssignedAdapter adapter;
    ArrayList<ComplaintModel> list = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint_assigned);
        recycler_ca = findViewById(R.id.recycler_ca);
        back_complaintassigned = findViewById(R.id.back_complaintassigned);
        back_complaintassigned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        adapter = new ComplaintAssignedAdapter(ComplaintsAssingned.this, list);
        recycler_ca.setLayoutManager(new LinearLayoutManager(ComplaintsAssingned.this));
        recycler_ca.setAdapter(adapter);

        for (int i = 0; i < 10; i++) {
            int[] pic = new int[]{R.drawable.home_t};
            model = new ComplaintModel("Low11 Water Pressure", "Tony Starck", "2032015456", "Tony Starck", "Satetelite Ahemdabad", "Complaint Number", "Delivered 29-01-2018", pic[0]);
            list.add(model);
            adapter.notifyDataSetChanged();
        }
    }
}
