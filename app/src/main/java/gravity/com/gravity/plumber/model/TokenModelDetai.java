package gravity.com.gravity.plumber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenModelDetai {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("otp")
        @Expose
        private String otp;
        @SerializedName("user_type")
        @Expose
        private String userType;
        @SerializedName("plumber_type")
        @Expose
        private String plumberType;
        @SerializedName("plumber_online")
        @Expose
        private String plumberOnline;
        @SerializedName("profile_pic")
        @Expose
        private String profilePic;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("service_location")
        @Expose
        private String serviceLocation;
        @SerializedName("adhaar_no")
        @Expose
        private String adhaarNo;
        @SerializedName("adhaar")
        @Expose
        private String adhaar;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("status")
        @Expose
        private String status;

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

        public String getOtp() {
                return otp;
        }

        public void setOtp(String otp) {
                this.otp = otp;
        }

        public String getUserType() {
                return userType;
        }

        public void setUserType(String userType) {
                this.userType = userType;
        }

        public String getPlumberType() {
                return plumberType;
        }

        public void setPlumberType(String plumberType) {
                this.plumberType = plumberType;
        }

        public String getPlumberOnline() {
                return plumberOnline;
        }

        public void setPlumberOnline(String plumberOnline) {
                this.plumberOnline = plumberOnline;
        }

        public String getProfilePic() {
                return profilePic;
        }

        public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
        }

        public String getFirstName() {
                return firstName;
        }

        public void setFirstName(String firstName) {
                this.firstName = firstName;
        }

        public String getLastName() {
                return lastName;
        }

        public void setLastName(String lastName) {
                this.lastName = lastName;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public String getPhone() {
                return phone;
        }

        public void setPhone(String phone) {
                this.phone = phone;
        }

        public String getCountry() {
                return country;
        }

        public void setCountry(String country) {
                this.country = country;
        }

        public String getState() {
                return state;
        }

        public void setState(String state) {
                this.state = state;
        }

        public String getCity() {
                return city;
        }

        public void setCity(String city) {
                this.city = city;
        }

        public String getLongitude() {
                return longitude;
        }

        public void setLongitude(String longitude) {
                this.longitude = longitude;
        }

        public String getLatitude() {
                return latitude;
        }

        public void setLatitude(String latitude) {
                this.latitude = latitude;
        }

        public String getServiceLocation() {
                return serviceLocation;
        }

        public void setServiceLocation(String serviceLocation) {
                this.serviceLocation = serviceLocation;
        }

        public String getAdhaarNo() {
                return adhaarNo;
        }

        public void setAdhaarNo(String adhaarNo) {
                this.adhaarNo = adhaarNo;
        }

        public String getAdhaar() {
                return adhaar;
        }

        public void setAdhaar(String adhaar) {
                this.adhaar = adhaar;
        }

        public String getDeviceToken() {
                return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
                this.deviceToken = deviceToken;
        }

        public String getDeviceType() {
                return deviceType;
        }

        public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }
}
