package gravity.com.gravity.plumber.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.Edit_profile_activity;
import gravity.com.gravity.utils.CommonUtils;

public class stateadapter_profile extends RecyclerView.Adapter<stateadapter_profile.InnerState> {
    ArrayList<String> list;
    Context context;
    Edit_profile_activity edit_profile_activity;
    ArrayList<String> stateList;
    ArrayList<String> filterList;

    public stateadapter_profile(ArrayList<String> list, Context context, Edit_profile_activity edit_profile_activity) {

        this.stateList = list;
        this.context = context;
         this.edit_profile_activity=edit_profile_activity ;
        filterList = new ArrayList<>();
        this.filterList.addAll(stateList);


    }

    @Override
    public InnerState onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.state_single, parent, false);

        return new InnerState(view);
    }

    @Override
    public void onBindViewHolder(final InnerState holder, final int position) {

        if (CommonUtils.getIntPreferences(context, "place") == position) {
         /*
                holder.itemView.setBackgroundColor(Color.parseColor("#BA2F44"));
                holder.txt_state.setTextColor(Color.parseColor("#8f8f8f"));
      */

            holder.itemView.setBackgroundColor(Color.parseColor("#BA2F44"));
            holder.txt_state.setTextColor(Color.WHITE);
        }

        else {
            holder.itemView.setBackgroundColor(Color.WHITE);
        }
        holder.txt_state.setText(stateList.get(position));
       /* if (CommonUtils.getPreferencesBoolean(context, "linear_background")) {
            // holder.linear_state.setBackgroundColor(Color.parseColor("#BA2F44"));
            CommonUtils.getIntPreferences(context, "place");
        }*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   holder.txt_state.setText(view.);
                CommonUtils.savePreferencesBoolean(context, "linear_background", true);
                CommonUtils.saveIntPreferences(context, "place", position);
                edit_profile_activity.setState(position, stateList);
            }
        });
    }


    @Override
    public int getItemCount() {
        return stateList.size();
    }

    public void filter(String charText) {


        charText = charText.toLowerCase(Locale.getDefault());
        stateList.clear();
        if (charText.length() == 0) {
            stateList.addAll(filterList);
        } else {
            for (String wp : filterList) {
                if (wp.toLowerCase(Locale.getDefault()).contains(charText)) {
                    stateList.add(wp);
                }
            }
        }
        notifyDataSetChanged();

    }

    public class InnerState extends RecyclerView.ViewHolder {
        TextView txt_state;
        LinearLayout linear_state;

        public InnerState(View itemView) {
            super(itemView);
            txt_state = itemView.findViewById(R.id.txt_state);
            linear_state = itemView.findViewById(R.id.linear_state);

        }
    }
}
