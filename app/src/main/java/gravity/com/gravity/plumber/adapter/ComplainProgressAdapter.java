package gravity.com.gravity.plumber.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.RequestDetails;
import gravity.com.gravity.plumber.model.ProgressCompleteDetails;


public class ComplainProgressAdapter extends RecyclerView.Adapter<ComplainProgressAdapter.InnerComplaint> {
    Context context;
   List<ProgressCompleteDetails> list = new ArrayList<>();

    public ComplainProgressAdapter(Context context, List<ProgressCompleteDetails> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public InnerComplaint onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.compalint_single, parent, false);
        return new InnerComplaint(view);
    }

    @Override
    public void onBindViewHolder(InnerComplaint holder, int position) {
        ProgressCompleteDetails model = list.get(position);






        if(list.get(position).getComp().getLatitude()!=null&&list.get(position).getComp().getLongitude()!=null) {
            getLocation(list.get(position).getComp().getLatitude(), list.get(position).getComp().getLongitude(),holder);
        }
       /* holder.txt_ca.setText(model.getTxt_ca());
        holder.txt2_ca.setText(model.getTxt2_ca());
        holder.txt3_ca.setText(model.getTxt3_ca());
            holder.txt4_ca.setText(model.getTxt4_ca());
        holder.txt5_ca.setText(model.getTxt5_ca());
        holder.txt6_ca.setText(model.getTxt6_ca());
        holder.txt7_ca.setText(model.getTxt7_ca());*/






        if(list.get(position).getComp().getDate()!=null) {
            String date = list.get(position).getComp().getDate();

            String[] parts = date.split(" ");
            String currentDate = parts[0]; // 004
            String time = parts[1];


            holder.txt6_ca.setText(currentDate);
            holder.txt7_ca.setText(time);


        }


        
        holder.txt_ca.setText(list.get(position).getComp().getProduct());
        holder.txt2_ca.setText(list.get(position).getUserdata().getFirstName());
        holder.txt3_ca.setText(list.get(position).getUserdata().getPhone());
      //  holder.txt4_ca.setText(model.getCity());
    //    holder.txt5_ca.setText(list.get(position).getUserdata().getDate());

      //  holder.img_ca.setImageResource(model.getImg_ca());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,RequestDetails.class);
               // intent.putExtra(AppConstants.REQUEST_DETAILS,"RequestDetails");
                context.startActivity(intent);
            }
        });
    }


    private void getLocation(String latitudec, String longitudec, InnerComplaint holder) {




        if (latitudec!=null&& !latitudec.equals("")) {
            double latitude = Double.valueOf(latitudec);
            double longitude =Double.valueOf(longitudec);
            try {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses.size() > 0) {
                    String cityName = addresses.get(0).getLocality();
                    String stateName = addresses.get(0).getAdminArea();
                    String s = ""
                            + addresses.get(0).getAddressLine(0) + "\n";
                           /* + addresses.get(0).getFeatureName() + "\n"
                            + "Locality: "
                            + addresses.get(0).getLocality() + "\n"
                            + addresses.get(0).getPremises() + "\n"
                            + "Admin Area: ";*/
                           /* + addresses.get(0).getAdminArea() + "\n"
                            + "Country code: "
                            + addresses.get(0).getCountryCode() + "\n"
                            + "Country name: "
                            + addresses.get(0).getCountryName() + "\n"
                            + "Phone: " + addresses.get(0).getPhone()
                            + "\n" + "Postbox: "
                            + addresses.get(0).getPostalCode() + "\n"
                            + "SubLocality: "
                            + addresses.get(0).getSubLocality() + "\n"
                            + "SubAdminArea: "
                            + addresses.get(0).getSubAdminArea() + "\n"
                            + "SubThoroughfare: "
                            + addresses.get(0).getSubThoroughfare()
                            + "\n" + "Thoroughfare: "
                            + addresses.get(0).getThoroughfare() + "\n"
                            + "URL: " + addresses.get(0).getUrl();*/

                    Log.e("locationstate","locationstate"+s);
                    // Toast.makeText(context, "city:::::statename" + cityName+""+stateName, Toast.LENGTH_SHORT).show();

                   holder.txt4_ca.setText(s);
                    // tv_plumber_location.setText(s+""+cityName);


                    Log.e("CurrentCity","CurrentCity"+cityName);
                    Log.e("CurrentCity","Currentstate"+stateName);

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            Log.e("NotLocation","NotLocation");

        }

    }


    @Override
    public int getItemCount() {

        return list.size();

    }

    public class InnerComplaint extends RecyclerView.ViewHolder {
        TextView txt_ca, txt2_ca, txt3_ca, txt4_ca, txt5_ca, txt6_ca, txt7_ca, txt1_ca;
        ImageView img_ca;

        public InnerComplaint(View itemView) {
            super(itemView);

            txt_ca = itemView.findViewById(R.id.txt1_ca);
            txt2_ca = itemView.findViewById(R.id.t2_ca);
            txt3_ca = itemView.findViewById(R.id.t3_ca);
            txt4_ca = itemView.findViewById(R.id.t4_ca);
            txt5_ca = itemView.findViewById(R.id.t5_ca);
            txt6_ca = itemView.findViewById(R.id.t6_ca);
            txt7_ca = itemView.findViewById(R.id.t7_ca);
            img_ca = itemView.findViewById(R.id.img_ca);

        }
    }
}
