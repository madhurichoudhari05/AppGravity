package gravity.com.gravity.plumber.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import gravity.com.gravity.retail_customer.model.ComplianResponse;
import gravity.com.gravity.retail_customer.model.UserResponse;

public class ProgressCompleteDetails {

    @SerializedName("comp")
    @Expose
    private ComplianResponse comp;
    @SerializedName("userdata")
    @Expose
    private UserResponse userdata;

    public ComplianResponse getComp() {
        return comp;
    }

    public void setComp(ComplianResponse comp) {
        this.comp = comp;
    }

    public UserResponse getUserdata() {
        return userdata;
    }

    public void setUserdata(UserResponse userdata) {
        this.userdata = userdata;
    }
}
