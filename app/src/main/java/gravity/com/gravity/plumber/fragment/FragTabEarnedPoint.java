package gravity.com.gravity.plumber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.adapter.EarningAdater;
import gravity.com.gravity.retail_customer.adapter.FragRecievedAdapter;
import gravity.com.gravity.retail_customer.model.ReceivedOrderModel;
import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;

/**
 * Created by Admin on 3/12/2018.
 */

public class FragTabEarnedPoint extends Fragment {

RecyclerView recyclerView;
ArrayList<String> arrayList;
    EarningAdater adapter;
NestedScrollView nestedView;
TextView tv_plmbr_name;
Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.earning_fragment, container, false);
        context=getActivity();
        recyclerView=view.findViewById(R.id.recycler_earning);
        tv_plmbr_name=view.findViewById(R.id.tv_plmbr_name);
        tv_plmbr_name.setText(CommonUtils.NameCaps(CommonUtils.getPreferencesString(context, AppConstants.USER_NAME)));
        recyclerView.setFocusable(false);
        arrayList=new ArrayList<>();

        for(int i=0;i<10;i++) {

            arrayList.add("rajneesh");
            arrayList.add("4.5");
            arrayList.add("2000k");

        }
        adapter=new EarningAdater(getActivity(),arrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);

        return view;
    }

}
