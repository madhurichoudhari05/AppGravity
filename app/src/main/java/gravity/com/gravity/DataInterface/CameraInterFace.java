package gravity.com.gravity.DataInterface;

import java.util.List;

/**
 * Created by Admin on 3/8/2018.
 */

public interface CameraInterFace {
    public void callCamera();
    public List<String> getImageList();
}
