package gravity.com.gravity.DataInterface;


import java.util.Map;

import gravity.com.gravity.retail_customer.model.RegisterComplaintModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface FileUploadInterface {


    @Multipart
    @POST("plumber_registration_2.php")
    Call<ResponseBody> uploadFileProfileile(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part[] file);




    @Multipart
    @POST("complaint.php")
    Call<ResponseBody> raise_complaint(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part[] file);


    @Multipart
    @POST("complaint.php")
    Call</*ArrayList<PostResponse>*/ResponseBody> uploadFileWithPartMap(
            @Part("user_id") RequestBody user_id,
            @Part("category_name") RequestBody category_name,
            @Part("subcategory_name") RequestBody subcategory_name,
            @Part("product_name") RequestBody product_name,
            @Part("complaint_description") RequestBody complaint_description,
            @Part("warranty") RequestBody warranty,
            @Part("longitude") RequestBody longitude,
            @Part("latitude") RequestBody latitude);

    //TODO  Single image

   /* @Multipart
    @POST("contact-us.php")
    Call<ResponseBody> uploadFileFromLib(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part file);*/

}
