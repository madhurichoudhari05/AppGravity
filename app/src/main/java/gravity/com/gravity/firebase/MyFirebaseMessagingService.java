package gravity.com.gravity.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import java.util.Map;

import gravity.com.gravity.R;
import gravity.com.gravity.plumber.activity.HomeActivity;
import gravity.com.gravity.retail_customer.model.PlumberUser;

import static gravity.com.gravity.firebase.IConstants.IApp.NOTIF_INTENT;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        //  sendNotification(remoteMessage.getNotification().getBody());

//        Log.e("remoteMessage","remoteMessageBody"+remoteMessage.getNotification().getBody());

        String msg = "";
        String reply = "";
        Map<String, String> map = remoteMessage.getData();

      //  Log.e("remoteMessage","remoteMessage"+remoteMessage.toString());
       // Log.e("remoteMessage","remoteBody"+remoteMessage.getData());
//        Log.e("remoteMessage","remoteMessageBody"+remoteMessage.getNotification().getBody());
        /*map = new HashMap<Steering, String>();*/
        for (Map.Entry<String, String> entry : map.entrySet()) {
            /*Retrieve data as key:msg and value:json*/
            if (entry.getKey().trim().equals("msg")){
                msg = entry.getValue();
            }
            if (entry.getKey().trim().equals("reply")) {
                reply = entry.getValue();
            }
        }




        for (Map.Entry<String, String> entry : map.entrySet()) {
            /*Retrieve data as key:msg and value:json*/
            if (entry.getKey().trim().equals("msg")){
                msg = entry.getValue();
            }
        }


        if(!msg.trim().equals("")){
            Intent intent = new Intent();
            intent.setAction(NOTIF_INTENT);
            intent.putExtra(IConstants.IApp.PARAM_1, msg + "");
            sendBroadcast(intent);
        }
        }

    //This method is only generating push notification
    //It is same as we did in earlier posts


    private void sendNotification(String messageBody) {

        if(isNetworkAvailable(getApplicationContext())) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Netwrko App Notification")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }



}
