package gravity.com.gravity.firebase.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;



import java.util.List;

import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;
import io.reactivex.Flowable;

/**
 * Created by abul on 23/11/17.
 */

@Dao
public interface UserDao extends IConstants {

    @Query("SELECT * FROM user_table")
    LiveData<List<UserStatusDto>> getAll();

    @Query("SELECT * FROM user_table WHERE approveStatus == :appStatus AND requestStatus == :requestStatus")
    LiveData<List<UserStatusDto>> getAllByAppStatus(boolean appStatus, boolean requestStatus);

    @Query("SELECT * FROM user_table WHERE approveStatus == :appStatus")
    LiveData<List<UserStatusDto>> getAllByAppStatus(boolean appStatus);

    @Query("SELECT * FROM user_table")
    Flowable<List<UserStatusDto>> getAllRx();

    @Query("SELECT * FROM user_table WHERE toId = :uid")
    LiveData<UserStatusDto> getById(String uid);

    @Query("SELECT * FROM user_table WHERE toId = :uid")
    Flowable<UserStatusDto> getByIdRx(String uid);


    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);*/

    @Query("UPDATE user_table SET approveStatus=:status WHERE toId=:id")
    void approveUser(String id, Boolean status);

    @Query("UPDATE user_table SET approveStatus=:app , requestStatus=:req WHERE toId=:id")
    void blockUnblockUser(String id, boolean app, boolean req);

    @Query("UPDATE user_table SET latestMsg=:msg WHERE toId=:id")
    void updateData(String id, String msg);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserStatusDto> users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserStatusDto users);

    @Delete
    void delete(UserStatusDto user);

    @Query("DELETE FROM user_table")
    public void nukeTable();
}

