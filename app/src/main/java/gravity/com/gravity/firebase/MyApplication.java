package gravity.com.gravity.firebase;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import gravity.com.gravity.firebase.db.database.AppDatabase;


/**
 * Created by abul on 11/9/2017.
 */

public class MyApplication extends MultiDexApplication implements IConstants.INetwork,IConstants {

    private static MyApplication societyApplication;
    public static Context mAppContext;
    private static int maxThread=4;
    private static AppDatabase db;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        AndroidThreeTen.init(this);
        societyApplication = this;
        mAppContext = getApplicationContext();

        db = Room.databaseBuilder(this,
                AppDatabase.class, IDb.USER_DB)
                //DBMadhu
                .fallbackToDestructiveMigration()
                .build();



    }

    public static AppDatabase getDb(){
        return db;
    }

    public static synchronized MyApplication getInstance() {
        return societyApplication;
    }

    public static synchronized Context getmAppContext() {
        return mAppContext;
    }


}
