
package gravity.com.gravity.firebase.retofit.model.notif;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageData {

    @SerializedName("token")
    @Expose
    private String token;
    /*@SerializedName("notification")
    @Expose
    private Notification notification;*/
    @SerializedName("data")
    @Expose
    private Data data;

    public MessageData() {
    }

    public MessageData(String token, Notification notification, Data data) {
        this.token = token;
//        this.notification = notification;
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /*public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }*/

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
