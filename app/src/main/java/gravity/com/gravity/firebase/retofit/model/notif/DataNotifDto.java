
package gravity.com.gravity.firebase.retofit.model.notif;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataNotifDto {

    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("data")
    @Expose
    private Data data;
    /*@SerializedName("notification")
    @Expose
    private Notification notification;*/
    @SerializedName("delay_while_idle")
    @Expose
    private Boolean delayWhileIdle;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("content_available")
    @Expose
    private Boolean contentAvailable;

    public DataNotifDto() {
    }

    public DataNotifDto(String to, Data data/*,Notification notification*/, Boolean delayWhileIdle, String priority, Boolean contentAvailable) {
        this.to = to;
        this.data = data;
        this.delayWhileIdle = delayWhileIdle;
        this.priority = priority;
        this.contentAvailable = contentAvailable;
//        this.notification=notification;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Boolean getDelayWhileIdle() {
        return delayWhileIdle;
    }

    public void setDelayWhileIdle(Boolean delayWhileIdle) {
        this.delayWhileIdle = delayWhileIdle;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Boolean getContentAvailable() {
        return contentAvailable;
    }

    public void setContentAvailable(Boolean contentAvailable) {
        this.contentAvailable = contentAvailable;
    }

}
