package gravity.com.gravity.firebase.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;



import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;

import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.MyApplication;
import gravity.com.gravity.firebase.db.database.AppDatabase;
import gravity.com.gravity.firebase.db.entities.ChatMessageDto;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;
import gravity.com.gravity.firebase.retofit.model.fcmRes.handler.ExecutorHandler;
import gravity.com.gravity.firebase.utils.AppUtils;
import gravity.com.gravity.firebase.utils.NotificationDto;
import gravity.com.gravity.plumber.activity.RequestDetails;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class NotifiactionReciever extends BroadcastReceiver implements IConstants.IFcm,IConstants.INotification {

    ExecutorService mExecutor;
    AppDatabase mAppDb;
    private List<UserStatusDto> userDetail;
    private Flowable<List<UserStatusDto>> user;
    private boolean isDBUpdate;
    private Flowable<List<ChatMessageDto>> chat;
    private List<ChatMessageDto> chatList=new ArrayList<>();
    private boolean isMsgRead;


    @Override
    public void onReceive(Context context, Intent intent) {
        /*here we get the json we send form the user side*/
        String msg = intent.getStringExtra(IConstants.IApp.PARAM_1);
        String reply = intent.getStringExtra(IConstants.IApp.PARAM_2);
        if(reply!=null && !reply.trim().equals("")){
            sendNotification(context,reply, IConstants.IFcm.FCM_REPLY);
            return;
        }

        isDBUpdate=false;
        isMsgRead=false;
        mAppDb= MyApplication.getDb();
        mExecutor= ExecutorHandler.getInstance().getExecutor();

        if(user==null){

            user=mAppDb.userDao().getAllRx();
            user.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(responseBody -> {
                        if(isDBUpdate==false){
                            userDetail=responseBody;
                            handleMsg(context,msg);
                            isDBUpdate=true;
                        }

                    },e->{});

            chat=mAppDb.chatDao().loadMsgByReadStatusRx(false);
            chat.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(responseBody -> {
                        if(!isMsgRead){
                            chatList.clear();
                            chatList.addAll(responseBody);
                            handleMsg(context,msg);
                            isMsgRead=true;
                        }

                    },e->{});

        }else {
            handleMsg(context,msg);
        }

    }

    private void handleMsg(Context context,String msg){
        if (msg != null) {
            FcmMessage chat = new Gson().fromJson(msg, FcmMessage.class);

            if(chat!=null){
                switch (chat.getMsgType()) {
                    case FCM_TEXT_MSG:
                        handleChatMessage(context, chat);
                        break;
                    case FCM_LINK_MSG:
                        handleLinkMessage(context, chat);
                        break;
                    case FCM_FILE_MSG:
                        handleLinkMessage(context, chat);
                        break;
                    case FCM_STATUS_MSG:
                        handlePresence(context, chat);
                        break;
                    case FCM_NEW_REQUEST:
                        handleNewRequest(context, chat);
                        break;
                    case FCM_APP_REQUEST:
                        handleApproveRequest(context, chat);
                        break;
                    case FCM_APP_BLOCK:
                        handleBlockRequest(context, chat);
                        break;
                    case FCM_COMMENT:
                        sendNotification(context,
                                chat.getMsg(),chat.getMsgType());
                        break;
                    case FCM_REPLY:
                        sendNotification(context,
                                chat.getMsg(),chat.getMsgType());
                        break;
                }
            }


        }
    }


    /****************
     * Handling Type of Messages
     * ******************/
    private void handleNewRequest(Context context, FcmMessage chat) {
        if(checkUserApproved(chat.getMyId())){
            return;
        }
        saveUser(chat);
        sendNotification(context,
                chat.getMyName()+" send you a complain request",FCM_NEW_REQUEST);
        if(checkAvailable(chat.getMyId())){
            return;
        }
    }

    private void handleApproveRequest(Context context, FcmMessage chat) {
        updateUser(chat);
        sendNotification(context,
                chat.getMyName()+" accepted your complain request",FCM_APP_REQUEST,new Gson().toJson(chat));
        if(checkUserApproved(chat.getMyId())){
            return;
        }
    }

    private void handleBlockRequest(Context context, FcmMessage chat) {
//        saveUser(chat);
        mExecutor.execute(()->mAppDb.userDao().blockUnblockUser(chat.getMyId(),false,false));
        sendNotification(context,
                chat.getMyName()+" block you.",FCM_APP_REQUEST);
        if(checkUserApproved(chat.getMyId())){
            return;
        }
    }

    private void handleChatMessage(Context context, FcmMessage chat) {
        saveMessage(chat);

        String notMsg="";
        FcmMessage cloned=chat.clone();
        if(chatList.size()>0){
            for(ChatMessageDto dto:chatList){
                notMsg=dto.getToName()+" : "+dto.getMsg()+"\n"+notMsg;
            }
            notMsg+=chat.getMyName()+" : "+chat.getMsg();

            cloned.setMsg(notMsg);
        }

        if (AppUtils.isAppRunning(context)) {
            Log.e("package", AppUtils.isRunning(context) + "\n" + RequestDetails.class.getSimpleName());
            if (!AppUtils.isRunning(context).contains(RequestDetails.class.getSimpleName())) {
                sendNotification(context, new Gson().toJson(cloned),FCM_TEXT_MSG);
            }
        } else {
            sendNotification(context, new Gson().toJson(cloned),FCM_TEXT_MSG);
        }
    }

    private void handlePresence(Context context, FcmMessage chat) {

    }

    private void handleLinkMessage(Context context, FcmMessage chat) {
        saveMessage(chat);
    }


    /****************
     * Save Data to DB
     * ***************/
    private void saveMessage(FcmMessage chat) {

        if(!checkUserApproved(chat.getMyId())){
            return;
        }

        ChatMessageDto table = new ChatMessageDto(
                chat,
                ""/*fcmToken*/,
                false,
                true,
                chat.getMsgType()
        );
//        table.setSend(true);
//        table.setDateTime((Long.parseLong(chat.getDateTime()) + 1) + "");
//        table.setMyId(2 + "");
        mExecutor.execute(() -> mAppDb.chatDao().insert(table));

        mExecutor.execute(() -> mAppDb.userDao().updateData(table.getToId(),chat.getMsg()));
    }

    private void saveUser(FcmMessage chat) {
        UserStatusDto table = new UserStatusDto(
                chat.getMyId(),
                chat.getMsg(),
                false,
                MY_APPROVAL,
                chat.getMyName(),
                chat.getMyPic()
        );
        mExecutor.execute(() -> mAppDb.userDao().insert(table));
        ChatMessageDto chatTable = new ChatMessageDto(
                chat,
                ""/*fcmToken*/,
                true,
                true,
                chat.getMsgType()
        );
        mExecutor.execute(() -> mAppDb.chatDao().insert(chatTable));
    }

    private void updateUser(FcmMessage chat) {

        Type type=new TypeToken<HashMap<String,String>>(){}.getType();
        HashMap<String,String> map=new Gson().fromJson(chat.getMsg(),type);

        mExecutor.execute(() -> mAppDb.userDao().blockUnblockUser(chat.getMyId(),true,MY_REQUEST));

        chat.setMsg(map.get("msg"));
        ChatMessageDto table1 = new ChatMessageDto(
                chat,
                ""+map.get("key"),
                false,
                true,
                IConstants.IFcm.FCM_TEXT_MSG
        );
//        table1.setMsg("");
//        table.setSend(true);
//        table.setDateTime((Long.parseLong(chat.getDateTime()) + 1) + "");
//        table.setMyId(2 + "");
        mExecutor.execute(() -> mAppDb.chatDao().insert(table1));

    }

    private boolean checkUserApproved(String userId){
        boolean isUserValid=false;
        for(UserStatusDto dto:userDetail){
            if(dto.getToId().equals(userId)){
                if(dto.isApproveStatus()){
                    isUserValid=true;
                }
            }
        }
        return isUserValid;
    }

    private boolean checkAvailable(String userId){
        boolean isUserValid=false;
        for(UserStatusDto dto:userDetail){
            if(dto.getToId().equals(userId)){
                    isUserValid=true;
            }
        }
        return isUserValid;
    }

    /*****************************
     * showing notification on message recieced
     * **************************/
    private void sendNotification(Context context, String remoteMessage,int type) {
       sendNotification(context, remoteMessage,type,"");
    }
    private void sendNotification(Context context, String remoteMessage,int type,String extraMsg) {


        int count = type;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());

        NotificationDto dto = new NotificationDto();
        dto.setType(type);
        dto.setMsg(remoteMessage);
        dto.setDate(formattedDate);
        dto.setReadStatus(0);
        dto.setId(0);

        AppUtils.showNotification2(context, dto, count,extraMsg);
    }


}
