package gravity.com.gravity.firebase.retofit;




import java.util.HashMap;
import java.util.List;

import gravity.com.gravity.firebase.retofit.model.TokenResponse;
import gravity.com.gravity.plumber.model.TokenModel;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by abul on 29/12/17.
 * "http://www.404coders.com/NetWrko/WebServices/chat/devicetoken_get.php"
 */

public interface ApiInterface {

    @FormUrlEncoded
    Observable<ResponseBody> updateFcmKey(@Field("user_id") String user_id, @Field("token") String token);




    @FormUrlEncoded
    @POST("http://wehyphens.com/gravitybath/webservices/user_data.php")
    Observable<TokenModel> getFcmToken(@Field("user_id") String user_id);




}
