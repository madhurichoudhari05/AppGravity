package gravity.com.gravity.firebase.retofit.model;

/**
 * Created by abul on 15/11/17.
 */

public class PermissionDto {

    private String permission;
    private boolean isCompalsary;
    private boolean isGranted;

    public PermissionDto(String permission, boolean isCompalsary) {
        this.permission = permission;
        this.isCompalsary = isCompalsary;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public boolean isCompalsary() {
        return isCompalsary;
    }

    public void setCompalsary(boolean compalsary) {
        isCompalsary = compalsary;
    }

    public boolean isGranted() {
        return isGranted;
    }

    public void setGranted(boolean granted) {
        isGranted = granted;
    }
}
