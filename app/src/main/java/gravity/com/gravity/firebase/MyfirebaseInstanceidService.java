package gravity.com.gravity.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import gravity.com.gravity.utils.AppConstants;
import gravity.com.gravity.utils.CommonUtils;


/**
 * Created by mamta on 11-04-2017.
 */

public class MyfirebaseInstanceidService extends FirebaseInstanceIdService {

    private  static  final  String  TOKEN="gettoken";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token= FirebaseInstanceId.getInstance().getToken();
        Log.e(TOKEN,"firebasetoken::"+token);
        CommonUtils.savePreferencesString(MyfirebaseInstanceidService.this, AppConstants.FIREBASE_KEY,token);
    }
}
