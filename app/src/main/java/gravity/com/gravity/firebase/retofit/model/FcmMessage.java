package gravity.com.gravity.firebase.retofit.model;

/**
 * Created by abul on 29/12/17.
 */

public class FcmMessage implements Cloneable{

    private String myId;
    private String myName;
    private String toId;
    private String toName;
    private String postId;
    private String dateTime;
    private String msg;
    private String myPic;
    private int msgType;

    public FcmMessage() {
    }

    public FcmMessage(String myId, String myName, String toId, String postId, String dateTime, String msg, int msgType) {
        this.myId = myId;
        this.myName = myName;
        this.toId = toId;
        this.postId = postId;
        this.dateTime = dateTime;
        this.msg = msg;
        this.msgType = msgType;
    }


    /*Use only in case of request and approve or user detail update*/
    public FcmMessage(String myId, String myName, String toId, String dateTime, String msg, int msgType,String senderImage, String postId) {
        this.myId = myId;
        this.myName = myName;
        this.toId = toId;
        this.dateTime = dateTime;
        this.msg = msg;
        this.msgType = msgType;
        this.myPic = senderImage;
        this.postId = postId;
    }

    public FcmMessage(String myId, String myName, String toId, String dateTime, String msg, int msgType,String senderImage, String postId,String toName) {
        this.myId = myId;
        this.myName = myName;
        this.toId = toId;
        this.dateTime = dateTime;
        this.msg = msg;
        this.msgType = msgType;
        this.myPic = senderImage;
        this.postId = postId;
        this.toName=toName;
    }


    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getMyPic() {
        return myPic;
    }

    public void setMyPic(String myPic) {
        this.myPic = myPic;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }


    public FcmMessage clone() {
        try {
            return (FcmMessage)super.clone();
        }
        catch (CloneNotSupportedException e) {
            // This should never happen
        }
        return null;
    }
}
