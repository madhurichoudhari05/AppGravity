package gravity.com.gravity.firebase.retofit.model.fcmRes.handler;



import java.util.concurrent.TimeUnit;

import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.retofit.ApiInterface;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class ChstRetrofitHandler {

    private static ChstRetrofitHandler uniqInstance;
   private final String BASE_URL = IConstants.IFcm.BASE_URL;


    private ApiInterface apiInterface;

    public static synchronized ChstRetrofitHandler getInstance() {
        if (uniqInstance == null) {
            uniqInstance = new ChstRetrofitHandler();
        }
        return uniqInstance;
    }

    public static synchronized ChstRetrofitHandler getNewInstanceOnLogin() {
        uniqInstance = new ChstRetrofitHandler();
        return uniqInstance;
    }

    private void ApiClient() {
        try {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // set up log type
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(50, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient)
                    .build();

            apiInterface = retrofit.create(ApiInterface.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ApiInterface getApi() {
        if (apiInterface == null) {
            uniqInstance.ApiClient();
        }
        return apiInterface;
    }

   /* Interceptor header = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            String firebaseKey=AppUtils.readStringFromPref(HappyApplication.getmAppContext(), FIREBASE_KEY);
            Request.Builder builder = chain.request().newBuilder();
            builder.addHeader("Accept", "application/json");
            builder.addHeader(STR_API_KEY, API_KEY);
            if(firebaseKey==null){
                firebaseKey=" ";
            }
            builder.addHeader(STR_FIRE_KEY, firebaseKey);
            return chain.proceed(builder.build());
        }
    };*/
}
