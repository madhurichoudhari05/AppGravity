package gravity.com.gravity.firebase.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;



import java.util.List;

import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.db.entities.ChatMessageDto;
import io.reactivex.Flowable;

/**
 * Created by abul on 23/11/17.
 */

@Dao
public interface ChatDao extends IConstants {

    @Query("SELECT * FROM chat_table")
    LiveData<List<ChatMessageDto>> getAll();

    @Query("SELECT * FROM chat_table WHERE dateTime = :uid")
    LiveData<List<ChatMessageDto>> loadAllByIds(String uid);

    @Query("SELECT * FROM CHAT_TABLE WHERE isRead LIKE (:status)")
    LiveData<List<ChatMessageDto>> loadMsgByReadStatus(boolean status);

    @Query("SELECT * FROM CHAT_TABLE WHERE isRead = :status ORDER BY dateTime DESC LIMIT 10")
    Flowable<List<ChatMessageDto>> loadMsgByReadStatusRx(boolean status);

    @Query("SELECT * FROM CHAT_TABLE WHERE toId LIKE (:toId)")
    Flowable<List<ChatMessageDto>> loadMessageByPostRx(String toId);

    @Query("SELECT * FROM CHAT_TABLE WHERE postId LIKE (:postId) and toId LIKE (:type)")
    LiveData<List<ChatMessageDto>> loadMessageByPost(String postId, String type);

    @Query("SELECT * FROM CHAT_TABLE WHERE postId LIKE (:postId) and msgType LIKE (:type) and toId LIKE (:recId)")
    LiveData<List<ChatMessageDto>> loadMessageByPost(String postId, int type, String recId);

    @Query("SELECT * FROM CHAT_TABLE WHERE isSend LIKE (:approved) and msgType LIKE (:type)")
    LiveData<List<ChatMessageDto>> getApprovalStatus(Boolean approved, String type);

    @Query("SELECT * FROM CHAT_TABLE WHERE msgType LIKE (:type) and toId LIKE (:recId)")
    LiveData<List<ChatMessageDto>> getChatDBOnType(int type, String recId);




    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);*/

    @Query("UPDATE CHAT_TABLE SET isRead=:status WHERE toId=:id")
    void updateData(String id, boolean status);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ChatMessageDto> users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ChatMessageDto users);

    @Delete
    void delete(ChatMessageDto user);

    @Query("DELETE FROM chat_table")
    public void nukeTable();
}

