package gravity.com.gravity.firebase.retofit.model.fcmRes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by abul on 30/12/17.
 */

public class FcmResponseDto {

    @SerializedName("multicast_id")
    @Expose
    private String multicastId;
    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("failure")
    @Expose
    private int failure;
    @SerializedName("canonical_ids")
    @Expose
    private int canonicalIds;
    @SerializedName("results")
    @Expose
    private List<Result> results = null;

    public String getMulticastId() {
        return multicastId;
    }

    public void setMulticastId(String multicastId) {
        this.multicastId = multicastId;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getFailure() {
        return failure;
    }

    public void setFailure(Integer failure) {
        this.failure = failure;
    }

    public Integer getCanonicalIds() {
        return canonicalIds;
    }

    public void setCanonicalIds(Integer canonicalIds) {
        this.canonicalIds = canonicalIds;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }


    public class Result {

        @SerializedName("error")
        @Expose
        private String error;

        public String getError() {
            return error;
        }

        public void setError(String error) {
            this.error = error;
        }
    }
}
