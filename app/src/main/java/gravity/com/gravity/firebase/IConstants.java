package gravity.com.gravity.firebase;


/**
 * Created by abul on 11/9/2017.
 */

public interface IConstants {

    interface IApp{
        public static final int ALL_PERMISSION=100;

        public static final String PARAM_1="param_1";
        public static final String PARAM_2="param_2";
        public static final String PARAM_3="param_3";
        public static final String PARAM_4="param_4";

        public static final String NOT_AVAIL="Not Available";

        public static String NOTIF_INTENT="android.cam.action.NOTIF";

        public static String PREF_FCM="pref_fcm";
        public static String PREF_NAME="pref_name";

    }

    interface INetwork{
//        public static final String BASE_URL="https://www.societyconnect.in";
        public static final String LIVE_DOMAIN = "http://apis.societyconnect.in";
        public static final String NETWORK_ERROR="Network not available.";

        public static final String CHAT_BASE_URL1="http://apis.societyconnect.in";
        public static final String CHAT_BASE_URL="http://34.212.94.240";
        public static final String CHAT_SOCKET_URL="34.212.94.240";

        public static final String CHAT_DOMAIN = "society-connect-prod";

        public static final int RET_START=0;
        public static final int RET_COMPLETE=1;
        public static final int RET_ERROR=-1;
        public static final int RET_NEXT=2;
    }

    interface IHeader{
        public static final String HEADER_KEY_V1="apikey";
        public static final String HEADER_VALUE_V1="0410e40afd4419511343fc2078dcf1cd";
    }

    interface IFrag{

        public static final int POP_SINGLE=-1;
        public static final int POP_FULL=0;


    }

    interface IDb{
        public static final String USER_DB="user_db";
        public static final int USER_DB_VERSION=2;
    }

    interface ILinks{
        public static final String IMAGE_PATH="https://www.societyconnect.in/public/society-uploads/public/";

    }

    interface INotification{
        public static final int NOTIF_TEXT_MSG=1;
        public static final int NOTIF_LINK_MSG=2;
        public static final int NOTIF_FILE_MSG=3;
        public static final int NOTIF_STATUS_MSG=4;
        public static final int NOTIF_NEW_REQUEST=5;
        public static final int NOTIF_APP_REQUEST=6;
        public static final int NOTIF_APP_BLOCK=7;
        public static final int NOTIF_COMMENT_REPLY=8;
    }
    interface IFcm{
        public static final String FCM_LEGECY_KEY="AIzaSyBRZbgrSNEkCvdx9wQUUHJhzZId-d2mNro";
        public static final String BASE_URL_FCM="https://fcm.googleapis.com";
        public static final String BASE_URL="http://wehyphens.com";

        public static final int FCM_TEXT_MSG=1;
        public static final int FCM_LINK_MSG=2;
        public static final int FCM_FILE_MSG=3;
        public static final int FCM_STATUS_MSG=4;
        public static final int FCM_NEW_REQUEST=5;
        public static final int FCM_APP_REQUEST=6;

        public static final int FCM_APP_BLOCK=7;
        public static final int FCM_COMMENT_REPLY=8;


        public static final int FCM_SENDER=11;
        public static final int FCM_RECIEVER=12;

        public static final int FCM_COMMENT=21;
        public static final int FCM_REPLY=22;

        public static final boolean MY_REQUEST=false;
        public static final boolean MY_APPROVAL=true;
    }


}
