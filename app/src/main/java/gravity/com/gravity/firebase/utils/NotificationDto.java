package gravity.com.gravity.firebase.utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abul on 11/7/17.
 */

public class NotificationDto implements Parcelable {

    private int id;
    private int type;
    private String date;
    private String msg;
    private int readStatus;

    public NotificationDto() {
    }


    private NotificationDto(Parcel in) {
        this.id=in.readInt();
        this.type = in.readInt();
        this.date = in.readString();
        this.msg = in.readString();
        this.readStatus=in.readInt();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(type);
        dest.writeString(date);
        dest.writeString(msg);
        dest.writeInt(readStatus);
    }

    public static Creator<NotificationDto> CREATOR= new Creator<NotificationDto>() {
        @Override
        public NotificationDto createFromParcel(Parcel source) {
            return new NotificationDto(source);
        }

        @Override
        public NotificationDto[] newArray(int size) {
            return new NotificationDto[size];
        }
    };
}
