package gravity.com.gravity.firebase.db.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import gravity.com.gravity.firebase.db.dao.ChatDao;
import gravity.com.gravity.firebase.db.dao.UserDao;
import gravity.com.gravity.firebase.db.entities.ChatMessageDto;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;

import static gravity.com.gravity.firebase.IConstants.IDb.USER_DB_VERSION;


/**
 * Created by abul on 23/11/17.
 */

@Database(entities = {
        ChatMessageDto.class,
        UserStatusDto.class
},
        version = USER_DB_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ChatDao chatDao();

    public abstract UserDao userDao();
}
