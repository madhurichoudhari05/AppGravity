package gravity.com.gravity.firebase.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import gravity.com.gravity.firebase.IConstants;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;


/**
 * Created by abul on 23/11/17.
 */

@Entity(tableName = "chat_table")
public class ChatMessageDto {

    @PrimaryKey
    @NonNull
    private String dateTime;
    private String myId;
    private String myName;
    private String toId;
    private String toName;
    private String postId;
    private String fcmToken;
    private boolean isRead;
    private boolean isSend;
    private int msgType;
    private String msg;

    public ChatMessageDto() {
    }

    @Ignore
    public ChatMessageDto(FcmMessage msg, String fcmToken, boolean isRead, boolean isSend, int type) {

        if(type== IConstants.IFcm.FCM_SENDER){
            this.toId =msg.getToId();
            this.myId =msg.getMyId();
            this.myName =msg.getMyName();
            this.toName =toName;
        }else {
            this.toId =msg.getMyId();
            this.myId =msg.getToId();
            this.myName =toName;
            this.toName =msg.getMyName();
        }

        this.msg=msg.getMsg();
        this.msgType=type;
        this.postId=msg.getPostId();
        this.dateTime=msg.getDateTime();
        this.fcmToken=fcmToken;
        this.isRead=isRead;
        this.isSend = isSend;

    }

    @Ignore
    public ChatMessageDto(String myId, String myName, String toId, String toName, String postId, String fcmToken, boolean isRead, boolean isSend, int msgType, String dateTime, String msg) {
        this.myId = myId;
        this.myName = myName;
        this.toId = toId;
        this.toName = toName;
        this.postId = postId;
        this.fcmToken = fcmToken;
        this.isRead = isRead;
        this.msgType = msgType;
        this.dateTime = dateTime;
        this.msg = msg;
        this.isSend = isSend;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}