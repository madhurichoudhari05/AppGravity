package gravity.com.gravity.firebase.retofit;





import java.util.Map;

import gravity.com.gravity.firebase.retofit.model.fcmRes.FcmResponseDto;
import gravity.com.gravity.firebase.retofit.model.notif.DataNotifDto;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

/**
 * Created by abul on 14/11/17.
 */

public interface FcmApi {

    /*@POST("/fcm/send")
    Observable<ResponseBody> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body Message message);*/

    @POST("/fcm/send")
    Observable<FcmResponseDto> sendMessage(
            @HeaderMap Map<String, String> headers,
            @Body DataNotifDto message);
}

/*AIzaSyC_QW0rp1O2aWp32ee4g-dLECPyiJwMAbk*/