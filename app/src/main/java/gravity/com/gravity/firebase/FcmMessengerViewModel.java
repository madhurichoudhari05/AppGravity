package gravity.com.gravity.firebase;

import android.app.Application;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;


import java.util.HashMap;
import java.util.List;

import gravity.com.gravity.firebase.db.entities.ChatMessageDto;
import gravity.com.gravity.firebase.db.entities.UserStatusDto;
import gravity.com.gravity.firebase.retofit.model.FcmMessage;
import gravity.com.gravity.firebase.retofit.model.TokenResponse;
import gravity.com.gravity.firebase.retofit.model.fcmRes.FcmResponseDto;
import gravity.com.gravity.firebase.retofit.model.notif.Data;
import gravity.com.gravity.firebase.retofit.model.notif.DataNotifDto;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by abul on 28/12/17.
 */


public class FcmMessengerViewModel extends SuperAndroidViewModel implements IConstants.IFcm {

    private LiveData<List<ChatMessageDto>> chatDBOns;
    private LiveData<List<UserStatusDto>> userListObs;
    private MutableLiveData<TokenResponse> fcmKeyObs;
    private MutableLiveData<Boolean> correctTokenObs;
    private MutableLiveData<String> myTokenUpdateObs;
    private MutableLiveData<Integer> requestStatus;
    boolean isValidToken = false;



    public FcmMessengerViewModel(@NonNull Application application) {
        super(application);
     chatDBOns = mAppDb.chatDao().loadMessageByPost("","");
       userListObs = mAppDb.userDao().getAll();
        fcmKeyObs = new MutableLiveData<>();
        correctTokenObs = new MutableLiveData<>();
        myTokenUpdateObs = new MutableLiveData<>();
        requestStatus = new MutableLiveData<>();
    }

    /*************************
     * Getter
     * ****************/
    public MutableLiveData<Boolean> getCorrectTokenObs() {
        return correctTokenObs;
    }

    public LiveData<UserStatusDto> getUserStatusObs(String userId) {
        return mAppDb.userDao().getById(userId);
    }

    public LiveData<List<UserStatusDto>> getUserAppList(boolean approveStatus, boolean requestStatus) {
        return mAppDb.userDao().getAllByAppStatus(approveStatus, requestStatus);
    }

    public LiveData<List<UserStatusDto>> getUserAppList(boolean approveStatus) {
        return mAppDb.userDao().getAllByAppStatus(approveStatus);
    }

    public LiveData<List<ChatMessageDto>> getChatDBOns(boolean status) {
        chatDBOns = mAppDb.chatDao().loadMsgByReadStatus(status);
        return chatDBOns;
    }

    public LiveData<List<ChatMessageDto>> getChatDBOns(String postId, String type) {
        chatDBOns = mAppDb.chatDao().loadMessageByPost(postId, type);
        return chatDBOns;
    }

    public LiveData<List<ChatMessageDto>> getChatDBOns(String postId, int type, String recId) {
        chatDBOns = mAppDb.chatDao().loadMessageByPost(postId, type, recId);
        return chatDBOns;
    }

    public LiveData<List<ChatMessageDto>> getChatDBOnType(int type, String recId) {
        chatDBOns = mAppDb.chatDao().getChatDBOnType(type, recId);
        return chatDBOns;
    }

    public MutableLiveData<TokenResponse> getFcmKeyObs() {
        return fcmKeyObs;
    }

    public MutableLiveData<String> getMyTokenUpdateObs() {
        return myTokenUpdateObs;
    }

    public LiveData<List<UserStatusDto>> getUserListObs() {
        return userListObs;
    }

    public MutableLiveData<Integer> getRequestStatus() {
        return requestStatus;
    }


    /*=========================================================================*/
    /******************
     * Send Msg
     * ****************/

    boolean approveStatus;
    boolean type;

    public void sendFcmMessage(FcmMessage chat, String fcmToken) {
        sendFcmMessage(chat, fcmToken, false);
    }

    public void sendFcmMessage(FcmMessage chat, String fcmToken, boolean isRequest) {
        /*Header*/
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", "key=" + IConstants.IFcm.FCM_LEGECY_KEY);

        /*data*/
        Data data = new Data("msg", new Gson().toJson(chat), 2);
        DataNotifDto dto = new DataNotifDto(fcmToken, data, false, "high", true);

        Observable<FcmResponseDto> call = mApi.sendMessage(header, dto);
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doFinally(() -> status.setValue(INetwork.RET_COMPLETE))
                .subscribe(responseBody -> {
                            if (responseBody.getSuccess() == 1) {
                                Log.e("requestAPiHit","requestAPiHit");
                                handleDb(chat, fcmToken, isRequest);
                                requestStatus.setValue(1);
                            } else {
                                requestStatus.setValue(0);
                            }
                        },
                        e -> {
                            requestStatus.setValue(0);
                        });
    }


    /**********************
     * Handle DataBase
     * ****************/
    private void handleDb(FcmMessage chat, String fcmToken, boolean isRequest) {

        switch (chat.getMsgType()) {
            case FCM_TEXT_MSG:
                handleChatMessage(chat, fcmToken);
                break;
            case FCM_LINK_MSG:
//                handleLinkMessage(chat);
                break;
            case FCM_FILE_MSG:
//                handleLinkMessage(chat);
                break;
            case FCM_STATUS_MSG:
//                handlePresence( chat);
                break;
            case FCM_NEW_REQUEST:
                handleNewRequest(chat, fcmToken, isRequest);
                break;
            case FCM_APP_REQUEST:
                handleApproveRequest(chat, fcmToken, isRequest);
                break;
            case FCM_APP_BLOCK:
                handleBlockRequest(chat, fcmToken);
                break;
            case FCM_COMMENT:
//                handleBlockRequest(chat, fcmToken);
                break;
        }
    }

    /*Save Chat message
    */
    private void handleChatMessage(FcmMessage chat, String fcmToken) {
        ChatMessageDto table = new ChatMessageDto(
                chat,
                fcmToken + "",
                true,
                false,
                IFcm.FCM_SENDER
        );
        mExecutor.execute(() -> mAppDb.chatDao().insert(table));
    }

    /*approve user status save*/
    private void handleApproveRequest(FcmMessage chat, String fcmToken, boolean isRequest) {

        if (!isRequest) {
          //  approveStatus = true;
            approveStatus = false;
            type = IFcm.MY_APPROVAL;
        } else {
            approveStatus = false;
            type = IFcm.MY_REQUEST;
        }

        mExecutor.execute(() -> mAppDb.userDao().blockUnblockUser(chat.getToId(),approveStatus,type));
    }

    /*new user request save*/
    private void handleNewRequest(FcmMessage chat, String fcmToken, boolean isRequest) {

        if (!isRequest) {
            approveStatus = true;
            type = IFcm.MY_APPROVAL;
        } else {
            approveStatus = false;
            type = IFcm.MY_REQUEST;
        }

        UserStatusDto user = new UserStatusDto(
                chat.getToId(),
                fcmToken,
                approveStatus,
                type,
                chat.getToName(),
                chat.getMyPic()

        );
        mExecutor.execute(() -> mAppDb.userDao().insert(user));
    }

    /*block a user*/
    private void handleBlockRequest(FcmMessage chat, String fcmToken) {
        mExecutor.execute(() -> mAppDb.userDao().blockUnblockUser(chat.getToId(),false,MY_APPROVAL));
    }


/*=========================================================================================*/

    /****************
     * Update fcm token
     * ***************/
    public void updateFcmToken(String userId, String fcmToken) {

        call = mApiInter.updateFcmKey(userId, fcmToken);
        getCall().subscribe(responseBody -> {
                    myTokenUpdateObs.setValue(System.currentTimeMillis() + "");
                    status.setValue(IConstants.INetwork.RET_NEXT);
                    msgToast.setValue("Updated");
                },
                e -> {
                    status.setValue(IConstants.INetwork.RET_ERROR);
                    error.setValue(e);
                });
    }

    /*********************
     * Get fcm Token
     * ********************/
    public void getFcmToken(UserStatusDto user) {

        call = mApiInter.getFcmToken(user.getToId());
        getCall(TokenResponse.class).subscribe(responseBody -> {
                    status.setValue(IConstants.INetwork.RET_NEXT);
                    fcmKeyObs.setValue(responseBody);
                    user.setFcmKey(responseBody.getDeviceToken());
                    mExecutor.execute(() -> mAppDb.userDao().insert(user));
                },
                e -> {
                    status.setValue(IConstants.INetwork.RET_ERROR);
                    error.setValue(e);
                });
    }

}
