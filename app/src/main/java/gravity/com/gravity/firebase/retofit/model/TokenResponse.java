package gravity.com.gravity.firebase.retofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abul on 30/12/17.
 */

public class TokenResponse {


    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

  /*  @SerializedName("response")
    @Expose
    private List<Response> response = null;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }


    public class Response {

        @SerializedName("device_token")
        @Expose
        private String deviceToken;

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }
    }
*/
    }
