package gravity.com.gravity.firebase.retofit.model.fcmRes.handler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by administrator on 31/8/16.
 */
public class ExecutorHandler {

    private static ExecutorHandler executorHandler;
    private static ExecutorService executor;

    public static ExecutorHandler getInstance(){
        if(executorHandler==null){
            executorHandler=new ExecutorHandler();
            executor=Executors.newFixedThreadPool(3);
        }
        return executorHandler;
    }
    public ExecutorService getExecutor(){
        if(executor.isTerminated()){
            executor=Executors.newFixedThreadPool(3);
        }
        return executor;
    }
    public void stopExecutor(){
        executor.shutdown();
    }
}
